<?php
require_once(__DIR__."/inc/core.php");

$pagetitle = "Registrazione Admin";

if(!LOGGED_IN){
    location(PATH."login.php");
}

if(checkpost("do")){
	switch($_POST["do"]){
		case "adminregistration":
			if(!checkpost("name")){
				$output["message"] = "Inserisci il nome del nuovo amministratore";
			}elseif(!checkpost("email")){
				$output["message"] = "Inserisci l'indirizzo email del nuovo amministratore";
			}elseif(!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)){
				$output["message"] = "L'indirizzo email inserito non &egrave; valido";
			}elseif(!checkpost("number")){
				$output["message"] = "Inserisci il numero di telefono del nuovo amministratore";
			}elseif(!checkpost("password")){
				$output["message"] = "Inserisci la password del nuovo amministratore";
			}else{
                if(num_rows(query("SELECT null FROM users WHERE email = '".escape($_POST["email"])."' OR username = '".escape($_POST["name"])."'")) != 0){
					$output["message"] = "Esiste gi&agrave; un utente con questo indirizzo email o con questo username";
				} else{
					query("INSERT INTO users(email,password,username,number,role)
						   VALUES ('".escape($_POST["email"])."','".escape(password($_POST["password"]))."', 
						   			'".escape($_POST["name"])."', '".escape($_POST["number"])."', 'admin')");
					$output["result"] = "success";
				}
			}
		break;
	}
	output();
}

include(__DIR__."/inc/header.php");
?>
<a class="fullbuttoncontainer" href="<?=PATH?>account.php">
    <button class = "empty">Torna all'elenco degli Organizzatori</button>
</a>
<section id="registrationadminsection">
	<h1>Registrazione Admin</h1>
	<form>
		<input type="text" id="name" name="name" autofocus />
        <label for="name">Username</label>

		<input type="email" id="registrationemail" name="email" />
		<label for="registrationemail">Email</label>

        <input type="tel" id="number" name="number" />
        <label for="number">Numero di Cellulare</label>

		<input type="password" id="adminpassword" name="password" />
        <label for="adminpassword">Password</label>
		<br />
		<input type="submit" name="adminregistration" value="Continua" />
	</form>
</section>

<script>
	$("section#registrationadminsection form").on("submit", function(e){
		e.preventDefault();
		formPost("registrationadminsection", function(data){
			if(checkData(data)){
				openAlert({
					title: "Fatto",
					text: "La registrazione &egrave; completata con successo!",
					okbutton: {
						text: "Ok",
						onclick: function(){
							loc("<?=PATH?>account.php");
						},
						close: false
					}
				});
			}
		});
	});
</script>
<?php
include(__DIR__."/inc/footer.php");
?>
