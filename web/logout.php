<?php
define("PAGE_LOGOUT", true);
require_once(__DIR__."/inc/core.php");

session_destroy();
location(PATH);
?>
