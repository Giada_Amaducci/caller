<?php
require_once(__DIR__."/inc/core.php");

$pagetitle = "Accedi";

if(LOGGED_IN){
	location(PATH."account.php");
}

if(checkpost("do")){
	switch($_POST["do"]){
		case "login":
			if(!checkpost("email") || !checkpost("password")){
				$output["message"] = "Email o password errati";
			}else{
				$user = user_info($_POST["email"], $_POST["password"]);
				if($user === false){
					$output["message"] = "Email o password errati";
				}else{
					if($user["role"] == "admin"){
						$_SESSION["email"] = $_POST["email"];
						$_SESSION["password"] = $_POST["password"];
						$output["result"] = "success";
					}else{
						$output["message"] = "Non sei un admin, per cui non ti è permesso entrare!";
					}
					
				}
			}
		break;
	}
	output();
}

include(__DIR__."/inc/header.php");
?>
<section id="loginsection" class="min">
	<h1>Benvenuto/a!</h1>
	<form>
		<input type="email" id="loginemail" name="email" <?=checkget("email")?"value=\"".$_GET["email"]."\"":"autofocus"?> />
		<label for="loginemail">Email</label>
		<input type="password" id="loginpassword" name="password" <?=checkget("email")?"autofocus":""?> />
		<label for="loginpassword">Password</label>
		<input type="submit" name="login" value="Accedi" />
	</form>
</section>
<script>
	$("section#loginsection form").on("submit", function(e){
		e.preventDefault();
		formPost("loginsection", function(data){
			if(checkData(data)){
				reload();
			}
		});
	});
</script>
<?php
include(__DIR__."/inc/footer.php");
?>
