<?php
require_once(__DIR__."/inc/core.php");

$pagetitle = "Gestione Utenti";

if(!LOGGED_IN){
    location(PATH."index.php");
}

include(__DIR__."/inc/header.php");
?>
<section id="button">
	<a href="<?=PATH?>logout.php">
		<button aria-label="disconnettiti">Disconnettiti</button>
	</a>
</section>
</br>
<a class="fullbuttoncontainer" href="<?=PATH?>admin_registration.php">
	<button class="empty">Aggiungi un nuovo admin</button>
</a>
<section id="userssection">
	<h1>Organizzatori da approvare</h1>
	<?php
		$organizers = query("SELECT id, username, email, number FROM users WHERE role = 'organizer_to_approve'");
		while($organizer = fetch($organizers)){
		?>
			<a href="<?=PATH?>user_manage.php?id=<?=$organizer["id"]?>">
				<div class="infobox">
					<span class="important"><?=entities($organizer["username"])?></span>
					<footer>
						<?=entities($organizer["number"])?>
						<br />
						<?=entities($organizer["email"])?>
					</footer>
				</div>
			</a>
		<?php
		}
	?>
</section>

<?php
    include(__DIR__."/inc/footer.php");
?>
