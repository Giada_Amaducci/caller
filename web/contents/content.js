function reloadWaterfall(){
	$("div.cards").on("DOMSubtreeModified", function(){
		reloadWaterfall();
	});
	$("div.cards").each(function(i, el){
		waterfall(el);
	});
}
function onResize(){
	// menu
	let width = $(window).width();
	let height = $(window).height();
	let radius = Math.sqrt(width*width+height*height);
	$("div#menucontainer").css("--data-radius", radius+"px");
	// waterfall: cards organization (2 columns)
	reloadWaterfall();
	// width check
	if(width < 320){
		alert("La larghezza minima per visualizzare correttamente questo sito web \xE8 di 320px.\n\nContinuando nella navigazione, alcuni contenuti potrebbero non essere posizionati correttamente.");
	}
}
$(window).on("resize", onResize);
$(document).ready(function(){
	onResize();

	// radiobuttons
	$("input[type='radio']").each(function(i, el){
		let radio = $(this);
		let newradio = $("<div class=\"radio\" data-name=\""+radio.attr("name")+"\"></div>");
		if(radio.prop("checked")){
			newradio.addClass("checked");
		}
		radio.on("change", function(){
			if($(this).prop("checked")){
				$("div.radio[data-name="+$(this).attr("name")+"]").removeClass("checked");
				newradio.addClass("checked");
				$(this).parents("form").submit();
			}
		});
		newradio.on("click", function(){
			radio.prop("checked", !$(this).hasClass("checked")).triggerHandler("change");
		});
		newradio.insertAfter(radio);
		radio.hide();
		if(radio.attr("id") != ""){
			$("label[for='"+radio.attr("id")+"']").addClass("radio");
		}
	});

	// checkboxes
	$("input[type='checkbox']").each(function(i, el){
		let checkbox = $(this);
		let newcheckbox = $("<div class=\"checkbox\"></div>");
		if(checkbox.prop("checked")){
			newcheckbox.addClass("checked");
		}
		checkbox.on("change", function(){
			if($(this).prop("checked")){
				newcheckbox.addClass("checked");
			}else{
				newcheckbox.removeClass("checked");
			}
		});
		newcheckbox.on("click", function(){
			checkbox.prop("checked", !$(this).hasClass("checked")).triggerHandler("change");
		});
		newcheckbox.insertAfter(checkbox);
		checkbox.hide();
		if(checkbox.attr("id") != ""){
			$("label[for='"+checkbox.attr("id")+"']").addClass("checkbox");
		}
	});

	// swiper: slider
	$("main div.slider").each(function(i, el){
		let slidesperview = el.getAttribute("data-slidesperview");
		slidesperview = (slidesperview==null ? 1 : slidesperview);
		new Swiper(el, {
			loop: false,
			slidesPerView: slidesperview,
			spaceBetween: (slidesperview==1 ? 0 : 10),
			navigation: {
				prevEl: el.querySelector("button.left"),
				nextEl: el.querySelector("button.right")
			},
			pagination: {
			   el: el.querySelector("div.pagination"),
			}
		});
	});

	// menu
	let menuopen = false;
	$("header#navbar button.menu").on("click", function(){
		if(!menuopen){
			$("div#menucontainer").addClass("visible");
			setTimeout(function(){
				$("div#menucontainer nav#menu").fadeIn();
				setTimeout(function(){
					$("body").addClass("menuopen");
					menuopen = true;
				}, 400);
			}, 200);
		}
	});
	$("div#menucontainer nav#menu header button.close").on("click", function(){
		if(menuopen){
			$("body").removeClass("menuopen");
			$("div#menucontainer nav#menu").fadeOut();
			$("div#menucontainer").removeClass("visible");
			setTimeout(function(){
				menuopen = false;
			}, 700);
		}
	});

	// organizer_registration.php
	$("section#registrationsport select[name='sport']").on("change", function(){
		if($(this).val() == "Calcio"){
			$("section#registrationsport select[name='type']").show();
			$("section#registrationsport label[for='type']").show();
		}else{
			$("section#registrationsport select[name='type']").hide();
			$("section#registrationsport label[for='type']").hide();
		}
	});
});
