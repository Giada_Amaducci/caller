function ajax(type, data){
	if(typeof data["url"] == "undefined"){
		data["url"] = "";
	}
	if(typeof data["data"] == "undefined"){
		data["data"] = {};
	}
	if(typeof data["callback"] == "undefined"){
		data["callback"] = function(){};
	}
	$.ajax({
		type: type,
		url: data["url"],
		data: data["data"],
		success: data["callback"],
		dataType: "json"
	});
}
function get(data){
	ajax("GET", data);
}
function post(data){
	ajax("POST", data);
}
function formPost(formsectionid, callback, loadingalert=true){
	if(loadingalert){
		openAlert({ text: "<div class=\"loading\"></div>" });
	}
	let data = {};
	$("section#"+formsectionid+" form input, section#"+formsectionid+" form select, section#"+formsectionid+" form textarea").each(function(i, el){
		if(el.tagName == "INPUT" && el.type == "submit"){
			data["do"] = el.name;
		}else if(el.tagName == "INPUT" && el.type == "checkbox"){
			data[el.name] = el.checked ? 1 : 0;
		}else if(el.tagName == "INPUT" && el.type == "radio"){
			if(el.checked){
				data[el.name] = el.value;
			}
		}else{
			data[el.name] = el.value;
			console.log(el.name);
		}
	});
	post({
		data: data,
		callback: callback
	});
}
function checkData(data){
	if(data["result"] == "error"){
		openAlert({
			title: "Whoops..",
			text: data["message"] || "Si &egrave; verificato un errore imprevisto, riprova",
			okbutton: {
				text: "Ok"
			}
		});
		return false;
	}
	return true;
}
function loc(url){
	document.location.href = url;
}
function reload(){
	openAlert({
		text: "<div class=\"loading\"></div>"
	});
	document.location.reload();
}
function isset(variable){
	return (typeof(variable) != "undefined");
}
function time(){
	return new Date().getTime();
}
var alert_okbutton_callback = function(){};
var alert_okbutton_close = true;
var alert_cancelbutton_callback = function(){};
var alert_cancelbutton_close = true;
var alert_canclose = false;
var alert_opentime;
function openAlert(data){
	alert_opentime = time();
	setTimeout(function(){
		if(isset(data["title"])){
			$("div#alertcontainer section h1").html(data["title"]);
			$("div#alertcontainer section h1").show();
		}else{
			$("div#alertcontainer section h1").hide();
		}
		if(isset(data["text"])){
			$("div#alertcontainer section p").html(data["text"]);
			$("div#alertcontainer section p").show();
		}else{
			$("div#alertcontainer section p").hide();
		}
		alert_okbutton_callback = function(){ };
		alert_canclose = false;
		if(isset(data["okbutton"])){
			alert_canclose = true;
			$("div#alertcontainer section button[name='ok']").html(data["okbutton"]["text"]);
			$("div#alertcontainer section button[name='ok']").show();
			if(isset(data["okbutton"]["onclick"])){
				alert_okbutton_callback = data["okbutton"]["onclick"];
			}else{
				alert_okbutton_callback = function(){ };
			}
			alert_okbutton_close = (!isset(data["okbutton"]["close"]) || data["okbutton"]["close"] !== false);
		}else{
            $("div#alertcontainer section button[name='ok']").hide();
            alert_okbutton_close = true;
        }
		alert_cancelbutton_callback = function(){ };
		if(isset(data["cancelbutton"])){
			alert_canclose = true;
			$("div#alertcontainer section button[name='cancel']").html(data["cancelbutton"]["text"]);
			$("div#alertcontainer section button[name='cancel']").show();
			if(isset(data["cancelbutton"]["onclick"])){
				alert_cancelbutton_callback = data["cancelbutton"]["onclick"];
			}else{
				alert_cancelbutton_callback = function(){ };
			}
			alert_cancelbutton_close = (!isset(data["cancelbutton"]["close"]) || data["cancelbutton"]["close"] !== false);
		}else{
            $("div#alertcontainer section button[name='cancel']").hide();
            alert_cancelbutton_close = true;
        }
        $(":focus").blur();
        setTimeout(function(){ $("div#alertcontainer section").scrollTop(0); }, 0);
		$("div#alertcontainer").fadeIn();
		$("div#alertcontainer section").addClass("visible");
		if(isset(data["oncreate"])){
			data["oncreate"]();
		}
	}, 0);
}
function closeAlert(method="ok"){
	alert_canclose = false;
	setTimeout(function(){
		if(method == "ok"){
			alert_okbutton_callback();
		}else if(method == "cancel"){
			alert_cancelbutton_callback();
		}else{
			$("div#alertcontainer").fadeOut(200);
		}
		if((method == "ok" && alert_okbutton_close) || (method == "cancel" && alert_cancelbutton_close)){
			$("div#alertcontainer").fadeOut(200);
			$("div#alertcontainer section").removeClass("visible");
			setTimeout(function(){
				$("div#alertcontainer section h1").html("");
				$("div#alertcontainer section p").html("");
				$("div#alertcontainer section button[name='ok']").hide();
				$("div#alertcontainer section button[name='ok']").html("");
				$("div#alertcontainer section button[name='cancel']").hide();
				$("div#alertcontainer section button[name='cancel']").html("");
			}, 300);
		}
	}, 300);
}
function errorAlert(message, okbuttoncallback=function(){}){
	openAlert({
		title: "Whoops...",
		text: message,
		okbutton: {
			text: "Ok",
			onclick: function(){
				okbuttoncallback();
			}
		}
	});
}
$(document).ready(function(){
	$("div#alertcontainer section button[name='ok']").on("click", function(){
		closeAlert("ok");
	});
	$("div#alertcontainer section button[name='cancel']").on("click", function(){
		closeAlert("cancel");
	});
	$(window).on("keyup", function(e){
		if(alert_canclose && alert_opentime < time()-300 && (e.keyCode == 13 || e.keyCode == 27)){
			closeAlert("ok");
		}
	});
});
