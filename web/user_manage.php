<?php
require_once(__DIR__."/inc/core.php");

if(!checkget("id")){
    location(PATH);
}

$user_to_search = query("SELECT *
                        FROM users 
                        WHERE id = '".escape($_GET["id"])."'");
$user_to_search = fetch($user_to_search);

if(checkpost("do")){
    switch($_POST["do"]){
        case "approveorganizer":
            query("UPDATE users
                    SET role = 'organizer'
                    WHERE id = '".escape($user_to_search["id"])."'");
            $pasw = generate_random_code();
            send_email($user_to_search["email"], "Approvazione account", entities($user_to_search["username"]).", ti diamo il benvenuto tra i nostri organizzatori di <b>Call-ER</b>!<br><br>Siamo molto felici di averti nella nostra squadra. <br /><br /> La password per entrare nel tuo nuovo profilo &egrave;: ".$pasw.", ti consigliamo di modificarla nel tuo primo accesso.<br /><br />Da questo momento in poi potrai pubblicare i tuoi itinerari.<br /><br />A presto!");
            $output["result"] = "success";
            break;
    }
    output();
}

include(__DIR__."/inc/header.php");
?>

<?php
    $user = fetch(query("SELECT u.*
                        FROM users u WHERE u.id = '".escape($_GET["id"])."'"));
    ?>
    <a class="fullbuttoncontainer" href="<?=PATH?>account.php">
        <button class = "empty">Torna all'elenco degli Organizzatori</button>
    </a>
    <section id="organizerdata" class="margintop">
        <h1>Dati dell'Organizzatore</h1>
        <form>
            <input type="text" id="organizerdata_name" name="name" value="<?=$user["username"]?>" disabled />
            <label for="organizerdata_name">Nome</label>
            <input type="email" id="organizerdata_registrationemail" name="email" value="<?=$user["email"]?>" disabled />
            <label for="organizerdata_registrationemail">Email</label>
            <input type="tel" id="organizerdata_phonenumber" name="phonenumber" value="<?=$user["number"]?>" disabled />
            <label for="organizerdata_phonenumber">Numero di Telefono</label>
            <input type="submit" name="approveorganizer" value="Approva Organizzatore" />

        </form>
    </section>
    <script type="text/javascript">
        $("section#organizerdata form").on("submit", function(e){
            e.preventDefault();
            formPost("organizerdata", function(data){
                if(checkData(data)){
                    openAlert({
                        title: "Fatto",
                        text: "L'organizzatore è stato approvato",
                        okbutton: {
                            text: "Ok",
                            onclick: function(){
                                loc("<?=PATH?>account.php");
                            },
                            close: false
                        }
                    });
                }
            });
    });
    </script>

<?php
include(__DIR__."/inc/footer.php");
?>
