<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" charset="utf-8" />
		<title>Call-ER | Forlì-Cesena<?=isset($pagetitle)?" - ".$pagetitle:""?></title>
		<script type="text/javascript" src="https://www.google.com/recaptcha/api.js"></script>
		<script type="text/javascript" src="<?=PATH?>contents/jquery.js?<?=time()?>"></script>
		<script type="text/javascript" src="<?=PATH?>contents/global.js?<?=time()?>"></script>
		<script type="text/javascript" src="<?=PATH?>contents/content.js?<?=time()?>"></script>
		<script type="text/javascript" src="<?=PATH?>contents/waterfall.js?<?=time()?>"></script>
		<script type="text/javascript" src="<?=PATH?>contents/swiper.js?<?=time()?>"></script>
		<link rel="stylesheet" href="<?=PATH?>contents/styles.css?<?=time()?>"/>
	</head>
	<body>
		<header id="navbar">
        	<div></div>
			<a id="logo" href="<?=PATH?>">
				<img alt="Logo sito: Call-ER" src="<?=PATH?>contents/logo2.png"/>
			</a>
		</header>
			<main class="background_style">
			
