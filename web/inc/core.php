<?php
// Error reporting
error_reporting(E_ALL);
ini_set("display_errors", 1);

// Data
define("DEBUG", ($_SERVER["SERVER_NAME"]=="localhost"));
define("SPATH", "caller.altervista.org");
define("PATH", (DEBUG?"http":"https")."://".SPATH."/");
define("OUTPUT_ERROR_MSG", "Errore inaspettato, riprova.");
$output = array("result" => "error", "message" => OUTPUT_ERROR_MSG);
define("EMAIL_FROM", "callemiliaromagna@gmail.com");
define("EMAIL_PASSWORD", "CallERApp");
define("MY_IP", isset($_SERVER["HTTP_CF_CONNECTING_IP"])?$_SERVER["HTTP_CF_CONNECTING_IP"]:$_SERVER["REMOTE_ADDR"]);

// Database connection
$_db = @mysqli_connect("localhost", "caller", "", "my_caller")or die("<center style='padding-top:50px;font-family:sans-serif;font-size:18px;'>Error during database connection :(</center>");
mysqli_set_charset($_db, "utf8");

// Session management
session_name("caller_session");
session_set_cookie_params(1296000);
session_start();

// Login
if(isset($_SESSION["email"]) && $_SESSION["email"] != "" && isset($_SESSION["password"]) && $_SESSION["password"] != ""){
	$myrow = user_info($_SESSION["email"], $_SESSION["password"]);
	if($myrow !== false){
		define("LOGGED_IN", true);
	}
}
if(!defined("LOGGED_IN")){
	define("LOGGED_IN", false);
}

// GET/POST data check
$_POST = r_trim($_POST);

// Utility function
function r_trim($data){
	if(is_array($data)){
		foreach($data as $key => $val){
			$data[$key] = r_trim($val);
		}
	}else{
		$data = trim($data);
	}
	return $data;
}
function checkget($key){
	return (isset($_GET[$key]) && trim($_GET[$key]) != "");
}
function checkpost($key){
	return (isset($_POST[$key]) && trim($_POST[$key]) != "");
}
function password($password){
	// Salt + Email before hash
	return md5("[#CALLER-SALT-LEFT#]".$password."[#CALLER-SALT-RIGHT#]");
}
function output(){
	global $output;
	header("Content-type: application/json");
	if($output["result"] == "success" && isset($output["message"]) && $output["message"] == OUTPUT_ERROR_MSG){
		unset($output["message"]);
	}
	exit(json_encode($output));
}
function location($url){
	header("Location: ".$url);
	exit;
}
function query($string){
	global $_db;
	$query = mysqli_query($_db, $string);
	if(!$query){
		exit($string." => ERRORE: ".mysqli_error($_db));
	}
	return $query;
}
function fetch($resource){
	return mysqli_fetch_assoc($resource);
}
function fetch_all($resource){
	return mysqli_fetch_all($resource);
}
function fetch_array($resource){
	return mysqli_fetch_array($resource, MYSQLI_ASSOC);
}
function num_rows($resource){
	return mysqli_num_rows($resource);
}
function insert_id(){
	global $_db;
	return mysqli_insert_id($_db);
}
function data_seek($resource, $offset=0){
	return mysqli_data_seek($resource, $offset);
}
function escape($string){
	global $_db;
	return mysqli_real_escape_string($_db, $string);
}
function is_alpha($text){
	return str_replace(array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"), "", $text) == "";
}
function user_info($email, $password){
	$myrow = query("SELECT * FROM users WHERE email = '".escape($email)."' AND password = '".escape(password($password))."'");	
	if(num_rows($myrow) == 1){
		$myrow = fetch($myrow);
		return $myrow;
	}
	return false;
}
function entities($string){
	return htmlentities($string);
}
function generate_random_code($length=8){
	$code = "";
	$chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	while(strlen($code) < $length){
		$char = substr($chars, mt_rand(0, strlen($chars)-1), 1);
		if(!strpos($code, $char)){
			$code .= $char;
		}
	}
	return $code;
}

function send_email($to, $subject, $text){
	require_once(__DIR__."/libs/class.phpmailer.php");
	require_once(__DIR__."/libs/class.smtp.php");
	if(!is_array($to)){
		$to = array($to);
	}
	$email = new PHPMailer();
	$email->isSMTP();
	$email->Host = "smtp.gmail.com";
	$email->SMTPAuth = true;
	$email->SMTPDebug = 0;
	$email->Username = EMAIL_FROM;
	$email->Password = EMAIL_PASSWORD;
	$email->SMTPSecure = "tls";
	$email->Port = 465;
	$email->setFrom(EMAIL_FROM, "CallER");
	$email->AddReplyTo(EMAIL_FROM);
	for($i=0;$i<count($to);$i++){
		$email->AddAddress($to[$i]);
	}
	$email->Subject = "CallER: ".$subject;
	$email->AddEmbeddedImage(__DIR__."/../contents/logo2.png", "logo");
	$email->AddEmbeddedImage(__DIR__."/../contents/pattern1.png", "bgpattern");
	$emailbody = "<center><div style=\"background-color: #6a1b9a; display: block; width: 90%; max-width: 600px; padding: 20px 100px; box-sizing: border-box; border-radius: 20px 20px 0 0;\"><img src=\"cid:logo\" style=\"width: 100%;\"></div><div style=\"width: 90%; max-width: 600px; background-color: #222; color: #FFF; padding: 30px; box-sizing: border-box; background-image: url(cid:bgpattern); font-size: 15px;\">".$text."</div><div style=\"width: 90%; max-width: 600px; background-color: #6a1b9a; color: #FFF; padding: 10px; box-sizing: border-box; font-size: 12px; border-radius: 0 0 20px 20px;\">Questa &egrave; un'email autogenerata. Non rispondere a questa email.<br/></div></center>";

	$email->Body = $emailbody;
	$email->isHTML(true);
	$send = $email->Send();
	$email->SmtpClose();
	return $send;
}
?>
