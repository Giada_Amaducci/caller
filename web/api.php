<?php

    define("OUTPUT_ERROR_MSG", "Errore inaspettato, riprova.");
    $output = array("result" => "error", "message" => OUTPUT_ERROR_MSG);

    $_db = @mysqli_connect("localhost", "caller", "", "my_caller")or die("<center style='padding-top:50px;font-family:sans-serif;font-size:18px;'>Error during database connection :(</center>");
    mysqli_set_charset($_db, "utf8");

    function output(){
        global $output;
        header("Content-type: application/json");
        exit(json_encode($output));
    }

    function escape($string){
        global $_db;
        return mysqli_real_escape_string($_db, $string);
    }

    function password($password){
        // Salt + Email before hash
        return md5("[#CALLER-SALT-LEFT#]".$password."[#CALLER-SALT-RIGHT#]");
    }
    
    if(isset($_POST["type"])){
    	switch ($_POST["type"]){
            case 'forgot_password':
                if(!isset($_POST["email"]) || !isset($_POST["password"]) 
                    || trim($_POST["email"])=="" || trim($_POST["password"])==""){
                    $output["message"]="E-mail o password non inseriti!";
                }else{
                    $data=mysqli_query($_db, "SELECT id
                                                  FROM users 
                                                  WHERE email='".escape($_POST["email"])."'");
                    if(mysqli_num_rows($data)!=0){
                        $data = mysqli_fetch_assoc($data);
                        $id = $data["id"];
                        mysqli_query($_db, "UPDATE users SET password = '".escape(password($_POST["password"]))."' WHERE id = $id");
                        $output["result"]="success";
                        $output["message"]="Login eseguito con successo con successo!";
                    }else{
                        $output["message"]="L'E-mail inserita NON è valida!";
                    }  
                }
            break;
        	case 'registration':
                if(!isset($_POST["username"]) || !isset($_POST["password"]) 
                   || !isset($_POST["email"]) || !isset($_POST["image"])
                   || !isset($_POST["role"])
                   || trim($_POST["username"])=="" || trim($_POST["password"]=="")
                   || trim($_POST["email"])=="" || trim($_POST["image"])==""){
                    $output["message"]="Non hai compilato tutti i campi obbligatori";
                }else{
                    if(!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)){
                        $output["message"]="L'indirizzo email inserito non è valido";
                    }else{
                        if(mysqli_num_rows(mysqli_query($_db, "SELECT null FROM users WHERE email='".escape($_POST["email"])."'"))!=0){
                            $output["message"]="Esiste già un utente con questo indirizzo email";
                        }else{
                            if(mysqli_num_rows(mysqli_query($_db, "SELECT null FROM users WHERE username='".escape($_POST["username"])."'"))!=0){
                                $output["message"]="Esiste già un utente con questo username, scegli un username unico!";
                            }else{
                                mysqli_query($_db, "INSERT INTO users (email, password, username, image, number, role) VALUES ('".escape($_POST["email"])."', '".escape(password($_POST["password"]))."', '".escape($_POST["username"])."', '".escape($_POST["image"])."', '".escape($_POST["number"])."', '".escape($_POST["role"])."')");
                                $output["result"]="success";
                                $output["id"] = mysqli_insert_id($_db);
                                $output["message"]="Registrazione eseguita con successo!";
                            }
                        }
                    }
                }
                break;
            case 'login':
                if(!isset($_POST["email"]) || !isset($_POST["password"]) 
                    || trim($_POST["email"])=="" || trim($_POST["password"])==""){
                    $output["message"]="E-mail o password non inseriti!";
                }else{
                    $data=mysqli_query($_db, "SELECT username,
                                                         email,
                                                         image,
                                                         number,
                                                         role,
                                                         id
                                                  FROM users 
                                                  WHERE email='".escape($_POST["email"])."' 
                                                        and password='".escape(password($_POST["password"]))."'");
                    if(mysqli_num_rows($data)!=0){
                        $data=mysqli_fetch_assoc($data);
                        $output["user"] = $data;
                        $output["result"]="success";
                        $output["message"]="Login eseguito con successo con successo!";
                    }else{
                        $output["message"]="E-mail o password non corretti!";
                    }  
                }
                break;

            case 'getChats':
                if(!isset($_POST["user_id"])){
                    $output["message"] = "Errore inaspettato, riprova!";
                }else{
                    $data = mysqli_query($_db, "SELECT DISTINCT receiver_id
                                                FROM messages 
                                                WHERE sender_id='".escape($_POST["user_id"])."'");
                    if(mysqli_num_rows($data)!=0){
                    	while($row = mysqli_fetch_array($data, MYSQLI_ASSOC)){
                            $receiver_ids[] = $row["receiver_id"];
                        }
                    }

                    $data = mysqli_query($_db, "SELECT DISTINCT sender_id
                                                FROM messages 
                                                WHERE receiver_id='".escape($_POST["user_id"])."'");
			
                    if(mysqli_num_rows($data)!=0){
                    	while($row = mysqli_fetch_array($data, MYSQLI_ASSOC)){
                            $sender_ids[] = $row["sender_id"];
                        }
                    }

                    if(!isset($receiver_ids) && !isset($sender_ids)){
                        $output["result"] = "success";
                        $output["message"] = "Nessun messaggio";
                    }else{
                        foreach($receiver_ids as $receiver_id){
                            if (in_array($receiver_id, $sender_ids)) {
                                $data = mysqli_query($_db, "SELECT m.message as lastMessage
                                                            FROM messages m
                                                            WHERE (receiver_id='".escape($receiver_id)."' AND
                                                                sender_id ='".escape($_POST["user_id"])."') OR
                                                                (sender_id='".escape($receiver_id)."' AND
                                                                receiver_id ='".escape($_POST["user_id"])."')
                                                            ORDER BY createdAt DESC
                                                            LIMIT 1");
                                $data = mysqli_fetch_assoc($data);
                                $userData = mysqli_query($_db, "SELECT id as user_id,
                                                                       username as name,
                                                                       email,
                                                                       image
                                                            FROM users 
                                                            WHERE id = $receiver_id");
                                $userData = mysqli_fetch_assoc($userData);
                                $partialResult = array_merge($data, $userData);
                                $messages = mysqli_fetch_assoc(mysqli_query($_db, "SELECT COUNT(*) as messagesToRead
                                                                    FROM messages 
                                                                    WHERE receiver_id = '".escape($_POST["user_id"])."'
                                                                        AND sender_id = $receiver_id
                                                                        AND status = 'notRead'"));
                                $partialResult["messagesToRead"] = $messages["messagesToRead"];
                                $result[] = $partialResult;
                                unset($sender_ids[array_search($receiver_id,$sender_ids)]); 
                            }else{
                                $data = mysqli_query($_db, "SELECT u.id as user_id,
                                                                   u.username as name,
                                                                   m.message as lastMessage,
                                                                   u.email,
                                                                   u.image 
                                                            FROM messages m, users u
                                                            WHERE u.id = m.receiver_id AND
                                                                 m.receiver_id='".escape($receiver_id)."' AND
                                                                 m.sender_id ='".escape($_POST["user_id"])."'
                                                            ORDER BY createdAt DESC
                                                            LIMIT 1");
                                if(mysqli_num_rows($data)!=0){
                                    $message = mysqli_fetch_assoc($data);
                                    $message["messagesToRead"] = 0;
                                    $result[] = $message;
                                }
                            }
                        }
    
                        foreach($sender_ids as $sender_id){
                            $data = mysqli_query($_db, "SELECT u.id as user_id,
                                                                   u.username as name,
                                                                   m.message as lastMessage,
                                                                   u.image,
                                                                   u.email
                                                        FROM messages m, users u
                                                        WHERE u.id = $sender_id AND 
                                                            sender_id='".escape($sender_id)."' AND
                                                            receiver_id ='".escape($_POST["user_id"])."'
                                                        ORDER BY createdAt DESC
                                                        LIMIT 1");
                            if(mysqli_num_rows($data)!=0){
                                $message = mysqli_fetch_assoc($data);
                                $partialResult = $message;
                                $messages = mysqli_fetch_assoc(mysqli_query($_db, "SELECT COUNT(*) as messagesToRead
                                                                    FROM messages 
                                                                    WHERE receiver_id = '".escape($_POST["user_id"])."'
                                                                        AND sender_id = $sender_id
                                                                        AND status = 'notRead'"));
                                $partialResult["messagesToRead"] = $messages["messagesToRead"];
                                $result[] = $partialResult;
                            }
                        }
                        $output["values"] = JSON_encode($result);
                        $output["result"] = "success";
                    }

                }
                break;
            case 'updateUserData':
                if(!isset($_POST["oldEmail"])){
                    $output["message"]="Impossibile cambiare i dati dell'utente";
                }else{
                    $data = mysqli_query($_db, "SELECT id
                                                FROM users 
                                                WHERE email='".escape($_POST["oldEmail"])."'");
                    if(mysqli_num_rows($data)==0){
                        $output["message"]="NON esiste alcun utente con tale indirizzo email";
                    }else{
                        $userId = mysqli_fetch_assoc($data)["id"];
                        $error = false;
                        if(isset($_POST["email"])){
                            if(!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)){
                                $output["message"]="L'indirizzo email inserito non è valido";
                            }else{
                                $data = mysqli_query($_db, "SELECT null
                                                            FROM users 
                                                            WHERE email='".escape($_POST["email"])."'");
                                if(mysqli_num_rows($data)==0){
                                    mysqli_query($_db, "UPDATE users SET email='".escape($_POST["email"])."' WHERE id='$userId'");
                                    $output["result"]="success";
                                }else{
                                    $output["message"]="L'indirizzo email è già utilizzato da un altro utente!";
                                    $error = true;
                                }
                            }
                        }
                        if(isset($_POST["username"]) && !$error){
                            $data = mysqli_query($_db, "SELECT null
                                                            FROM users 
                                                            WHERE email='".escape($_POST["email"])."'");
                            if(mysqli_num_rows($data)==0){
                                mysqli_query($_db, "UPDATE users SET username='".escape($_POST["username"])."' WHERE id='$userId'");
                                $output["result"]="success";
                            }else{
                                $output["message"]="L'username inserito è già utilizzato da un altro utente!";
                                $error = true;
                            }
                        }
                        if(isset($_POST["password"]) && !$error){
                            mysqli_query($_db, "UPDATE users SET password='".escape(password($_POST["password"]))."' WHERE id='$userId'");
                            $output["result"]="success";
                        }
                        if(isset($_POST["number"]) && !$error){
                            mysqli_query($_db, "UPDATE users SET number='".escape($_POST["number"])."' WHERE id='$userId'");
                            $output["result"]="success";
                        }
                        if(isset($_POST["image"]) && !$error){
                            mysqli_query($_db, "UPDATE users SET image='".escape($_POST["image"])."' WHERE id='$userId'");
                            $output["result"]="success";
                        }
                        $data = mysqli_query($_db, "SELECT username, 
                                                            email,
                                                            number,
                                                            role
                                                    FROM users 
                                                    WHERE id='$userId'");
                        $data = mysqli_fetch_assoc($data);
                        $output["email"] = $data["email"];
                        $output["number"] = $data["number"];
                        $output["username"] = $data["username"];
                        $output["role"] = $data["role"];
                    }
                }
                break;
            case 'searchUser':
                if(!isset($_POST["stringToSearch"]) || !isset($_POST["with"])){
                    $output["message"] = "Errore inaspettato, riprova!";
                }else{
                    if($_POST["with"] == "username"){
                        $data = mysqli_query($_db, "SELECT id as user_id,
                                                           username as name,
                                                           email,
                                                           number,
                                                           image
                                                    FROM users 
                                                    WHERE username LIKE '".escape($_POST["stringToSearch"])."%'");
                    }else{
                        $data = mysqli_query($_db, "SELECT id as user_id,
                                                            username as name,
                                                            email,
                                                            number,
                                                            image
                                                    FROM users 
                                                    WHERE number='".escape($_POST["stringToSearch"])."'");
                    }
                    if(mysqli_num_rows($data)==0){
                        $output["message"] = "Nessun utente";
                        $output["result"] = "success";
                    }else{
                        $data = mysqli_fetch_all($data, MYSQLI_ASSOC);
                        $output["values"] = JSON_encode($data);
                        $output["result"] = "success";
                    }
                }
                break;
            case 'getMessages':
                if(!isset($_POST["userId"]) || !isset($_POST["id"])){
                    $output["message"] = "Errore inaspettato, riprova!";
                }else{
                    $userOneData = mysqli_query($_db, "SELECT null 
                                                    FROM users 
                                                    WHERE id = '".escape($_POST["id"])."'");
                    $userTwoData = mysqli_query($_db, "SELECT null 
                                                    FROM users 
                                                    WHERE id = '".escape($_POST["userId"])."'");
                    if(mysqli_num_rows($userOneData)==0 || mysqli_num_rows($userTwoData)==0){
                        $output["message"] = "Nessun utente";
                    }else{
                        $messagesData = mysqli_query($_db, "SELECT * 
                                                            FROM messages 
                                                            WHERE (sender_id = '".escape($_POST["id"])."' AND receiver_id = '".escape($_POST["userId"])."') 
                                                                OR (sender_id = '".escape($_POST["userId"])."' AND receiver_id = '".escape($_POST["id"])."')
                                                            ORDER BY createdAt ASC");
                        $data = mysqli_fetch_all($messagesData, MYSQLI_ASSOC);

                        $messagesData = mysqli_query($_db, "SELECT * 
                                                            FROM messages 
                                                            WHERE (sender_id = '".escape($_POST["userId"])."' AND receiver_id = '".escape($_POST["id"])."')
                                                            ORDER BY createdAt ASC");
                        $messagesData = mysqli_fetch_all($messagesData, MYSQLI_ASSOC);
                        
                        foreach($messagesData as $message){
                            $message_id = $message["id"];
                            mysqli_query($_db, "UPDATE messages SET status = 'read' WHERE id = '$message_id'");
                        }

                        $output["values"] = JSON_encode($data);
                        $output["result"] = "success";
                    }
                }
                break; 
            case 'getNumberOfMessagesToRead':
                if(!isset($_POST["user_id"])){
                    $output["message"] = "Errore inaspettato, riprova!";
                }else{
                    $messagesData = mysqli_query($_db, "SELECT COUNT(*) as messagesToRead
                                                    FROM messages 
                                                    WHERE receiver_id = '".escape($_POST["user_id"])."'
                                                          AND status = 'notRead'");
                    $message=mysqli_fetch_assoc($messagesData);
                    $output["messagesToRead"] = $message["messagesToRead"];
                    $output["result"] = "success";
                }
                break; 
            case 'addMessage':
                if(!isset($_POST["userId"]) || !isset($_POST["id"]) || !isset($_POST["message"])){
                    $output["message"] = "Errore inaspettato, riprova!";
                }else{
                    $userOneData = mysqli_query($_db, "SELECT null 
                                                    FROM users 
                                                    WHERE id = '".escape($_POST["id"])."'");
                    $userTwoData = mysqli_query($_db, "SELECT null 
                                                    FROM users 
                                                    WHERE id = '".escape($_POST["userId"])."'");
                    if(mysqli_num_rows($userOneData)==0 || mysqli_num_rows($userTwoData)==0){
                        $output["message"] = "Nessun utente";
                    }else{
                    	$dateTime = date("Y-m-d H:i:s");
                        mysqli_query($_db, "INSERT INTO messages (sender_id, receiver_id, message, createdAt, status) VALUES ('".escape($_POST["id"])."', '".escape($_POST["userId"])."', '".escape($_POST["message"])."', '$dateTime', 'notRead')");
                        $output["result"] = "success";
                        $output["createdAt"] = $dateTime;
                        $output["message"]="Messaggio inviato!";
                    }
                }
                break; 
            case 'getPlaceImages':
                if(!isset($_POST["city"]) || !isset($_POST["name"]) || !isset($_POST["road"]) || !isset($_POST["user_id"])){
                    $output["message"] = "Errore inaspettato, riprova!";
                }else{
                    $data = mysqli_query($_db, "SELECT id 
                                                    FROM places 
                                                    WHERE name = '".escape($_POST["name"])."' AND
                                                          city = '".escape($_POST["city"])."' AND
                                                          road = '".escape($_POST["road"])."'");
                    if(mysqli_num_rows($data)==0){
                        $output["message"] = "nessuna foto";
                        $output["result"] = "success";
                    }else{
                        $data = mysqli_fetch_assoc($data);
                        $place_id = $data["id"];
                        $data = mysqli_query($_db, "SELECT i.id as image_id
                                                FROM images i, places p, visitedPlaces v 
                                                WHERE p.id = v.place_id AND 
                                                      i.visited_place_id = v.id AND
                                                      p.id='".escape($place_id)."'");
                        if(mysqli_num_rows($data)!=0){
                            while($row = mysqli_fetch_array($data, MYSQLI_ASSOC)){
                                $image_ids[] = $row["image_id"];
                            }
                        }
                        foreach($image_ids as $image_id){
                            $data = mysqli_query($_db, "SELECT COUNT(*) as vote
                                                        FROM votes
                                                        WHERE image_id = '".escape($image_id)."'");
                            $data = mysqli_fetch_assoc($data);
                            $imageData = mysqli_query($_db, "SELECT id, image
                                                        FROM images
                                                        WHERE id = '".escape($image_id)."'");
                            $imageData = mysqli_fetch_assoc($imageData);
                            $partialResult = array_merge($data, $imageData);  
                            $data = mysqli_query($_db, "SELECT null
                                                        FROM votes
                                                        WHERE image_id = '".escape($image_id)."' AND
                                                              user_id = '".escape($_POST["user_id"])."'");
                            if(mysqli_num_rows($data)!=0){
                                $partialResult["isVoted"] = '1';
                            }else{
                                $partialResult["isVoted"] = '0';
                            }

                            $result[] = $partialResult;
                        }

                        $vote = array_column($result, 'vote');
                        array_multisort($vote, SORT_DESC, $result);
                        $output["values"] = JSON_encode($result);
                        $output["result"] = "success";
                    }
                }
                break;
            case 'addOrDeleteVote':
                if(!isset($_POST["image_id"]) || !isset($_POST["action"]) || !isset($_POST["user_id"])){
                    $output["message"] = "Errore inaspettato, riprova!";
                }else{
                    if($_POST["action"] == "delete"){
                        mysqli_query($_db, "DELETE FROM votes WHERE user_id = '".escape($_POST["user_id"])."' AND image_id = '".escape($_POST["image_id"])."'");
                        $output["message"] = "Eliminato correttamente il tuo voto a tale foto!";
                    }else{
                        mysqli_query($_db, "INSERT INTO votes (image_id, user_id) VALUES ('".escape($_POST["image_id"])."', '".escape($_POST["user_id"])."')");
                        $output["message"] = "Aggiunto correttamente il tuo voto a tale foto!";
                    }
                }
                $output["result"] = "success";
                break;
            case 'getVisitedPlaces':
                if(!isset($_POST["user_id"]) || !isset($_POST["city"])){
                    $output["message"] = "Errore inaspettato, riprova!";
                }else{
                    $data = mysqli_query($_db, "SELECT v.id as visited_place_id
                                                FROM visitedPlaces v, places p
                                                WHERE v.place_id = p.id AND 
                                                      v.user_id = '".escape($_POST["user_id"])."' AND
                                                      p.city = '".escape($_POST["city"])."'");
                    if(mysqli_num_rows($data)==0){
                        $output["result"] = "success";
                        $output["message"] = "Nessun posto visitato";
                    }else{

                        while($row = mysqli_fetch_array($data, MYSQLI_ASSOC)){
                            $visited_place_ids[] = $row["visited_place_id"];
                        }    

                        foreach($visited_place_ids as $visited_place_id){
                            $data = mysqli_query($_db, "SELECT v.id as visited_place_id, p.name, p.road, p.address, p.city, v.prefered, p.lat, p.lon, v.visited_date as date
                                                        FROM places p, visitedPlaces v
                                                        WHERE p.id = v.place_id AND 
                                                              v.id = '".escape($visited_place_id)."'");
                            $data = mysqli_fetch_assoc($data);
                            $imageData = mysqli_query($_db, "SELECT i.id as image_id, i.image
                                                            FROM images i
                                                            WHERE i.visited_place_id = '".escape($visited_place_id)."'
                                                            ORDER BY RAND() 
                                                            LIMIT 1");
                            if(mysqli_num_rows($imageData)==0){
                                $result[] = $data;
                            }else{
                                $imageData = mysqli_fetch_assoc($imageData);
                                $result[] = array_merge($data, $imageData);
                            }
                        }

                        $output["values"] = JSON_encode($result);
                        $output["result"] = "success";
                    }
                }
                break;
            case 'addVisitedPlace':
                if(!isset($_POST["user_id"]) || !isset($_POST["name"]) || !isset($_POST["road"])
                    || !isset($_POST["address"]) || !isset($_POST["city"]) || !isset($_POST["lat"])
                    || !isset($_POST["lon"])){
                    $output["message"] = "Errore inaspettato, riprova!";
                }else{
                    $data = mysqli_query($_db, "SELECT id 
                                                FROM places 
                                                WHERE name = '".escape($_POST["name"])."' AND
                                                        city = '".escape($_POST["city"])."' AND
                                                        road = '".escape($_POST["road"])."'");
                    if(mysqli_num_rows($data)==0){
                        mysqli_query($_db, "INSERT INTO places(name,city,road,address,lat,lon) 
                                            VALUES ('".escape($_POST["name"])."', '".escape($_POST["city"])."', 
                                                    '".escape($_POST["road"])."', '".escape($_POST["address"])."',
                                                    '".escape($_POST["lat"])."', '".escape($_POST["lon"])."')");
                        $place_id = mysqli_insert_id($_db);
                    }else{
                        $data = mysqli_fetch_assoc($data);
                        $place_id = $data["id"];
                    }

                    mysqli_query($_db, "INSERT INTO visitedPlaces(user_id, place_id, visited_date, prefered) 
                                            VALUES ('".escape($_POST["user_id"])."', '".escape($place_id)."', 
                                                    CURDATE(), '0')");
                    
                    $output["message"] = "Luogo aggiunto correttamente ai luoghi visitati!";
                    $output["result"] = "success";
                }
                break;
            case 'addOrDeleteFavorite':
                if(!isset($_POST["place_id"]) || !isset($_POST["action"])){
                    $output["message"] = "Errore inaspettato, riprova!";
                }else{
                    if($_POST["action"] == "delete"){
                        mysqli_query($_db, "UPDATE visitedPlaces SET prefered = '0' WHERE id = '".escape($_POST["place_id"])."'");
                        $output["message"] = "Eliminato correttamente dai luoghi preferiti!";
                    }else{
                        mysqli_query($_db, "UPDATE visitedPlaces SET prefered = '1' WHERE id = '".escape($_POST["place_id"])."'");
                        $output["message"] = "Aggiunto correttamente ai luoghi preferiti!";
                    }
                }
                $output["result"] = "success";
                break;
            case 'getVisitedPlaceImages':
                if(!isset($_POST["visited_place_id"])){
                    $output["message"] = "Errore inaspettato, riprova!";
                }else{
                    $data = mysqli_query($_db, "SELECT id, image 
                                                FROM images 
                                                WHERE visited_place_id = '".escape($_POST["visited_place_id"])."'");
                    if(mysqli_num_rows($data)==0){
                        $output["message"] = "nessuna foto";
                    }else{
                        $output["values"] = mysqli_fetch_all($data, MYSQLI_ASSOC);
                    }
                    $output["result"] = "success";
                }
                break;
            case 'addVisitedPlaceImage':
                if(!isset($_POST["visited_place_id"]) || !isset($_POST["image"])){
                    $output["message"] = "Errore inaspettato, riprova!";
                }else{
                    mysqli_query($_db, "INSERT INTO images(visited_place_id, image) VALUES ('".escape($_POST["visited_place_id"])."', '".escape($_POST["image"])."')");
                    $output["image_id"] = mysqli_insert_id($_db);
                    $output["image"] = $_POST["image"];
                    $output["result"] = "success";
                }
                break;
            case 'deleteImagePlace':
                if(!isset($_POST["image_id"])){
                    $output["message"] = "Errore inaspettato, riprova!";
                }else{
                    mysqli_query($_db, "DELETE FROM votes WHERE image_id = '".escape($_POST["image_id"])."'");
                    mysqli_query($_db, "DELETE FROM images WHERE id = '".escape($_POST["image_id"])."'");
                    $output["message"] = "Immagine cancellata con successo!";
                    $output["result"] = "success";
                }
                break;
            case 'getVisitedPlaceReview':
                if(!isset($_POST["visited_place_id"])){
                    $output["message"] = "Errore inaspettato, riprova!";
                }else{
                    $data = mysqli_query($_db, "SELECT vote, message 
                                                FROM reviews 
                                                WHERE visited_place_id = '".escape($_POST["visited_place_id"])."'");
                    if(mysqli_num_rows($data)==0){
                        $output["message"] = "nessuna recensione";
                    }else{
                        $output["place"] = mysqli_fetch_assoc($data);
                    }
                    $output["result"] = "success";
                }
                break;
            case 'updateVisitedPlace':
                if(!isset($_POST["visited_place_id"])){
                    $output["message"] = "Errore inaspettato, riprova!";
                }else{
                    $data = mysqli_query($_db, "SELECT null 
                                                FROM reviews 
                                                WHERE visited_place_id = '".escape($_POST["visited_place_id"])."'");
                    
                    if(mysqli_num_rows($data)==0){
                        if(!isset($_POST["vote"]) || !isset($_POST["note"])){
                           $output["message"] = "Per inserire una recensione devi dare un voto ed aggiungere un commento!";
                        }else{
                            mysqli_query($_db, "INSERT INTO reviews(vote, visited_place_id, message) VALUES ('".escape($_POST["vote"])."','".escape($_POST["visited_place_id"])."','".escape($_POST["note"])."')");
                            if(isset($_POST["date"])){
                                mysqli_query($_db, "UPDATE visitedPlaces SET visited_date = '".escape($_POST["date"])."' WHERE id = '".escape($_POST["visited_place_id"])."'");
                            }
                            $output["result"] = "success";
                        }
                    }else{
                        if(isset($_POST["date"])){
                            mysqli_query($_db, "UPDATE visitedPlaces SET visited_date = '".escape($_POST["date"])."' WHERE id = '".escape($_POST["visited_place_id"])."'");
                        }
                        if(isset($_POST["note"])){
                            mysqli_query($_db, "UPDATE reviews SET message = '".escape($_POST["note"])."' WHERE visited_place_id = '".escape($_POST["visited_place_id"])."'");
                        }
                        if(isset($_POST["vote"])){
                            mysqli_query($_db, "UPDATE reviews SET vote = '".escape($_POST["vote"])."' WHERE visited_place_id = '".escape($_POST["visited_place_id"])."'");
                        }
                        $output["result"] = "success";
                    }
                }
                break;
            case 'getReviews':
                if(isset($_POST["city"]) && isset($_POST["name"]) && isset($_POST["road"])){
                    $data = mysqli_query($_db, "SELECT id 
                                                FROM places 
                                                WHERE name = '".escape($_POST["name"])."' AND
                                                        city = '".escape($_POST["city"])."' AND
                                                        road = '".escape($_POST["road"])."'");
                    if(mysqli_num_rows($data)==0){
                        $output["message"] = "nessuna recensione";
                        $output["result"] = "success";
                    }else{
                        $data = mysqli_fetch_assoc($data);
                        $place_id = $data["id"];
                        $data = mysqli_query($_db, "SELECT v.id as visited_place_id
                                                    FROM visitedPlaces v 
                                                    WHERE v.place_id = '".escape($place_id)."'");
                        if(mysqli_num_rows($data)!=0){
                            while($row = mysqli_fetch_array($data, MYSQLI_ASSOC)){
                                $visited_places_ids[] = $row["visited_place_id"];
                            }
                        }
                        foreach($visited_places_ids as $visited_place_id){
                            $reviewData = mysqli_query($_db, "SELECT vote, message
                                                        FROM reviews
                                                        WHERE id = '".escape($visited_place_id)."'");
                            if(mysqli_num_rows($reviewData)!=0){
                                $reviewData = mysqli_fetch_assoc($reviewData);
                                $userData = mysqli_query($_db, "SELECT u.username, u.email as user_email, u.id as user_id, u.image as user_image
                                                        FROM users u, visitedPlaces v
                                                        WHERE u.id = v.user_id AND
                                                              v.id = '".escape($visited_place_id)."'");
                                $userData = mysqli_fetch_assoc($userData);
                                $result[] = array_merge($userData, $reviewData); 
                            }
                        }

                        if(!isset($result)){
                            $output["message"] = "nessuna recensione";
                        }else{
                            $output["values"] = JSON_encode($result);
                        }
                        $output["result"] = "success";    
                    }
                }elseif(isset($_POST["itinerary_id"])){
                    $data = mysqli_query($_db, "SELECT r.vote, r.message, u.username, u.email as user_email, u.id as user_id, u.image as user_image
                                                FROM users u, itineraryReviews r, itineraryUser iu
                                                WHERE u.id = iu.user_id AND
                                                        r.itinerary_user_id = iu.id AND
                                                        iu.itinerary_id = '".escape($_POST["itinerary_id"])."'");
                    $result = mysqli_fetch_all($data, MYSQLI_ASSOC);

                    if(mysqli_num_rows($data)==0){
                        $output["message"] = "nessuna recensione";
                    }else{
                        $output["values"] = JSON_encode($result);
                    }
                    $output["result"] = "success"; 
                    
                }else{
                    $output["message"] = "Errore inaspettato, riprova!";
                }
                break;
            case 'getFavoritePlaces':
                if(!isset($_POST["user_id"])){
                    $output["message"] = "Errore inaspettato, riprova!";
                }else{
                    $data = mysqli_query($_db, "SELECT v.id as visited_place_id
                                                FROM visitedPlaces v, places p
                                                WHERE v.place_id = p.id AND 
                                                        v.user_id = '".escape($_POST["user_id"])."' AND 
                                                        v.prefered = '1'");
                    if(mysqli_num_rows($data)==0){
                        $output["message"] = "Nessun posto visitato";
                    }else{

                        while($row = mysqli_fetch_array($data, MYSQLI_ASSOC)){
                            $visited_place_ids[] = $row["visited_place_id"];
                        }    

                        foreach($visited_place_ids as $visited_place_id){
                            $data = mysqli_query($_db, "SELECT v.id as visited_place_id, p.name, p.road, p.address, p.city, v.prefered, p.lat, p.lon, v.visited_date as date
                                                        FROM places p, visitedPlaces v
                                                        WHERE p.id = v.place_id AND 
                                                                v.id = '".escape($visited_place_id)."'");
                            $data = mysqli_fetch_assoc($data);
                            $imageData = mysqli_query($_db, "SELECT i.id as image_id, i.image
                                                            FROM images i
                                                            WHERE i.visited_place_id = '".escape($visited_place_id)."'
                                                            ORDER BY RAND() 
                                                            LIMIT 1");
                            if(mysqli_num_rows($imageData)==0){
                                $result[] = $data;
                            }else{
                                $imageData = mysqli_fetch_assoc($imageData);
                                $result[] = array_merge($data, $imageData);
                            }
                        }

                        if(isset($result)){
                            $output["values"] = JSON_encode($result);
                        }else{
                            $output["message"] = "Nessun posto visitato";
                        }
                    }
                    
                    $output["result"] = "success";
                }
                break;
            case 'getUserOrOrganizerItineraries':
                if(!isset($_POST["user_id"]) || !isset($_POST["city"]) 
                    || !isset($_POST["role"]) || !isset($_POST["only_created_itinerary"])){
                    $output["message"] = "Errore inaspettato, riprova!";
                }else{
                    if($_POST["role"] == "user"){
                        if($_POST["only_created_itinerary"] == "yes"){
                            $itineraryData = mysqli_query($_db, "SELECT i.id as itinerary_id, i.name, iu.departure_date, iu.return_date, u.username as creator_name
                                                                FROM itineraries i, itineraryUser iu, users u
                                                                WHERE i.id = iu.itinerary_id AND 
                                                                    i.creator_id = u.id AND
                                                                    i.creator_id = '".escape($_POST["user_id"])."' AND
                                                                    i.city = '".escape($_POST["city"])."'");
                            $result = mysqli_fetch_all($itineraryData, MYSQLI_ASSOC);
                        }else{
                            $data = mysqli_query($_db, "SELECT i.id as itinerary_id, i.name, iu.departure_date, iu.return_date, u.username as creator_name
                                                FROM itineraries i, itineraryUser iu, users u
                                                WHERE i.id = iu.itinerary_id AND 
                                                        i.creator_id = u.id AND
                                                        iu.user_id = '".escape($_POST["user_id"])."' AND 
                                                        i.city = '".escape($_POST["city"])."'");
                            $result = mysqli_fetch_all($data, MYSQLI_ASSOC);
                        }
                    }else{
                        $data = mysqli_query($_db, "SELECT i.id as itinerary_id
                                                FROM itineraries i
                                                WHERE i.creator_id = '".escape($_POST["user_id"])."' AND 
                                                        i.city = '".escape($_POST["city"])."'");

                        while($row = mysqli_fetch_array($data, MYSQLI_ASSOC)){
                            $itinerary_ids[] = $row["itinerary_id"];
                        }    

                        foreach($itinerary_ids as $itinerary_id){
                            $itineraryData = mysqli_query($_db, "SELECT i.id as itinerary_id, i.name
                                                                FROM itineraries i
                                                                WHERE i.id = $itinerary_id");
                                                                
                            $itineraryData = mysqli_fetch_assoc($itineraryData);
                            $countData = mysqli_query($_db, "SELECT COUNT(*) AS user_count
                                                            FROM itineraryUser iu
                                                            WHERE iu.itinerary_id = $itinerary_id");
                            $countData = mysqli_fetch_assoc($countData);
                            $result[] = array_merge($itineraryData, $countData);
                        }
                    }
                    
                    if(mysqli_num_rows($data)==0){
                        $output["message"] = "Nessun itinerario creato";
                    }else{
                        $output["values"] = $result;
                    }
                    
                    $output["result"] = "success";
                }
                break;
            case 'addItineraryUser':
                if(!isset($_POST["title"]) || !isset($_POST["departureDate"]) || 
                    !isset($_POST["returnDate"]) || !isset($_POST["user_id"]) ||
                    !isset($_POST["city"])){
                    $output["message"]="Non hai inserito tutti i dati!";
                }else{
                    $departureDate = date('Y-m-d',strtotime($_POST["departureDate"]));
                    $returnDate = date('Y-m-d',strtotime($_POST["returnDate"]));
                    $query = "SELECT i.id
                            FROM itineraries i, itineraryUser iu
                            WHERE i.id = iu.itinerary_id
                                and i.name = '".escape($_POST["title"])."'
                                and ui.user_id = '".escape($_POST["user_id"])."'
                                and i.city = '".escape($_POST["city"])."'";
                    if(mysqli_num_rows(mysqli_query($_db, $query))!=0){
                        $output["message"]="Hai già svolto un itinerario con le stesse caratteristiche";
                    }else{
                        $itineraryData = mysqli_query($_db, "SELECT id
                                                    FROM itineraries
                                                    WHERE name = '".escape($_POST["title"])."' AND 
                                                          city = '".escape($_POST["city"])."'");
                        if(mysqli_num_rows($itineraryData)!=0){
                             $itinerary_id = mysqli_fetch_assoc($itineraryData)["id"];
                        }else{
                            mysqli_query($_db, "INSERT INTO itineraries(name, city, creator_id) VALUES ('".escape($_POST["title"])."', '".escape($_POST["city"])."', '".escape($_POST["user_id"])."')");
                            $itinerary_id = mysqli_insert_id($_db);
                        }
                        mysqli_query($_db, "INSERT INTO itineraryUser(departure_date, return_date, user_id, itinerary_id) VALUES ('$departureDate', '$returnDate', '".escape($_POST["user_id"])."', $itinerary_id)");
                        if(isset($_POST["places"])){
                            $places = json_decode($_POST["places"], true);
                            for($i=0; $i<count($places); $i++){
                                $place = $places[$i];
                                $data = mysqli_query($_db, "SELECT id 
                                                            FROM places 
                                                            WHERE name = '".escape($place["name"])."' AND
                                                                    city = '".escape($place["city"])."' AND
                                                                    road = '".escape($place["road"])."'");
                                 if(mysqli_num_rows($data)==0){
                                    mysqli_query($_db, "INSERT INTO places(name,city,road,address,lat,lon) 
                                                        VALUES ('".escape($place["name"])."', '".escape($place["city"])."', 
                                                                '".escape($place["road"])."', '".escape($place["address"])."',
                                                                '".escape($place["lat"])."', '".escape($place["lon"])."')");
                                    $place_id = mysqli_insert_id($_db);
                                }else{
                                    $data = mysqli_fetch_assoc($data);
                                    $place_id = $data["id"];
                                }
                                mysqli_query($_db, "INSERT INTO placesInItinerary(order_place, place_id, itinerary_id) VALUES ($i+1, $place_id, $itinerary_id)");
                            }
                        }
                        $output["result"]="success";
                        $output["message"]="Itinerario aggiunto con successo!";
                    }
                }
                break;
            case 'getPlacesInItinerary':
                if(!isset($_POST["itinerary_id"])){
                    $output["message"] = "Errore inaspettato, riprova!";
                }else{
                    $data = mysqli_query($_db, "SELECT p.id as place_in_itinerary_id
                                                FROM placesInItinerary p
                                                WHERE p.itinerary_id = '".escape($_POST["itinerary_id"])."'");
                    if(mysqli_num_rows($data)==0){
                        $output["message"] = "nessun luogo";
                    }else{

                        while($row = mysqli_fetch_array($data, MYSQLI_ASSOC)){
                            $place_in_itinerary_ids[] = $row["place_in_itinerary_id"];
                        }    

                        foreach($place_in_itinerary_ids as $place_in_itinerary_id){
                            $data = mysqli_query($_db, "SELECT pii.id as place_in_itinerary_id, p.name as place_name, p.road, p.address, p.city, p.lat, p.lon
                                                        FROM places p, placesInItinerary pii
                                                        WHERE p.id = pii.place_id AND 
                                                                pii.id = '".escape($place_in_itinerary_id)."'");
                            $result[] = mysqli_fetch_assoc($data);
                        }

                        if(isset($result)){
                            $output["values"] = JSON_encode($result);
                        }else{
                            $output["message"] = "nessun luogo";
                        }
                    }
                    
                    $output["result"] = "success";
                }
                break;
            case 'getVisitedItineraryImages':
                if(!isset($_POST["itinerary_id"]) || !isset($_POST["user_id"])){
                    $output["message"] = "Errore inaspettato, riprova!";
                }else{
                    $data = mysqli_query($_db, "SELECT id 
                                                    FROM itineraryUser  
                                                    WHERE itinerary_id = '".escape($_POST["itinerary_id"])."' AND
                                                          user_id = '".escape($_POST["user_id"])."'");
                
                    $itinerary_id = mysqli_fetch_assoc($data)["id"];  

                    $data = mysqli_query($_db, "SELECT id, image
                                                FROM itineraryImages
                                                WHERE itinerary_user_id	 = '".escape($itinerary_id)."'");
                    
                    if(mysqli_num_rows($data)==0){
                        $output["message"] = "nessuna foto";
                    }else{
                        $output["values"] = mysqli_fetch_all($data, MYSQLI_ASSOC);
                    }
                    $output["result"] = "success";
                }
                break;
            case 'addVisitedItineraryImage':
                if(!isset($_POST["itinerary_id"]) || !isset($_POST["user_id"]) || !isset($_POST["image"])){
                    $output["message"] = "Errore inaspettato, riprova!";
                }else{
                    $data = mysqli_query($_db, "SELECT id 
                                                FROM itineraryUser  
                                                WHERE itinerary_id = '".escape($_POST["itinerary_id"])."' AND
                                                        user_id = '".escape($_POST["user_id"])."'");
                
                    $itinerary_id = mysqli_fetch_assoc($data)["id"];  

                    mysqli_query($_db, "INSERT INTO itineraryImages(itinerary_user_id, image) VALUES ('$itinerary_id', '".escape($_POST["image"])."')");
                    $output["image_id"] = mysqli_insert_id($_db);
                    $output["image"] = $_POST["image"];
                    $output["result"] = "success";
                }
                break;
            case 'updateVisitedItinerary':
                if(!isset($_POST["itinerary_id"]) || !isset($_POST["user_id"])){
                    $output["message"] = "Errore inaspettato, riprova!";
                }else{
                    
                    $data = mysqli_query($_db, "SELECT id 
                                                FROM itineraryUser  
                                                WHERE itinerary_id = '".escape($_POST["itinerary_id"])."' AND
                                                        user_id = '".escape($_POST["user_id"])."'");
                
                    $itinerary_id = mysqli_fetch_assoc($data)["id"];  

                    if(isset($_POST["departureDate"])){
                        mysqli_query($_db, "UPDATE itineraryUser SET departure_date = '".escape($_POST["departureDate"])."' WHERE id = '$itinerary_id'");
                    }

                    if(isset($_POST["returnDate"])){
                        mysqli_query($_db, "UPDATE itineraryUser SET return_date = '".escape($_POST["returnDate"])."' WHERE id = '$itinerary_id'");
                    }

                    if(isset($_POST["places"])){
                        $places = json_decode($_POST["places"], true);
                        
                        $data = mysqli_query($_db, "SELECT id 
                                                FROM placesInItinerary
                                                WHERE itinerary_id = '".escape($_POST["itinerary_id"])."'");

                        if(mysqli_num_rows($data)!=0){
                            while($row = mysqli_fetch_array($data, MYSQLI_ASSOC)){
                                $places_in_itinerary_ids[] = $row["id"];
                            }
                        }

                        for($i=0; $i<count($places); $i++){
                            $place = $places[$i];

                            if (in_array($place["id"], $places_in_itinerary_ids)) {
                                unset($places_in_itinerary_ids[array_search($place["id"],$places_in_itinerary_ids)]); 
                            }else{
                                $data = mysqli_query($_db, "SELECT id 
                                                            FROM places 
                                                            WHERE name = '".escape($place["name"])."' AND
                                                                    city = '".escape($place["city"])."' AND
                                                                    road = '".escape($place["road"])."'");
                                 if(mysqli_num_rows($data)==0){
                                    mysqli_query($_db, "INSERT INTO places(name,city,road,address,lat,lon) 
                                                        VALUES ('".escape($place["name"])."', '".escape($place["city"])."', 
                                                                '".escape($place["road"])."', '".escape($place["address"])."',
                                                                '".escape($place["lat"])."', '".escape($place["lon"])."')");
                                    $place_id = mysqli_insert_id($_db);
                                }else{
                                    $data = mysqli_fetch_assoc($data);
                                    $place_id = $data["id"];
                                }
                                mysqli_query($_db, "INSERT INTO placesInItinerary(order_place, place_id, itinerary_id) VALUES ($i+1, $place_id, '".escape($_POST["itinerary_id"])."')");
                            }
                        }

                        if(count($places_in_itinerary_ids) != 0){
                            foreach($places_in_itinerary_ids as $place_in_itinerary_id){
                                mysqli_query($_db, "DELETE FROM placesInItinerary WHERE id = $place_in_itinerary_id");
                            }
                        }
                    }
                    
                    $output["result"] = "success";
                }
                break;
            case 'getItineraryReview':
                if(!isset($_POST["itinerary_id"]) || !isset($_POST["user_id"])){
                    $output["message"] = "Errore inaspettato, riprova!";
                }else{
                    $data = mysqli_query($_db, "SELECT id 
                                                    FROM itineraryUser  
                                                    WHERE itinerary_id = '".escape($_POST["itinerary_id"])."' AND
                                                          user_id = '".escape($_POST["user_id"])."'");
                
                    $itinerary_id = mysqli_fetch_assoc($data)["id"];  

                    $data = mysqli_query($_db, "SELECT vote, message
                                                FROM itineraryReviews
                                                WHERE itinerary_user_id	 = '".escape($itinerary_id)."'");
                    
                    if(mysqli_num_rows($data)==0){
                        $output["message"] = "nessuna recensione";
                    }else{
                        $output["itinerary"] = mysqli_fetch_assoc($data);
                    }
                    $output["result"] = "success";
                }
                break;
            case 'addOrUpdateItineraryReview':
                if(!isset($_POST["itinerary_id"]) || !isset($_POST["user_id"]) || 
                    !isset($_POST["vote"]) || !isset($_POST["message"])){
                    $output["message"] = "Errore inaspettato, riprova!";
                }else{
                    $data = mysqli_query($_db, "SELECT id 
                                                    FROM itineraryUser  
                                                    WHERE itinerary_id = '".escape($_POST["itinerary_id"])."' AND
                                                            user_id = '".escape($_POST["user_id"])."'");
                
                    $itinerary_id = mysqli_fetch_assoc($data)["id"];  

                    $data = mysqli_query($_db, "SELECT *
                                                FROM itineraryReviews
                                                WHERE itinerary_user_id	 = '".escape($itinerary_id)."'");
                    
                    if(mysqli_num_rows($data)==0){
                        mysqli_query($_db, "INSERT INTO itineraryReviews(itinerary_user_id, vote, message) VALUES ('$itinerary_id', '".escape($_POST["vote"])."', '".escape($_POST["message"])."')");
                    }else{
                        mysqli_query($_db, "UPDATE itineraryReviews SET message = '".escape($_POST["message"])."' WHERE itinerary_user_id = 'itinerary_id'");
                        mysqli_query($_db, "UPDATE itineraryReviews SET vote = '".escape($_POST["vote"])."' WHERE itinerary_user_id = 'itinerary_id'");
                    }
                    $output["result"] = "success";
                }
                break;
            case 'addItineraryUser':
                if(!isset($_POST["title"]) || !isset($_POST["user_id"]) || !isset($_POST["city"])){
                    $output["message"]="Non hai inserito tutti i dati!";
                }else{
                    $itineraryData = mysqli_query($_db, "SELECT id
                                                    FROM itineraries
                                                    WHERE name = '".escape($_POST["title"])."' AND 
                                                            city = '".escape($_POST["city"])."'");
                    if(mysqli_num_rows(mysqli_query($_db, $query))!=0){
                        $output["message"]="È già stato creato un itinerario con queste caratteristiche";
                    }else{
                        mysqli_query($_db, "INSERT INTO itineraries(name, city, creator_id) VALUES ('".escape($_POST["title"])."', '".escape($_POST["city"])."', '".escape($_POST["user_id"])."')");
                        $itinerary_id = mysqli_insert_id($_db);
                        if(isset($_POST["places"])){
                            $places = json_decode($_POST["places"], true);
                            for($i=0; $i<count($places); $i++){
                                $place = $places[$i];
                                $data = mysqli_query($_db, "SELECT id 
                                                            FROM places 
                                                            WHERE name = '".escape($place["name"])."' AND
                                                                    city = '".escape($place["city"])."' AND
                                                                    road = '".escape($place["road"])."'");
                                if(mysqli_num_rows($data)==0){
                                    mysqli_query($_db, "INSERT INTO places(name,city,road,address,lat,lon) 
                                                        VALUES ('".escape($place["name"])."', '".escape($place["city"])."', 
                                                                '".escape($place["road"])."', '".escape($place["address"])."',
                                                                '".escape($place["lat"])."', '".escape($place["lon"])."')");
                                    $place_id = mysqli_insert_id($_db);
                                }else{
                                    $data = mysqli_fetch_assoc($data);
                                    $place_id = $data["id"];
                                }
                                mysqli_query($_db, "INSERT INTO placesInItinerary(order_place, place_id, itinerary_id) VALUES ($i+1, $place_id, $itinerary_id)");
                            }
                        }
                        $output["result"]="success";
                        $output["message"]="Itinerario aggiunto con successo!";
                    }
                }
                break;
            case 'updateOrganizerItinerary':
                if(!isset($_POST["itinerary_id"]) || !isset($_POST["user_id"])){
                    $output["message"] = "Errore inaspettato, riprova!";
                }else{
                    if(isset($_POST["places"])){
                        $places = json_decode($_POST["places"], true);
                        
                        $data = mysqli_query($_db, "SELECT id 
                                                FROM placesInItinerary
                                                WHERE itinerary_id = '".escape($_POST["itinerary_id"])."'");

                        if(mysqli_num_rows($data)!=0){
                            while($row = mysqli_fetch_array($data, MYSQLI_ASSOC)){
                                $places_in_itinerary_ids[] = $row["id"];
                            }
                        }

                        for($i=0; $i<count($places); $i++){
                            $place = $places[$i];

                            if (in_array($place["id"], $places_in_itinerary_ids)) {
                                unset($places_in_itinerary_ids[array_search($place["id"],$places_in_itinerary_ids)]); 
                            }else{
                                $data = mysqli_query($_db, "SELECT id 
                                                            FROM places 
                                                            WHERE name = '".escape($place["name"])."' AND
                                                                    city = '".escape($place["city"])."' AND
                                                                    road = '".escape($place["road"])."'");
                                if(mysqli_num_rows($data)==0){
                                    mysqli_query($_db, "INSERT INTO places(name,city,road,address,lat,lon) 
                                                        VALUES ('".escape($place["name"])."', '".escape($place["city"])."', 
                                                                '".escape($place["road"])."', '".escape($place["address"])."',
                                                                '".escape($place["lat"])."', '".escape($place["lon"])."')");
                                    $place_id = mysqli_insert_id($_db);
                                }else{
                                    $data = mysqli_fetch_assoc($data);
                                    $place_id = $data["id"];
                                }
                                mysqli_query($_db, "INSERT INTO placesInItinerary(order_place, place_id, itinerary_id) VALUES ($i+1, $place_id, '".escape($_POST["itinerary_id"])."')");
                            }
                        }

                        if(count($places_in_itinerary_ids) != 0){
                            foreach($places_in_itinerary_ids as $place_in_itinerary_id){
                                mysqli_query($_db, "DELETE FROM placesInItinerary WHERE id = $place_in_itinerary_id");
                            }
                        }
                    }
                    
                    $output["result"] = "success";
                }
                break;
            case 'addPlaceToItinerary':
                if(!isset($_POST["name"]) || !isset($_POST["itinerary_id"]) || !isset($_POST["city"])
                    || !isset($_POST["lat"]) || !isset($_POST["lon"]) || !isset($_POST["road"]) || !isset($_POST["address"])){
                    $output["message"]="Non hai inserito tutti i dati!";
                }else{
                    $data = mysqli_query($_db, "SELECT id 
                                                FROM places 
                                                WHERE name = '".escape($_POST["name"])."' AND
                                                        city = '".escape($_POST["city"])."' AND
                                                        road = '".escape($_POST["road"])."'");
                    if(mysqli_num_rows($data)==0){
                        mysqli_query($_db, "INSERT INTO places(name,city,road,address,lat,lon) 
                                            VALUES ('".escape($_POST["name"])."', '".escape($_POST["city"])."', 
                                                    '".escape($_POST["road"])."', '".escape($_POST["address"])."',
                                                    '".escape($_POST["lat"])."', '".escape($_POST["lon"])."')");
                        $place_id = mysqli_insert_id($_db);
                    }else{
                        $data = mysqli_fetch_assoc($data);
                        $place_id = $data["id"];
                    }

                    $data = mysqli_query($_db, "SELECT NULL 
                                                FROM placesInItinerary 
                                                WHERE place_id = $place_id AND
                                                      itinerary_id = '".escape($_POST["itinerary_id"])."'");

                    if(mysqli_num_rows($data)==0){
                        mysqli_query($_db, "INSERT INTO placesInItinerary(order_place, place_id, itinerary_id) VALUES (0, $place_id, '".escape($_POST["itinerary_id"])."')");
                        $output["result"]="success";
                        $output["message"]="Luogo aggiunto con successo al tuo itinerario!"; 
                    }else{
                        $output["message"]="L'itinerario ha già incluso questo luogo!";
                    }
                }
                break;
            case 'getPublicItineraries':
                if(!isset($_POST["role"])){
                    $output["message"] = "Errore inaspettato, riprova!";
                }else{
                    $data = mysqli_query($_db, "SELECT i.id as itinerary_id
                                                FROM itineraries i, users u 
                                                WHERE i.creator_id = u.id and
                                                        u.role = '".escape($_POST["role"])."'");
                    if(mysqli_num_rows($data)!=0){
                        while($row = mysqli_fetch_array($data, MYSQLI_ASSOC)){
                            $itinerary_ids[] = $row["itinerary_id"];
                        }
                    }

                    foreach($itinerary_ids as $itinerary_id){
                        $itineraryData = mysqli_query($_db, "SELECT i.id as itinerary_id, i.name, i.city, u.username as creator_name
                                                FROM itineraries i, users u 
                                                WHERE i.creator_id = u.id AND
                                                      i.id = $itinerary_id");
                        $itineraryData = mysqli_fetch_assoc($itineraryData);
                        $voteData = mysqli_query($_db, "SELECT count(*) as votes, avg(vote) as vote_avg
                                                        FROM itineraryReviews ir, itineraryUser i 
                                                        WHERE i.id = ir.itinerary_user_id AND
                                                            i.itinerary_id = $itinerary_id");

                        if(mysqli_num_rows($voteData)==0){
                            $partialResult = $itineraryData;
                        }else{
                            $voteData = mysqli_fetch_assoc($voteData);
                            $partialResult = array_merge($itineraryData, $voteData);
                        }

                        $imageData = mysqli_query($_db, "SELECT i.id as image_id, i.image
                                                FROM itineraryImages i, itineraryUser iu
                                                WHERE i.itinerary_user_id = iu.id AND
                                                      iu.itinerary_id = $itinerary_id
                                                LIMIT 1");

                        if(mysqli_num_rows($imageData)==0){
                            $result[] = $partialResult;
                        }else{
                            $imageData = mysqli_fetch_assoc($imageData);
                            $result[] = array_merge($partialResult, $imageData);
                        }
                        
                    }

                    if(isset($result)){
                        $output["values"] = $result;
                    }else{
                        $output["message"] = "Nessun itinerario creato";
                    }

                    $output["result"] = "success";
                }
                break;

            case 'getPublicItineraryImages':
                if(!isset($_POST["itinerary_id"])){
                    $output["message"] = "Errore inaspettato, riprova!";
                }else{
                    $data = mysqli_query($_db, "SELECT i.id, i.image 
                                                FROM itineraryImages i, itineraryUser iu 
                                                WHERE i.itinerary_user_id = iu.id AND
                                                      iu.itinerary_id = '".escape($_POST["itinerary_id"])."'");
                    if(mysqli_num_rows($data)==0){
                        $output["message"] = "nessuna foto";
                    }else{
                        $output["values"] = mysqli_fetch_all($data, MYSQLI_ASSOC);
                    }
                    $output["result"] = "success";
                }
                break;

            case 'addPublicItineraryUser':
                if(!isset($_POST["itinerary_id"]) || !isset($_POST["user_id"]) ||
                    !isset($_POST["departure_date"]) || !isset($_POST["return_date"])){
                    $output["message"] = "Errore inaspettato, riprova!";
                }else{
                    $departureDate = date('Y-m-d',strtotime($_POST["departure_date"]));
                    $returnDate = date('Y-m-d',strtotime($_POST["return_date"]));

                    $data = mysqli_query($_db, "SELECT NULL
                                                FROM itineraryUser iu
                                                WHERE iu.itinerary_id = '".escape($_POST["itinerary_id"])."'
                                                    and iu.departure_date = '$departureDate'
                                                    and iu.user_id = '".escape($_POST["user_id"])."'
                                                    and iu.return_date = '$returnDate");
                    if(mysqli_num_rows($data)!=0){
                        $output["message"] = "Ha già svolto questo itinerario!";
                    }else{
                        mysqli_query($_db, "INSERT INTO itineraryUser(departure_date, return_date, user_id, itinerary_id) VALUES ('$departureDate', '$returnDate', '".escape($_POST["user_id"])."', '".escape($_POST["itinerary_id"])."')");
                        $output["result"] = "success";
                    }

                }
                break;

            case 'getUserVisitedPlacesAndItineraries':
                if(!isset($_POST["user_id"])){
                    $output["message"] = "Errore inaspettato, riprova!";
                }else{
                    $data = mysqli_query($_db, "SELECT iu.departure_date, iu.return_date, i.name 
                                                FROM itineraries i, itineraryUser iu 
                                                WHERE i.id = iu.itinerary_id AND
                                                        iu.user_id = '".escape($_POST["user_id"])."'");
                    if(mysqli_num_rows($data)!=0){
                        $output["itineraries"] = mysqli_fetch_all($data, MYSQLI_ASSOC);
                    }

                    $data = mysqli_query($_db, "SELECT vp.visited_date as date, p.name 
                                                FROM visitedPlaces vp, places p
                                                WHERE p.id = vp.place_id AND
                                                      vp.user_id = '".escape($_POST["user_id"])."'");
                    if(mysqli_num_rows($data)!=0){
                        $output["visitedPlaces"] = mysqli_fetch_all($data, MYSQLI_ASSOC);
                    }

                    $output["result"] = "success";
                }
                break;

            case 'getUserImage':
                if(!isset($_POST["user_id"])){
                    $output["message"] = "Errore inaspettato, riprova!";
                }else{
                    $data = mysqli_query($_db, "SELECT image 
                                                FROM users 
                                                WHERE id = '".escape($_POST["user_id"])."'");
                    if(mysqli_num_rows($data)!=0){
                        $output["image"] = mysqli_fetch_assoc($data)["image"];
                    }

                    $output["result"] = "success";
                }
                break;

            case 'getVisitedPlacesAndItineraries':
                if(!isset($_POST["user_id"])){
                    $output["message"] = "Errore inaspettato, riprova!";
                }else{
                    $data = mysqli_query($_db, "SELECT i.id, i.name, iu.departure_date, iu.return_date, u.username as creator_name, i.city
                                                FROM itineraries i, itineraryUser iu, users u
                                                WHERE i.id = iu.itinerary_id AND 
                                                        i.creator_id = u.id AND
                                                        iu.user_id = '".escape($_POST["user_id"])."'");
                    if(mysqli_num_rows($data)!=0){
                        $output["itineraries"] = mysqli_fetch_all($data, MYSQLI_ASSOC);

                        for($i=0; $i<count($output["itineraries"]); $i++){
                            $itinerary_id = $output["itineraries"][$i]["id"];

                            $imageData = mysqli_query($_db, "SELECT i.id as image_id, i.image
                                                            FROM itineraryImages i
                                                            WHERE i.itinerary_id = '".escape($itinerary_id)."'
                                                            ORDER BY RAND() 
                                                            LIMIT 1");
                            if(mysqli_num_rows($imageData)!=0){
                                $output["itineraries"][$i]["image"] = mysqli_fetch_assoc($imageData);
                            }
                        }

                    }

                    $data = mysqli_query($_db, "SELECT v.id as visited_place_id, p.name, p.road, p.address, p.city, v.prefered, p.lat, p.lon, v.visited_date as date
                                                FROM places p, visitedPlaces v
                                                WHERE p.id = v.place_id AND 
                                                        v.user_id = '".escape($_POST["user_id"])."'");
                    if(mysqli_num_rows($data)!=0){
                        $output["visitedPlaces"] = mysqli_fetch_all($data, MYSQLI_ASSOC);

                        for($i=0; $i<count($output["visitedPlaces"]); $i++){
                            $visited_place_id = $output["visitedPlaces"][$i]["visited_place_id"];

                            $imageData = mysqli_query($_db, "SELECT i.id as image_id, i.image
                                                            FROM images i
                                                            WHERE i.visited_place_id = '".escape($visited_place_id)."'
                                                            ORDER BY RAND() 
                                                            LIMIT 1");
                            if(mysqli_num_rows($imageData)!=0){
                                $output["visitedPlaces"][$i]["image"] = mysqli_fetch_assoc($imageData);
                            }
                        }

                    }

                    $output["result"] = "success";
                }
                break;
            }
        output();
    }
    
?>
