-- *********************************************
-- * SQL MySQL generation                      
-- *--------------------------------------------
-- * DB-MAIN version: 11.0.1              
-- * Generator date: Dec  4 2018              
-- * Generation date: Sat Dec  5 16:35:17 2020 
-- * LUN file: C:\Users\giada\CallER\database\CallER_DB.lun 
-- * Schema: CallER_v2_model/1 
-- ********************************************* 


-- Database Section
-- ________________ 

create database CallER_v2_model;
use CallER_v2_model;


-- Tables Section
-- _____________ 

create table images (
     id int(11) not null AUTO_INCREMENT,
     image mediumtext not null,
     visited_place_id int(11) not null,
     constraint IDIMAGES primary key (id));

create table itineraries (
     id int(11) not null AUTO_INCREMENT,
     name varchar(300) not null,
     city enum('Cesena','Forlì') not null,
     creator_id int(11) not null,
     constraint IDITINERARIES primary key (id));

create table itineraryImages (
     id int(11) not null AUTO_INCREMENT,
     image mediumtext not null,
     itinerary_user_id int(11) not null,
     constraint IDItineraryImages primary key (id));

create table itineraryReviews (
     id int(11) not null AUTO_INCREMENT,
     itinerary_user_id int(11) not null,
     message varchar(400) not null,
     vote int(1) not null,
     constraint IDItineraryReviews primary key (id),
     constraint FKget_ID unique (itinerary_user_id));

create table itineraryUser (
     id int(11) not null AUTO_INCREMENT,
     departure_date date not null,
     return_date date not null,
     user_id int(11) not null,
     itinerary_id int(11) not null,
     constraint IDItineraryUser_ID primary key (id));

create table messages (
     id int(11) not null AUTO_INCREMENT,
     message varchar(400) not null,
     date dateTime not null,
     status enum('read','notRead') not null,
     sender_id int(11)  not null,
     receiver_id int(11)  not null,
     constraint IDMESSAGES primary key (id));

create table places (
     id bigint not null,
     place_id bigint,
     name varchar(300) not null,
     road varchar(300) not null,
     address varchar(300) not null,
     city varchar(200) not null,
     lat float(1) not null,
     lon float(1) not null,
     constraint IDPLACES primary key (id));

create table placesInItinerary (
     id int(11) not null AUTO_INCREMENT,
     order_place int(2) not null,
     place_id int(11) not null,
     itinerary_id int(11) not null,
     constraint IDPlacesInItinerary primary key (id));

create table Reviews (
     Id -- Index attribute not implemented -- not null,
     Vote int not null,
     Message varchar(300) not null,
     visited_place_id bigint not null,
     constraint IDIMAGES primary key (Id));

create table Users (
     id bigint not null,
     name varchar(300) not null,
     email varchar(300) not null,
     password varchar(32) not null,
     image varchar(500) not null,
     role varchar(100) not null,
     constraint IDUSERS_ID primary key (id));

create table VisitedPlaces (
     id bigint not null,
     visited_date date,
     prefered char not null,
     user_id bigint not null,
     place_id bigint not null,
     constraint IDVISITED_PLACES primary key (id));

create table Vote (
     id bigint not null,
     user_id bigint not null,
     image_id bigint not null,
     constraint IDVote primary key (id));


-- Constraints Section
-- ___________________ 

alter table Images add constraint FKtaken
     foreign key (visited_place_id)
     references VisitedPlaces (id);

alter table itineraries add constraint FKcreate
     foreign key (creator_id)
     references users (id);

alter table itineraryImages add constraint FKphoto
     foreign key (itinerary_user_id)
     references itineraryUser (id);

alter table itineraryReviews add constraint FKget_FK
     foreign key (itinerary_user_id)
     references itineraryUser (id);

-- Not implemented
-- alter table ItineraryUser add constraint IDItineraryUser_CHK
--     check(exists(select * from ItineraryReviews
--                  where ItineraryReviews.itinerary_user_id = id)); 

alter table itineraryUser add constraint FKdo
     foreign key (user_id)
     references users (id);

alter table itineraryUser add constraint FKsubgroup
     foreign key (itinerary_id)
     references itineraries (id);

alter table Messages add constraint FKsend
     foreign key (sender_id)
     references Users (id);

alter table Messages add constraint FKreceiver
     foreign key (receiver_id)
     references Users (id);

alter table placesInItinerary add constraint FKpla_Pla
     foreign key (place_id)
     references places (id);

alter table placesInItinerary add constraint FKpla_Iti
     foreign key (itinerary_id)
     references itineraries (id);

alter table Reviews add constraint FKowns
     foreign key (visited_place_id)
     references VisitedPlaces (id);

-- Not implemented
-- alter table Users add constraint IDUSERS_CHK
--     check(exists(select * from Messages
--                  where Messages.receiver_id = id)); 

-- Not implemented
-- alter table Users add constraint IDUSERS_CHK
--     check(exists(select * from Messages
--                  where Messages.sender_id = id)); 

alter table VisitedPlaces add constraint FKhas
     foreign key (user_id)
     references Users (id);

alter table VisitedPlaces add constraint FKis
     foreign key (place_id)
     references Places (id);

alter table Vote add constraint FKvot_Use
     foreign key (user_id)
     references Users (id);

alter table Vote add constraint FKvot_Ima
     foreign key (image_id)
     references Images (id);


-- Index Section
-- _____________ 

