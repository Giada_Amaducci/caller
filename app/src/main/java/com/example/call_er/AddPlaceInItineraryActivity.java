package com.example.call_er;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.call_er.Adapters.ListAdapter.SearchPlaceListAdapter;
import com.example.call_er.Items.ItineraryItem;
import com.example.call_er.Items.ItineraryPlaceItem;
import com.example.call_er.Items.PlaceItem;
import com.example.call_er.Utilities.Extra;
import com.example.call_er.Utilities.GpsUtilities;
import com.example.call_er.Utilities.InternetUtilities;
import com.example.call_er.Utilities.LoadingDialog;
import com.example.call_er.Utilities.OsmUtilities;
import com.example.call_er.Utilities.PermissionsUtilities;
import com.example.call_er.Utilities.Utility;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AddPlaceInItineraryActivity extends AppCompatActivity {

    private ItineraryPlaceItem selectedPlace;
    private String prevActivityName;
    private List<PlaceItem> places;
    private EditText gpsEditText;
    private ItineraryItem itineraryItem;
    private LoadingDialog loadingDialog;
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            if (intent.getAction() != null && intent.getAction().equals("SEND_DATA")) {
                final String lat = intent.getStringExtra("location_latitude");
                final String lon = intent.getStringExtra("location_longitude");
                if (InternetUtilities.getIsNetworkConnected()) {
                    createGpsRequestForPlace(AddPlaceInItineraryActivity.this, lat, lon, gpsEditText);
                } else {
                    InternetUtilities.getSnackbar().show();
                }
                GpsUtilities.removeLocationUpdates(AddPlaceInItineraryActivity.this, broadcastReceiver);
            }
        }
    };

    @Override
    public void onBackPressed() {
        if(this.getClass().equals(AddPlaceInItineraryActivity.class)){

            final Intent i;

            if(this.prevActivityName!= null && this.prevActivityName.equals("AddItineraryOrganizerActivity")){
                i = new Intent(getApplicationContext(), AddItineraryOrganizerActivity.class);
                i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(),  "AddPlaceInItinerary");
            }else if(this.prevActivityName!= null && this.prevActivityName.equals("PlacesInItineraryActivity")){
                i = new Intent(getApplicationContext(), PlacesInItineraryActivity.class);
                i.putExtra(Extra.DEPARTURE_DATE.getTypeExtra(),  this.itineraryItem.getDepartureDate());
                i.putExtra(Extra.RETURN_DATE.getTypeExtra(), itineraryItem.getReturnDate());
                i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(),  this.prevActivityName);
            }else{
                i = new Intent(getApplicationContext(), OrganizerItineraryActivity.class);
                i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(),  "AddPlaceInItinerary");
            }

            if (prevActivityName.equals("ItineraryActivity") || prevActivityName.equals("OrganizerItineraryActivity")) {
                i.putExtra(Extra.ID.getTypeExtra(), this.itineraryItem.getId());
            }

            if(prevActivityName.equals("OrganizerItineraryActivity")){
                i.putExtra(Extra.USER_COUNT.getTypeExtra(), this.itineraryItem.getUsers());
            }

            i.putExtra(Extra.NAME.getTypeExtra(),  this.itineraryItem.getName());
            i.putExtra(Extra.CREATOR_NAME.getTypeExtra(), this.itineraryItem.getCreatorName());
            i.putExtra(Extra.CITY.getTypeExtra(),  this.itineraryItem.getCity());

            i.putExtra(Extra.PLACES.getTypeExtra(),  this.itineraryItem.getAllPlaces());
            this.startActivity(i);
        }else{
            finish();
        }
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == PermissionsUtilities.REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length > 0) {
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    PermissionsUtilities.createSnackBar(this);
                }
            }
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_add_place_in_itinerary);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.addPlaceInItineraryConstraintLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI() {
        this.loadingDialog = new LoadingDialog(this);
        this.prevActivityName = this.getIntent().getStringExtra(Extra.ACTIVITY_NAME.getTypeExtra());
        this.places = new ArrayList<>();
        final String title = this.getIntent().getStringExtra(Extra.NAME.getTypeExtra());
        final String creatorName = this.getIntent().getStringExtra(Extra.CREATOR_NAME.getTypeExtra());
        final String city = this.getIntent().getStringExtra(Extra.CITY.getTypeExtra());
        final String count = this.getIntent().getStringExtra(Extra.USER_COUNT.getTypeExtra());
        final String itineraryId;

        if (this.prevActivityName != null) {
            if (this.prevActivityName.equals("ItineraryActivity") || this.prevActivityName.equals("OrganizerItineraryActivity")) {
                itineraryId = getIntent().getStringExtra(Extra.ID.getTypeExtra());
            } else {
                itineraryId = "nessuno";

            }
        }else{
            itineraryId = "nessuno";
        }

        final String placeList = getIntent().getStringExtra(Extra.PLACES.getTypeExtra());
        this.itineraryItem = new ItineraryItem(itineraryId, title, city);

        if(!this.prevActivityName.equals("AddItineraryOrganizerActivity")){
            final String departureDate = getIntent().getStringExtra(Extra.DEPARTURE_DATE.getTypeExtra());
            final String returnDate = getIntent().getStringExtra(Extra.RETURN_DATE.getTypeExtra());
            this.itineraryItem.setDepartureDate(departureDate);
            this.itineraryItem.setReturnDate(returnDate);
        }
        this.itineraryItem.setPlaces(placeList);
        this.itineraryItem.setCreatorName(creatorName);

        if(count != null){
            this.itineraryItem.setUsers(count);
        }else{
            this.itineraryItem.setUsers("0");
        }


        final TextInputLayout searchPlaceInputLayout = this.findViewById(R.id.searchOutlinedTextField);
        final TextInputLayout gpsPlaceInputlayout = this.findViewById(R.id.gpsOutlinedTextField);
        final EditText searchPlaceEditText = searchPlaceInputLayout.getEditText();
        this.gpsEditText = gpsPlaceInputlayout.getEditText();

        if (!PermissionsUtilities.checkPermissions(this)) {
            PermissionsUtilities.requestPermissions(this);
        }

        if (InternetUtilities.getIsNetworkConnected()) {
            GpsUtilities.setLocation(this);
        } else {
            InternetUtilities.getSnackbar().show();
        }

        final ListView listView = this.findViewById(R.id.searchedPlaceListView);
        final SearchPlaceListAdapter listAdapter = new SearchPlaceListAdapter(this);
        listView.setAdapter(listAdapter);

        listView.setOnItemClickListener((adapterView, view, position, id) -> {
            final PlaceItem placeItem = places.get(position);
            selectedPlace = new ItineraryPlaceItem("nessuno", placeItem);
            Objects.requireNonNull(searchPlaceEditText).setText(selectedPlace.getPlaceItem().getName());
        });

        this.findViewById(R.id.gpsImageButton).setOnClickListener(view -> {
            Objects.requireNonNull(searchPlaceEditText).setText("");
            places.clear();
            listAdapter.setData(places);
            if (InternetUtilities.getIsNetworkConnected()) {
                GpsUtilities.requestLocation(AddPlaceInItineraryActivity.this, broadcastReceiver);
            } else {
                InternetUtilities.getSnackbar().show();
            }
        });

        this.findViewById(R.id.searchImageButton).setOnClickListener(v -> {
            final String place = Objects.requireNonNull(searchPlaceEditText).getText().toString();
            gpsEditText.setText("");
            places.clear();
            listAdapter.setData(places);
            if (InternetUtilities.getIsNetworkConnected()) {
                loadingDialog.startLoadingDialog();
                OsmUtilities.searchAmenity(place, places, AddPlaceInItineraryActivity.this, listAdapter, itineraryItem.getCity(), loadingDialog);
            } else {
                InternetUtilities.getSnackbar().show();
            }
        });

        this.findViewById(R.id.fab_add_addplace).setOnClickListener(v -> {
            if (selectedPlace != null) {

                final Intent i;
                if(this.prevActivityName!= null && this.prevActivityName.equals("AddItineraryOrganizerActivity")) {
                    i = new Intent(getApplicationContext(), AddItineraryOrganizerActivity.class);
                    i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "AddPlaceInItinerary");
                }else if(this.prevActivityName!= null && this.prevActivityName.equals("AddItineraryActivity")){
                    i = new Intent(getApplicationContext(), PlacesInItineraryActivity.class);
                    i.putExtra(Extra.DEPARTURE_DATE.getTypeExtra(),  this.itineraryItem.getDepartureDate());
                    i.putExtra(Extra.RETURN_DATE.getTypeExtra(), itineraryItem.getReturnDate());
                    i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(),  this.prevActivityName);
                }else{
                    i = new Intent(getApplicationContext(), OrganizerItineraryActivity.class);
                    i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(),  "AddPlaceInItinerary");
                }

                if (prevActivityName.equals("ItineraryActivity") || prevActivityName.equals("OrganizerItineraryActivity")) {
                    i.putExtra(Extra.ID.getTypeExtra(), itineraryItem.getId());
                }

                if(prevActivityName.equals("OrganizerItineraryActivity")){
                    i.putExtra(Extra.USER_COUNT.getTypeExtra(), this.itineraryItem.getUsers());
                }

                i.putExtra(Extra.NAME.getTypeExtra(), itineraryItem.getName());
                i.putExtra(Extra.CITY.getTypeExtra(), itineraryItem.getCity());

                final List<ItineraryPlaceItem> places = itineraryItem.getPlaces();
                places.add(selectedPlace);

                i.putExtra(Extra.PLACES.getTypeExtra(), itineraryItem.getAllPlaces());
                startActivity(i);
            }

        });

        Utility.setUpToolbar(this, "Aggiungi un luogo");
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    private void createGpsRequestForPlace(final Activity activity, final String latitude, final String longitude, final EditText text){
        final String url ="https://nominatim.openstreetmap.org/reverse?lat="+latitude+"&lon="+longitude+"&addressdetails=1&format=json&limit=1";
        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, response -> {
                    try {
                        text.setText(response.get("display_name").toString().replace(",",";"));
                        final JSONObject addressObject = response.getJSONObject("address");
                        final PlaceItem placeItem = OsmUtilities.getPlaceFromOSMString(response.get("display_name").toString(), itineraryItem.getCity(),
                                addressObject.getString("road"), response.get("lon").toString(),
                                response.get("lat").toString());
                        selectedPlace = new ItineraryPlaceItem("nessuno", placeItem);
                    } catch (final JSONException e) {
                        Log.e(activity.getString(R.string.error), e.toString());
                    }
                }, error -> Log.e(activity.getString(R.string.error), error.toString()));
        jsonObjectRequest.setTag(OsmUtilities.OSM_REQUEST_TAG);
        InternetUtilities.getRequestQueue().add(jsonObjectRequest);
    }
}
