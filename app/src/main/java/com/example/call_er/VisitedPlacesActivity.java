package com.example.call_er;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.RadioGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.call_er.Adapters.ListAdapter.VisitedPlacesAdapter;
import com.example.call_er.Items.ImageItem;
import com.example.call_er.Items.VisitedPlaceItem;
import com.example.call_er.Utilities.Extra;
import com.example.call_er.Utilities.FileUtilities;
import com.example.call_er.Utilities.InternetUtilities;
import com.example.call_er.Utilities.LoadingDialog;
import com.example.call_er.Utilities.UserManagement;
import com.example.call_er.Utilities.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class VisitedPlacesActivity extends AppCompatActivity {

    private List<VisitedPlaceItem> places;
    private VisitedPlacesAdapter visitedPlacesListAdapter;

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.nav_onlysearch, menu);
        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(final String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(final String newText) {
                visitedPlacesListAdapter.getFilter().filter(newText);
                return true;
            }
        });
        return true;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_visited_places);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.visitedPlacesActivityLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI(){
        final ListView listView = findViewById(R.id.visitedPlacesListView);
        this.visitedPlacesListAdapter = new VisitedPlacesAdapter(this.getApplicationContext(), this);
        listView.setAdapter(this.visitedPlacesListAdapter);

        this.places = new ArrayList<>();

        final RadioGroup rg = findViewById(R.id.locationRadioGroup);
        rg.setOnCheckedChangeListener((group, checkedId) -> {
            final LoadingDialog loadingDialog = new LoadingDialog(VisitedPlacesActivity.this);
            loadingDialog.startLoadingDialog();

            final String user_id = FileUtilities.getData( getCacheDir()+"/mtj.txt").get(UserManagement.ID.getId());

            switch(checkedId){
                case -1:
                    Utility.showNeutralAlert(getString(R.string.attenzione),getString(R.string.nessuna_categoria_scelta), VisitedPlacesActivity.this);
                    break;
                case R.id.cesenaRadioButton:
                    if (InternetUtilities.getIsNetworkConnected()) {
                        getVisitedPlaces("Cesena", user_id, loadingDialog);
                    } else {
                        InternetUtilities.getSnackbar().show();
                    }
                    break;
                case R.id.forliRadioButton:
                    if (InternetUtilities.getIsNetworkConnected()) {
                        getVisitedPlaces("Forlì", user_id, loadingDialog);
                    } else {
                        InternetUtilities.getSnackbar().show();
                    }
                    break;
            }
        });

        this.findViewById(R.id.fabAddVisitedPlaceActivity).setOnClickListener(v -> {
            final Intent i = new Intent(getApplicationContext(), SearchPlaceActivity.class);
            i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "VisitedPlacesActivity");
            startActivity(i);
        });

        Utility.setUpToolbar(this, "I Tuoi Luoghi");
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    private void getVisitedPlaces(final String city, final String userId, final LoadingDialog loadingDialog){
        final String url = "https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("result").equals("success")) {
                            places.clear();
                            if(!jsonObject.getString("message").equals("Nessun posto visitato")) {
                                final JSONArray jsonArray = new JSONArray(jsonObject.getString("values"));
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    final JSONObject visitedPlace = (JSONObject) jsonArray.get(i);
                                    final String id = visitedPlace.getString("visited_place_id");
                                    final String name = visitedPlace.getString("name");
                                    final String road = visitedPlace.getString("road");
                                    final String address = visitedPlace.getString("address");
                                    final String city1 = visitedPlace.getString("city");
                                    final String lat = visitedPlace.getString("lat");
                                    final String lon = visitedPlace.getString("lon");
                                    final String date = visitedPlace.getString("date");
                                    final boolean prefered = !(visitedPlace.getString("prefered")).equals("0");
                                    final VisitedPlaceItem visitedPlaceItem = new VisitedPlaceItem(id, name, address, city1, road, prefered, lat, lon, date);
                                    if (visitedPlace.has("image")) {
                                        final String image = visitedPlace.getString("image");
                                        final String image_id = visitedPlace.getString("image_id");
                                        visitedPlaceItem.setImage(new ImageItem(image_id, image));
                                    }
                                    places.add(visitedPlaceItem);
                                }
                            }
                            visitedPlacesListAdapter.setData(places);
                        }else{
                            Utility.showNeutralAlert(getString(R.string.attenzione),jsonObject.getString("message"), VisitedPlacesActivity.this);
                        }
                        loadingDialog.dismissDialog();
                    } catch (final JSONException e) {
                        Log.e(getString(R.string.error), e.toString());
                    }
                },
                error -> Log.e(getString(R.string.error), error.toString())){
            @Override
            protected Map<String,String> getParams(){
                final Map<String, String> params = new HashMap<>();
                params.put("type", "getVisitedPlaces");
                params.put("user_id", userId);
                params.put("city", city);
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }
}
