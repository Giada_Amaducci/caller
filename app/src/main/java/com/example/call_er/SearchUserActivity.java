package com.example.call_er;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RadioGroup;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.call_er.Adapters.ListAdapter.SearchUserListAdapter;
import com.example.call_er.Items.UserItem;
import com.example.call_er.Utilities.Extra;
import com.example.call_er.Utilities.InternetUtilities;
import com.example.call_er.Utilities.Utility;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class SearchUserActivity  extends AppCompatActivity {

    private List<UserItem> userList;
    private SearchUserListAdapter listAdapter;
    private RadioGroup radioGroup;
    private UserItem selectedUser;
    private int selectedRadio;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_search_user);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.searchLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI(){
        final ListView listView = this.findViewById(R.id.searchedPlaceListView);
        final TextInputLayout searchInputLayout = findViewById(R.id.searchOutlinedTextField);

        this.radioGroup = this.findViewById(R.id.searchRadioGroup);
        this.userList = new ArrayList<>();

        this.listAdapter = new SearchUserListAdapter(this);
        listView.setAdapter(listAdapter);

        listView.setOnItemClickListener((adapterView, view, position, id) -> {
            selectedUser = userList.get(position);
            final Intent i = new Intent(getApplicationContext(), MessageListActivity.class);
            i.putExtra(Extra.NAME.getTypeExtra(), selectedUser.getName());
            i.putExtra(Extra.ID.getTypeExtra(), selectedUser.getId());
            i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "SearchUserActivity");
            startActivity(i);
        });

        this.findViewById(R.id.searchImageButton).setOnClickListener(v -> {
            final String user = Objects.requireNonNull(searchInputLayout.getEditText()).getText().toString();

            userList.clear();
            selectedRadio = radioGroup.getCheckedRadioButtonId();
            switch(selectedRadio) {
                case -1:
                    Utility.showNeutralAlert(getString(R.string.attenzione),getString(R.string.nessuna_categoria_scelta), SearchUserActivity.this);
                    break;
                case R.id.phoneRadioButton:
                    if (InternetUtilities.getIsNetworkConnected()) {
                        searchUser(user, "phone");
                    } else {
                        InternetUtilities.getSnackbar().show();
                    }
                    break;
                case R.id.usernameRadioButton:
                    if (InternetUtilities.getIsNetworkConnected()) {
                        searchUser(user, "username");
                    } else {
                        InternetUtilities.getSnackbar().show();
                    }
                    break;
            }
        });

        Utility.setUpToolbar(this, this.getString(R.string.ricerca_utente));
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    private void searchUser(final String stringToSearch, final String with){
        final Map<String, String> params = new HashMap<>();
        params.put("type", "searchUser");
        params.put("stringToSearch", stringToSearch);
        params.put("with", with);
        this.searchUser(params);
    }

    private void searchUser(final Map<String, String> params){
        final String url = "https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("result").equals("success")) {
                            userList.clear();
                            if(!jsonObject.getString("message").equals("Nessun utente")){
                                final JSONArray jsonArray = new JSONArray(jsonObject.getString("values"));
                                for (int i = 0; i <jsonArray.length(); i++){
                                    final JSONObject user = (JSONObject)jsonArray.get(i);
                                    final String userId = user.getString("user_id");
                                    final String name = user.getString("name");
                                    final String email = user.getString("email");
                                    final String phone = user.getString("number");
                                    final String image = user.getString("image");
                                    final UserItem userItem = new UserItem(name, image, email, userId, phone);
                                    userList.add(userItem);
                                }
                            }else{
                                Utility.showToast("Nessun Utente trovato!", SearchUserActivity.this);
                            }
                            listAdapter.setData(userList);
                        }else{
                            Utility.showNeutralAlert(getString(R.string.attenzione),jsonObject.getString("message"), SearchUserActivity.this);
                        }
                    } catch (final JSONException e) {
                        Log.e(getString(R.string.error), e.toString());
                    }
                },
                error -> Log.e(getString(R.string.error), error.toString())){
            @Override
            protected Map<String,String> getParams(){
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }
}
