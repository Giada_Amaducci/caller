package com.example.call_er;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.call_er.Adapters.ListAdapter.ReviewsAdapter;
import com.example.call_er.Items.ItineraryItem;
import com.example.call_er.Items.PlaceItem;
import com.example.call_er.Items.ReviewItem;
import com.example.call_er.Items.UserItem;
import com.example.call_er.Utilities.Extra;
import com.example.call_er.Utilities.InternetUtilities;
import com.example.call_er.Utilities.LoadingDialog;
import com.example.call_er.Utilities.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ReviewsActivity extends AppCompatActivity {

    private ReviewsAdapter listAdapter;
    private List<ReviewItem> reviewsList;
    private PlaceItem placeItem;
    private ItineraryItem itineraryItem;
    private String placeType;
    private String activityName;

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            this.onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if(this.getClass().equals(ReviewsActivity.class)){
            if(activityName != null && activityName.equals("PublicItineraryActivity")){
                final Intent i = new Intent(getApplicationContext(), PublicItineraryActivity.class);
                i.putExtra(Extra.NAME.getTypeExtra(), itineraryItem.getName());
                i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "PublicItineraryActivity");
                i.putExtra(Extra.CREATOR_NAME.getTypeExtra(), itineraryItem.getCreatorName());
                i.putExtra(Extra.ID.getTypeExtra(), itineraryItem.getId());
                i.putExtra(Extra.CITY.getTypeExtra(), itineraryItem.getCity());
                i.putExtra(Extra.AVG_VOTE.getTypeExtra(), itineraryItem.getAvg());
                i.putExtra(Extra.USER_COUNT.getTypeExtra(), itineraryItem.getVotes());
                this.startActivity(i);
            }else {
                final Intent i = new Intent(getApplicationContext(), PlaceActivity.class);
                i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), activityName);
                i.putExtra(Extra.NAME.getTypeExtra(), this.placeItem.getName());
                i.putExtra(Extra.ROAD.getTypeExtra(), this.placeItem.getRoad());
                i.putExtra(Extra.ADDRESS.getTypeExtra(), this.placeItem.getAddress());
                i.putExtra(Extra.LAT.getTypeExtra(), this.placeItem.getLat());
                i.putExtra(Extra.LON.getTypeExtra(), this.placeItem.getLon());
                i.putExtra(Extra.CITY.getTypeExtra(), this.placeItem.getCity());
                i.putExtra(Extra.TYPE.getTypeExtra(), this.placeType);
                this.startActivity(i);
            }
        }else{
            finish();
        }
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_reviews);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.reviewConstraintLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI(){
        this.activityName = this.getIntent().getStringExtra(Extra.ACTIVITY_NAME.getTypeExtra());

        final String name = this.getIntent().getStringExtra(Extra.NAME.getTypeExtra());
        final String city = this.getIntent().getStringExtra(Extra.CITY.getTypeExtra());

        if (this.activityName != null && this.activityName.equals("PublicItineraryActivity")) {
            final String id = this.getIntent().getStringExtra(Extra.ID.getTypeExtra());
            final String creator_name = this.getIntent().getStringExtra(Extra.CREATOR_NAME.getTypeExtra());
            final String avg = this.getIntent().getStringExtra(Extra.AVG_VOTE.getTypeExtra());
            final String count = this.getIntent().getStringExtra(Extra.USER_COUNT.getTypeExtra());
            this.itineraryItem = new ItineraryItem(id, name, city);
            this.itineraryItem.setCreatorName(creator_name);
            this.itineraryItem.setAvg(avg);
            this.itineraryItem.setVotes(count);
        }else{
            this.placeType = this.getIntent().getStringExtra(Extra.TYPE.getTypeExtra());
            final String address = this.getIntent().getStringExtra(Extra.ADDRESS.getTypeExtra());
            final String road = this.getIntent().getStringExtra(Extra.ROAD.getTypeExtra());
            final String lat = this.getIntent().getStringExtra(Extra.LAT.getTypeExtra());
            final String lon = this.getIntent().getStringExtra(Extra.LON.getTypeExtra());
            this.placeItem = new PlaceItem(name,address,city,road,lon,lat);
        }

        final TextView placeName = this.findViewById(R.id.placeNameTextView);
        placeName.setText(name);

        final ListView listView = this.findViewById(R.id.reviewsListView);
        this.listAdapter = new ReviewsAdapter(this, this);
        listView.setAdapter(this.listAdapter);
        this.reviewsList = new ArrayList<>();

        final LoadingDialog loadingDialog = new LoadingDialog(ReviewsActivity.this);
        loadingDialog.startLoadingDialog();

        if (InternetUtilities.getIsNetworkConnected()) {
            if(this.activityName != null && this.activityName.equals("PublicItineraryActivity")){
                this.getReviews(loadingDialog, false);
            }else{
                this.getReviews(loadingDialog, true);
            }

        } else {
            InternetUtilities.getSnackbar().show();
        }

        Utility.setUpToolbar(this, "Recensioni");
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    private void getReviews(final LoadingDialog loadingDialog, final boolean isAPlace){
        final String url = "https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("result").equals("success")) {
                            if(!jsonObject.getString("message").equals("nessuna recensione")) {
                                final JSONArray jsonArray = new JSONArray(jsonObject.getString("values"));
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    final JSONObject review = (JSONObject) jsonArray.get(i);
                                    final String vote = review.getString("vote");
                                    final String note = review.getString("message");
                                    final String user_id = review.getString("user_id");
                                    final String name = review.getString("username");
                                    final String email = review.getString("user_email");
                                    final String image = review.getString("user_image");

                                    final UserItem userItem = new UserItem(name, image, email, user_id);
                                    final ReviewItem reviewItem = new ReviewItem(userItem, vote, note);

                                    reviewsList.add(reviewItem);
                                }
                            }
                            listAdapter.setData(reviewsList);
                        }else{
                            Utility.showNeutralAlert(getString(R.string.attenzione),jsonObject.getString("message"), ReviewsActivity.this);
                        }
                        loadingDialog.dismissDialog();
                    } catch (final JSONException e) {
                        Log.e(getString(R.string.error), e.toString());
                    }
                },
                error -> Log.e(getString(R.string.error), error.toString())){
            @Override
            protected Map<String,String> getParams(){
                final Map<String, String> params = new HashMap<>();
                params.put("type", "getReviews");
                if(isAPlace){
                    params.put("name", placeItem.getName());
                    params.put("road", placeItem.getRoad());
                    params.put("city", placeItem.getCity());
                }else{
                    params.put("itinerary_id", itineraryItem.getId());
                }

                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }
}
