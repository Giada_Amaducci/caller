package com.example.call_er;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ExpandableListView;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.call_er.Adapters.ExpandableListAdapter.UserInfoExpandableListAdapter;
import com.example.call_er.Items.GenericItem;
import com.example.call_er.Items.UserItem;
import com.example.call_er.Utilities.Extra;
import com.example.call_er.Utilities.ImageUtilities;
import com.example.call_er.Utilities.InternetUtilities;
import com.example.call_er.Utilities.LoadingDialog;
import com.example.call_er.Utilities.Utility;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class UserInfoActivity extends AppCompatActivity {

    private List<GenericItem> itineraryItemList;
    private List<GenericItem> placeItemList;
    private Map<String, List<GenericItem>> values;
    private UserItem userItem;
    private String activityName;

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            this.onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if(this.getClass().equals(UserInfoActivity.class)){
            final Intent i;
            if(this.activityName.equals("ChatsActivity")){
                i = new Intent(getApplicationContext(), ChatsActivity.class);
            }else{
                i = new Intent(getApplicationContext(), MainActivity.class);
            }
            startActivity(i);
        }else{
            finish();
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_user_info);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.userInfoLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI(){
        this.activityName = this.getIntent().getStringExtra(Extra.ACTIVITY_NAME.getTypeExtra());
        final String username = this.getIntent().getStringExtra(Extra.NAME.getTypeExtra());
        final String userId = this.getIntent().getStringExtra(Extra.ID.getTypeExtra());
        final String email = this.getIntent().getStringExtra(Extra.EMAIL.getTypeExtra());

        this.userItem = new UserItem(username, email, userId);

        final ImageView userImage = findViewById(R.id.profileCircleImageView);
        final TextInputLayout emailField = findViewById(R.id.emailOutlinedTextField);

        final LoadingDialog loadingDialog = new LoadingDialog(this);
        loadingDialog.startLoadingDialog();

        findViewById(R.id.messageLinearLayout).setOnClickListener(v -> {
            final Intent i = new Intent(getApplicationContext(), MessageListActivity.class);
            i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "UserInfoActivity");
            i.putExtra(Extra.EMAIL.getTypeExtra(), userItem.getEmail());
            i.putExtra(Extra.NAME.getTypeExtra(), userItem.getName());
            i.putExtra(Extra.ID.getTypeExtra(), userItem.getId());
            startActivity(i);
        });

        this.placeItemList = new ArrayList<>();
        this.itineraryItemList = new ArrayList<>();
        this.values = new HashMap<>();

        final ExpandableListView listView = this.findViewById(R.id.desiredPlacesExpandableListView);
        final UserInfoExpandableListAdapter listAdapter = new UserInfoExpandableListAdapter(UserInfoActivity.this);
        listView.setAdapter(listAdapter);

        this.values.put("Luoghi Visitati", this.placeItemList);
        this.values.put("Itinerari", this.itineraryItemList);
        listAdapter.setData(values);

        if (InternetUtilities.getIsNetworkConnected()) {
            this.getUserVisitedPlacesAndItineraries(userId, listAdapter);
        } else {
            InternetUtilities.getSnackbar().show();
        }

        Objects.requireNonNull(emailField.getEditText()).setText(this.userItem.getEmail());
        if (InternetUtilities.getIsNetworkConnected()) {
            this.getUserImage(userId, userImage, loadingDialog);
        } else {
            InternetUtilities.getSnackbar().show();
        }

        Utility.setUpToolbar(this, this.userItem.getName());
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    private void getUserImage(final String userId, final ImageView userImage, final LoadingDialog loadingDialog){
        final String url ="https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("result").equals("success")) {
                            final String image = jsonObject.getString("image");
                            userImage.setImageBitmap(ImageUtilities.decodeBitmap(image));
                        }else{
                            Utility.showNeutralAlert(getString(R.string.attenzione), jsonObject.getString("message"), UserInfoActivity.this);
                        }
                        loadingDialog.dismissDialog();
                    } catch (final JSONException e) {
                        Log.e(getString(R.string.error), e.toString());
                    }

                },
                error -> Log.e(getString(R.string.error), error.toString())){
            @Override
            protected Map<String,String> getParams(){
                final Map<String,String> params = new HashMap<>();
                params.put("type", "getUserImage");
                params.put("user_id", userId);
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

    private void getUserVisitedPlacesAndItineraries(final String user_id, final UserInfoExpandableListAdapter listAdapter){
        final String url = "https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("result").equals("success")) {
                            values.clear();
                            if(!jsonObject.getString("message").equals("Nessun itinerario creato")) {
                                if(jsonObject.has("itineraries")){
                                    final JSONArray jsonArray = new JSONArray(jsonObject.getString("itineraries"));
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        final JSONObject itinerary = (JSONObject) jsonArray.get(i);
                                        final String name = itinerary.getString("name");
                                        final String departure_date = itinerary.getString("departure_date");
                                        final String return_date = itinerary.getString("return_date");
                                        final GenericItem genericItem = new GenericItem(name, departure_date, return_date);
                                        this.itineraryItemList.add(genericItem);
                                    }
                                }

                                if(jsonObject.has("visitedPlaces")) {
                                    final JSONArray visitedPlaces = new JSONArray(jsonObject.getString("visitedPlaces"));
                                    for (int i = 0; i < visitedPlaces.length(); i++) {
                                        final JSONObject visitedPlace = (JSONObject) visitedPlaces.get(i);
                                        final String name = visitedPlace.getString("name");
                                        final String date = visitedPlace.getString("date");
                                        final GenericItem genericItem = new GenericItem(name, date);
                                        this.placeItemList.add(genericItem);
                                    }
                                    this.values.put("Itinerari", itineraryItemList);
                                    this.values.put("Luoghi Visitati", this.placeItemList);
                                    listAdapter.setData(this.values);
                                    listAdapter.notifyDataSetChanged();
                                }
                            }
                        }else{
                            Utility.showNeutralAlert(getString(R.string.attenzione),jsonObject.getString("message"), UserInfoActivity.this);
                        }
                    } catch (final JSONException e) {
                        Log.e(getString(R.string.error), e.toString());
                    }
                },
                error -> Log.e(getString(R.string.error), error.toString())){
            @Override
            protected Map<String,String> getParams(){
                final Map<String, String> params = new HashMap<>();
                params.put("type", "getUserVisitedPlacesAndItineraries");
                params.put("user_id", user_id);
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

}
