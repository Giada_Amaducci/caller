package com.example.call_er;

import android.os.Bundle;
import android.widget.ExpandableListView;
import android.widget.RadioGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.toolbox.Volley;
import com.example.call_er.Adapters.ExpandableListAdapter.PlaceExpandableListAdapter;
import com.example.call_er.Items.PlaceItem;
import com.example.call_er.Utilities.AmenityPlace;
import com.example.call_er.Utilities.Extra;
import com.example.call_er.Utilities.InternetUtilities;
import com.example.call_er.Utilities.LoadingDialog;
import com.example.call_er.Utilities.OsmUtilities;
import com.example.call_er.Utilities.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class PlacesActivity extends AppCompatActivity {

    private Map<String, List<PlaceItem>> values;
    private String type;
    private LoadingDialog loadingDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_places);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.foodActivityLayout);

        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI(){
        this.type = this.getIntent().getStringExtra(Extra.TYPE.getTypeExtra());

        final ExpandableListView listView = this.findViewById(R.id.foodExpandableListView);
        final PlaceExpandableListAdapter listAdapter = new PlaceExpandableListAdapter(PlacesActivity.this, PlacesActivity.this, type);

        this.values = new HashMap<>();
        final List<String> headers = new ArrayList<>();
        final String pageName;
        if(this.type.equals("Food") ) {
            headers.add(AmenityPlace.RESTAURANTS.getPlaceName());
            headers.add(AmenityPlace.CAFE.getPlaceName());
            headers.add(AmenityPlace.ICE_CREAM_SHOPS.getPlaceName());
            headers.add(AmenityPlace.PUB.getPlaceName());
            headers.add(AmenityPlace.FAST_FOOD.getPlaceName());
            pageName = "Dove Mangiare";
        }else if(this.type.equals("Service")){
            headers.add(AmenityPlace.BANKS.getPlaceName());
            headers.add(AmenityPlace.POST_OFFICES.getPlaceName());
            headers.add(AmenityPlace.HOSPITALS.getPlaceName());
            headers.add(AmenityPlace.PHARMACIES.getPlaceName());
            headers.add(AmenityPlace.FIRE_STATIONS.getPlaceName());
            headers.add(AmenityPlace.POLICE.getPlaceName());
            headers.add(AmenityPlace.SUPERMARKETS.getPlaceName());
            pageName = "Servizi Utili";
        }else{
            headers.add(AmenityPlace.ATTRACTIONS.getPlaceName());
            headers.add(AmenityPlace.CINEMA.getPlaceName());
            headers.add(AmenityPlace.THEATERS.getPlaceName());
            headers.add(AmenityPlace.LIBRARIES.getPlaceName());
            headers.add(AmenityPlace.CHURCHES.getPlaceName());
            headers.add(AmenityPlace.ARCHAEOLOGICAL_SITES.getPlaceName());
            headers.add(AmenityPlace.MONUMENTS.getPlaceName());
            headers.add(AmenityPlace.ARTWOKS.getPlaceName());
            pageName = "Cosa Visitare";
        }

        listAdapter.setHeaders(headers);
        listView.setAdapter(listAdapter);

        for (String header:headers) {
            this.values.put(header, new ArrayList<>());
            listAdapter.setData(this.values);
        }

        final RadioGroup rg = findViewById(R.id.locationRadioGroup);

        rg.check(rg.getChildAt(0).getId());

        this.loadingDialog = new LoadingDialog(this);
        this.loadingDialog.startLoadingDialog();

        if (InternetUtilities.getIsNetworkConnected()) {
            if(type.equals("Food")){
                getAllFoodAmenities("Cesena", listAdapter);
            }else if(type.equals("Service")){
                getAllServiceAmenities("Cesena", listAdapter);
            }else{
                getAllCultureAmenities("Cesena", listAdapter);
            }
        } else {
            InternetUtilities.getSnackbar().show();
        }

        rg.setOnCheckedChangeListener((group, checkedId) -> {
            switch(checkedId){
                case -1:
                    Utility.showNeutralAlert(getString(R.string.attenzione),getString(R.string.nessuna_categoria_scelta), PlacesActivity.this);
                    break;
                case R.id.cesenaRadioButton:
                    if (InternetUtilities.getIsNetworkConnected()) {
                        this.loadingDialog.startLoadingDialog();
                        if(type.equals("Food")){
                            getAllFoodAmenities("Cesena", listAdapter);
                        }else if(type.equals("Service")){
                            getAllServiceAmenities("Cesena", listAdapter);
                        }else{
                            getAllCultureAmenities("Cesena", listAdapter);
                        }
                    } else {
                        InternetUtilities.getSnackbar().show();
                    }
                    break;
                case R.id.forliRadioButton:
                    if (InternetUtilities.getIsNetworkConnected()) {
                        this.loadingDialog.startLoadingDialog();
                        if(type.equals("Food")){
                            getAllFoodAmenities("Forlì", listAdapter);
                        }else if(type.equals("Service")){
                            getAllServiceAmenities("Forlì", listAdapter);
                        }else{
                            getAllCultureAmenities("Forlì", listAdapter);
                        }
                    } else {
                        InternetUtilities.getSnackbar().show();
                    }
                    break;
            }
        });

        Utility.setUpToolbar(this, pageName);
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    private void getAllFoodAmenities(final String city, final PlaceExpandableListAdapter listAdapter){
        final List<PlaceItem> restaurants = new ArrayList<>();
        final List<PlaceItem> cafe = new ArrayList<>();
        final List<PlaceItem> pub = new ArrayList<>();
        final List<PlaceItem> fastFood = new ArrayList<>();
        final List<PlaceItem> iceCreamShops = new ArrayList<>();

        OsmUtilities.getAmenities(city, AmenityPlace.RESTAURANTS.getAmenityName(), restaurants);
        OsmUtilities.getAmenities(city, AmenityPlace.CAFE.getAmenityName(), cafe);
        OsmUtilities.getAmenities(city, AmenityPlace.PUB.getAmenityName(), pub);
        OsmUtilities.getAmenities(city, AmenityPlace.FAST_FOOD.getAmenityName(), fastFood);
        OsmUtilities.getAmenities(city, AmenityPlace.ICE_CREAM_SHOPS.getAmenityName(), iceCreamShops);

        this.values.put(AmenityPlace.RESTAURANTS.getPlaceName(), restaurants);
        this.values.put(AmenityPlace.PUB.getPlaceName(), pub);
        this.values.put(AmenityPlace.CAFE.getPlaceName(), cafe);
        this.values.put(AmenityPlace.FAST_FOOD.getPlaceName(), fastFood);
        this.values.put(AmenityPlace.ICE_CREAM_SHOPS.getPlaceName(), iceCreamShops);
        listAdapter.setData(this.values);
        listAdapter.notifyDataSetChanged();

        this.loadingDialog.dismissDialog();
    }

    private void getAllServiceAmenities(final String city, final PlaceExpandableListAdapter listAdapter){
        final List<PlaceItem> banks = new ArrayList<>();
        final List<PlaceItem> post_offices = new ArrayList<>();
        final List<PlaceItem> hospitals = new ArrayList<>();
        final List<PlaceItem> pharmacies = new ArrayList<>();
        final List<PlaceItem> fire_stations = new ArrayList<>();
        final List<PlaceItem> police = new ArrayList<>();
        final List<PlaceItem> supermarkets = new ArrayList<>();

        OsmUtilities.getAmenities(city, AmenityPlace.BANKS.getAmenityName(), banks);
        OsmUtilities.getAmenities(city, AmenityPlace.POST_OFFICES.getAmenityName(), post_offices);
        OsmUtilities.getAmenities(city, AmenityPlace.HOSPITALS.getAmenityName(), hospitals);
        OsmUtilities.getAmenities(city, AmenityPlace.PHARMACIES.getAmenityName(), pharmacies);
        OsmUtilities.getAmenities(city, AmenityPlace.FIRE_STATIONS.getAmenityName(), fire_stations);
        OsmUtilities.getAmenities(city, AmenityPlace.POLICE.getAmenityName(), police);
        OsmUtilities.getAmenities(city, AmenityPlace.SUPERMARKETS.getAmenityName(), supermarkets);

        this.values.put(AmenityPlace.BANKS.getPlaceName(), banks);
        this.values.put(AmenityPlace.POST_OFFICES.getPlaceName(), post_offices);
        this.values.put(AmenityPlace.HOSPITALS.getPlaceName(), hospitals);
        this.values.put(AmenityPlace.PHARMACIES.getPlaceName(), pharmacies);
        this.values.put(AmenityPlace.FIRE_STATIONS.getPlaceName(), fire_stations);
        this.values.put(AmenityPlace.POLICE.getPlaceName(), police);
        this.values.put(AmenityPlace.SUPERMARKETS.getPlaceName(), supermarkets);
        listAdapter.setData(this.values);
        listAdapter.notifyDataSetChanged();

        this.loadingDialog.dismissDialog();
    }

    private void getAllCultureAmenities(final String city, final PlaceExpandableListAdapter listAdapter){
        final List<PlaceItem> cinema = new ArrayList<>();
        final List<PlaceItem> theaters = new ArrayList<>();
        final List<PlaceItem> libraries = new ArrayList<>();
        final List<PlaceItem> churches = new ArrayList<>();
        final List<PlaceItem> archaeologicalSites = new ArrayList<>();
        final List<PlaceItem> monuments = new ArrayList<>();
        final List<PlaceItem> artWorks = new ArrayList<>();
        final List<PlaceItem> attractions = new ArrayList<>();

        OsmUtilities.getAmenities(city, AmenityPlace.CINEMA.getAmenityName(), cinema);
        OsmUtilities.getAmenities(city, AmenityPlace.THEATERS.getAmenityName(), theaters);
        OsmUtilities.getAmenities(city, AmenityPlace.LIBRARIES.getAmenityName(), libraries);
        OsmUtilities.getAmenities(city, AmenityPlace.CHURCHES.getAmenityName(), churches);
        OsmUtilities.getAmenities(city, AmenityPlace.ARCHAEOLOGICAL_SITES.getAmenityName(), archaeologicalSites);
        OsmUtilities.getAmenities(city, AmenityPlace.MONUMENTS.getAmenityName(), monuments);
        OsmUtilities.getAmenities(city, AmenityPlace.ARTWOKS.getAmenityName(), artWorks);
        OsmUtilities.getAmenities(city, AmenityPlace.ATTRACTIONS.getAmenityName(), attractions);

        this.values.put(AmenityPlace.CINEMA.getPlaceName(), cinema);
        this.values.put(AmenityPlace.THEATERS.getPlaceName(), theaters);
        this.values.put(AmenityPlace.LIBRARIES.getPlaceName(), libraries);
        this.values.put(AmenityPlace.CHURCHES.getPlaceName(), churches);
        this.values.put(AmenityPlace.ARCHAEOLOGICAL_SITES.getPlaceName(), archaeologicalSites);
        this.values.put(AmenityPlace.MONUMENTS.getPlaceName(), monuments);
        this.values.put(AmenityPlace.ARTWOKS.getPlaceName(), artWorks);
        this.values.put(AmenityPlace.ATTRACTIONS.getPlaceName(), attractions);
        listAdapter.setData(this.values);
        listAdapter.notifyDataSetChanged();

        this.loadingDialog.dismissDialog();
    }
}
