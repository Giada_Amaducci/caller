package com.example.call_er;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.call_er.Utilities.InternetUtilities;
import com.example.call_er.Utilities.LoadingDialog;
import com.example.call_er.Utilities.Utility;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    private LoadingDialog loadingDialog;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_login);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.loginConstraintLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI(){
        this.loadingDialog = new LoadingDialog(this);

        this.findViewById(R.id.loginContainedButton).setOnClickListener(v -> {
            final TextInputLayout emailField = findViewById(R.id.emailOutlinedTextField);
            final TextInputLayout passwordField = findViewById(R.id.passwordOutlinedTextField);

            this.loadingDialog.startLoadingDialog();

            if(emailField.getEditText() != null && passwordField.getEditText() != null){
                final String email = emailField.getEditText().getText().toString();
                final String password = passwordField.getEditText().getText().toString();

                if (InternetUtilities.getIsNetworkConnected()) {
                    checkLogin(LoginActivity.this,email,password);
                } else {
                    InternetUtilities.getSnackbar().show();
                }
            }
        });

        this.findViewById(R.id.registrationContainedButton).setOnClickListener(v -> {
            final Intent i = new Intent(getApplicationContext(), RegistrationActivity.class);
            startActivity(i);
        });

        this.findViewById(R.id.forgotPasswordTextView).setOnClickListener(v -> {
            final Intent i = new Intent(getApplicationContext(), ForgotPasswordActivity.class);
            startActivity(i);
        });

        Utility.setUpToolbar(this, this.getString(R.string.app_name));
    }

    private void checkLogin(final Activity activity, final String email, final String password){
        final String url ="https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("result").equals("success")) {
                            final JSONObject user = jsonObject.getJSONObject("user");
                            Utility.saveUserData(LoginActivity.this, email, user.getString("username"),
                                                                                    user.getString("image"),
                                                                                    user.getString("number"),
                                                                                    user.getString("id"),
                                                                                    user.getString("role"));
                            this.loadingDialog.dismissDialog();
                            final Intent i = new Intent(activity.getApplicationContext(), MainActivity.class);
                            activity.startActivity(i);
                        }else{
                            Utility.showNeutralAlert(activity.getString(R.string.attenzione), jsonObject.getString("message"), activity);
                            this.loadingDialog.dismissDialog();
                        }
                    } catch (final JSONException e) {
                        Log.e(activity.getString(R.string.error), e.toString());
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError error) {
                        Log.e(activity.getString(R.string.error), error.toString());
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                final Map<String,String> params = new HashMap<>();
                params.put("type", "login");
                params.put("email", email);
                params.put("password", password);
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

}
