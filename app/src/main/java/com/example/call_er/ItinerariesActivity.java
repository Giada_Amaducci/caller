package com.example.call_er;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.RadioGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.call_er.Adapters.ListAdapter.ItinerariesListAdapter;
import com.example.call_er.Items.ItineraryItem;
import com.example.call_er.Items.PlaceItem;
import com.example.call_er.Utilities.Extra;
import com.example.call_er.Utilities.FileUtilities;
import com.example.call_er.Utilities.InternetUtilities;
import com.example.call_er.Utilities.LoadingDialog;
import com.example.call_er.Utilities.UserManagement;
import com.example.call_er.Utilities.Utility;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ItinerariesActivity extends AppCompatActivity {

    private ItinerariesListAdapter listAdapter;
    private List<ItineraryItem> values;

    @Override
    public void onBackPressed() {
        if(this.getClass().equals(ItinerariesActivity.class)){
            final Intent i = new Intent(getApplicationContext(), MainActivity.class);
            this.startActivity(i);
        }else{
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.nav_onlysearch, menu);
        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(final String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(final String newText) {
                listAdapter.getFilter().filter(newText);
                return true;
            }
        });
        return true;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_itineraries);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.itinerariesConstraintLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI(){
        final String nameActivity = this.getIntent().getStringExtra(Extra.ACTIVITY_NAME.getTypeExtra());

        final ListView listView = this.findViewById(R.id.itinerariesListView);
        this.listAdapter = new ItinerariesListAdapter(getApplicationContext(),this, nameActivity);
        listView.setAdapter(this.listAdapter);

        final RadioGroup rg = this.findViewById(R.id.locationRadioGroup);
        final LoadingDialog loadingDialog = new LoadingDialog(ItinerariesActivity.this);

        final String user_id = FileUtilities.getData( getCacheDir()+"/mtj.txt").get(UserManagement.ID.getId());

        if (nameActivity != null && nameActivity.equals("PlaceActivity")) {
            final String name = this.getIntent().getStringExtra(Extra.NAME.getTypeExtra());
            final String road = this.getIntent().getStringExtra(Extra.ROAD.getTypeExtra());
            final String address = this.getIntent().getStringExtra(Extra.ADDRESS.getTypeExtra());
            final String city = this.getIntent().getStringExtra(Extra.CITY.getTypeExtra());
            final String lat = this.getIntent().getStringExtra(Extra.LAT.getTypeExtra());
            final String lon = this.getIntent().getStringExtra(Extra.LON.getTypeExtra());
            final PlaceItem placeItem = new PlaceItem(name, address, city, road, lon, lat);
            this.listAdapter.setPlaceToAdd(placeItem);

            final int cityId;
            if (city != null) {
                if (city.equals("Cesena")) {
                    cityId = 0;
                }else{
                    cityId = 1;
                }
            }else{
                cityId = -1;
            }

            rg.check(rg.getChildAt(cityId).getId());

            for (int i = 0; i < rg.getChildCount(); i++) {
                rg.getChildAt(i).setEnabled(false);
            }

            loadingDialog.startLoadingDialog();

            if (InternetUtilities.getIsNetworkConnected()) {
                getItineraries(city, user_id, loadingDialog, nameActivity);
            } else {
                InternetUtilities.getSnackbar().show();
            }
        }

        this.values = new ArrayList<>();

        rg.setOnCheckedChangeListener((group, checkedId) -> {
            loadingDialog.startLoadingDialog();

            switch(checkedId){
                case -1:
                    Utility.showNeutralAlert(getString(R.string.attenzione),getString(R.string.nessuna_categoria_scelta), ItinerariesActivity.this);
                    break;
                case R.id.cesenaRadioButton:
                    if (InternetUtilities.getIsNetworkConnected()) {
                        getItineraries("Cesena", user_id, loadingDialog, nameActivity);
                    } else {
                        InternetUtilities.getSnackbar().show();
                    }
                    break;
                case R.id.forliRadioButton:
                    if (InternetUtilities.getIsNetworkConnected()) {
                        getItineraries("Forlì", user_id, loadingDialog, nameActivity);
                    } else {
                        InternetUtilities.getSnackbar().show();
                    }
                    break;
            }
        });

        final FloatingActionButton fabAdd = this.findViewById(R.id.fabitineariesActivity);
        if (nameActivity != null && nameActivity.equals("MainActivity")) {
            fabAdd.show();
            fabAdd.setOnClickListener(v -> {
                final String role = FileUtilities.getData( getCacheDir()+"/mtj.txt").get(UserManagement.ROLE.getId());

                if(role.equals("organizer")){
                    final Intent i = new Intent(getApplicationContext(), AddItineraryOrganizerActivity.class);
                    i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "ItinerariesActivity");
                    i.putExtra(Extra.CITY.getTypeExtra(), rg.getCheckedRadioButtonId());
                    startActivity(i);
                }else if (role.equals("user")){
                    final Intent i = new Intent(getApplicationContext(), AddItineraryUserActivity.class);
                    i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "ItinerariesActivity");
                    i.putExtra(Extra.CITY.getTypeExtra(), rg.getCheckedRadioButtonId());
                    startActivity(i);
                }else{
                    Utility.showNeutralAlert("Non puoi creare un itinerario fino alla tua approvazione come organizzatore!", ItinerariesActivity.this);
                }
            });
        }else{
            fabAdd.hide();
        }

        Utility.setUpToolbar(this, "I Tuoi Itinerari");
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    private void getItineraries(final String city, final String user_id, final LoadingDialog loadingDialog, final String nameActivity){
        final String url = "https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("result").equals("success")) {
                            values.clear();
                            final String role = FileUtilities.getData( getCacheDir()+"/mtj.txt").get(UserManagement.ROLE.getId());
                            if(!jsonObject.getString("message").equals("Nessun itinerario creato")) {
                                final JSONArray jsonArray = new JSONArray(jsonObject.getString("values"));
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    final JSONObject itinerary = (JSONObject) jsonArray.get(i);
                                    final String id = itinerary.getString("itinerary_id");
                                    final String name = itinerary.getString("name");
                                    final ItineraryItem itineraryItem = new ItineraryItem(id, name, city);
                                    if(role.equals("user")){
                                        final String departure_date = itinerary.getString("departure_date");
                                        final String return_date = itinerary.getString("return_date");
                                        final String creator_name = itinerary.getString("creator_name");
                                        itineraryItem.setDepartureDate(departure_date);
                                        itineraryItem.setReturnDate(return_date);
                                        itineraryItem.setCreatorName(creator_name);
                                    }else{
                                        final String user_count = itinerary.getString("user_count");
                                        itineraryItem.setUsers(user_count);
                                    }
                                    values.add(itineraryItem);
                                }
                            }
                            listAdapter.setData(values);
                        }else{
                            Utility.showNeutralAlert(getString(R.string.attenzione),jsonObject.getString("message"), ItinerariesActivity.this);
                        }
                        loadingDialog.dismissDialog();
                    } catch (final JSONException e) {
                        Log.e(getString(R.string.error), e.toString());
                    }
                },
                error -> Log.e(getString(R.string.error), error.toString())){
            @Override
            protected Map<String,String> getParams(){
                final Map<String, String> params = new HashMap<>();
                params.put("type", "getUserOrOrganizerItineraries");
                params.put("role", FileUtilities.getData( getCacheDir()+"/mtj.txt").get(UserManagement.ROLE.getId()));
                params.put("user_id", user_id);
                params.put("city", city);
                if(nameActivity.equals("PlaceActivity")){
                    params.put("only_created_itinerary", "yes");
                }else{
                    params.put("only_created_itinerary", "no");
                }
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }
}
