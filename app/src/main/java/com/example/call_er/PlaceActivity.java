package com.example.call_er;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.call_er.Adapters.ImageSliderAdapter.ImageSliderAdapter;
import com.example.call_er.Items.ImageItem;
import com.example.call_er.Items.PlaceItem;
import com.example.call_er.Utilities.Extra;
import com.example.call_er.Utilities.FileUtilities;
import com.example.call_er.Utilities.InternetUtilities;
import com.example.call_er.Utilities.OsmUtilities;
import com.example.call_er.Utilities.UserManagement;
import com.example.call_er.Utilities.Utility;
import com.smarteist.autoimageslider.SliderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class PlaceActivity extends AppCompatActivity {

    private PlaceItem placeItem;
    private String placeType;
    private String activityName;
    private List<ImageItem> images;

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            this.onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if(this.getClass().equals(PlaceActivity.class)) {
            final Intent i;
            if (this.activityName.equals("SearchPlaceActivity")) {
                i = new Intent(getApplicationContext(), SearchPlaceActivity.class);
                i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "PlaceActivity");
            } else if(this.activityName.equals("VisitedPlaceActivity")) {
                i = new Intent(getApplicationContext(), VisitedPlacesActivity.class);
            }else{
                i = new Intent(getApplicationContext(), PlacesActivity.class);
                i.putExtra(Extra.TYPE.getTypeExtra(), this.placeType);
            }
            startActivity(i);
        }else{
            finish();
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_place);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.placeConstraintLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI(){
        this.activityName = this.getIntent().getStringExtra(Extra.ACTIVITY_NAME.getTypeExtra());
        if (this.activityName != null && (this.activityName.equals("PlacesActivity") || this.activityName.equals("PlaceMapActivity"))) {
            this.placeType = this.getIntent().getStringExtra(Extra.TYPE.getTypeExtra());
        }
        if( this.placeType == null){
            this.activityName = "SearchPlaceActivity";
        }
        final String name = this.getIntent().getStringExtra(Extra.NAME.getTypeExtra());
        final String address = this.getIntent().getStringExtra(Extra.ADDRESS.getTypeExtra());
        final String road = this.getIntent().getStringExtra(Extra.ROAD.getTypeExtra());
        final String city = this.getIntent().getStringExtra(Extra.CITY.getTypeExtra());
        final String lat = this.getIntent().getStringExtra(Extra.LAT.getTypeExtra());
        final String lon = this.getIntent().getStringExtra(Extra.LON.getTypeExtra());
        this.placeItem = new PlaceItem(name,address,city,road,lon,lat);

        final TextView addressPlaceTextView = this.findViewById(R.id.addressPlaceTextView);
        final TextView telephoneAttrValueTextView = this.findViewById(R.id.telephoneAttrValueTextView);
        final TextView websiteAttrValueTextView = this.findViewById(R.id.websiteAttrValueTextView);
        final TextView emailAttrValueTextView = this.findViewById(R.id.emailAttrValueTextView);

        this.images = new ArrayList<>();

        final SliderView sliderView = findViewById(R.id.imageSlider);
        final ImageSliderAdapter adapter = new ImageSliderAdapter(this, this);
        sliderView.setSliderAdapter(adapter);

        addressPlaceTextView.setText(this.placeItem.getAddress());

        if (InternetUtilities.getIsNetworkConnected()) {
            OsmUtilities.getAmenity(this.placeItem.getCity(), this.placeItem.getName(), this.placeItem.getRoad(), telephoneAttrValueTextView,
                                    websiteAttrValueTextView, emailAttrValueTextView);
            this.getImages(this.placeItem, adapter);
        } else {
            InternetUtilities.getSnackbar().show();
        }

        this.findViewById(R.id.searchInMapButton).setOnClickListener(v -> {
            final Intent i = new Intent(getApplicationContext(), PlaceMapActivity.class);
            i.putExtra(Extra.NAME.getTypeExtra(), placeItem.getName());
            i.putExtra(Extra.ROAD.getTypeExtra(), placeItem.getRoad());
            i.putExtra(Extra.ADDRESS.getTypeExtra(), placeItem.getAddress());
            i.putExtra(Extra.LAT.getTypeExtra(), placeItem.getLat());
            i.putExtra(Extra.LON.getTypeExtra(), placeItem.getLon());
            i.putExtra(Extra.CITY.getTypeExtra(), placeItem.getCity());
            i.putExtra(Extra.TYPE.getTypeExtra(), placeType);
            startActivity(i);
        });

        final TextView addVisitedPlaceTextView = this.findViewById(R.id.addVisitedPlaceTextView);
        addVisitedPlaceTextView.setOnClickListener(v -> addVisitedPlace());

        final TextView addItineraryTextView = this.findViewById(R.id.addItineraryTextView);
        addItineraryTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent i = new Intent(getApplicationContext(), ItinerariesActivity.class);
                i.putExtra(Extra.NAME.getTypeExtra(), placeItem.getName());
                i.putExtra(Extra.ROAD.getTypeExtra(), placeItem.getRoad());
                i.putExtra(Extra.ADDRESS.getTypeExtra(), placeItem.getAddress());
                i.putExtra(Extra.LAT.getTypeExtra(), placeItem.getLat());
                i.putExtra(Extra.LON.getTypeExtra(), placeItem.getLon());
                i.putExtra(Extra.CITY.getTypeExtra(), placeItem.getCity());
                i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "PlaceActivity");
                i.putExtra(Extra.TYPE.getTypeExtra(), placeType);
                startActivity(i);
            }
        });

        this.findViewById(R.id.ReviewsButton).setOnClickListener(v -> {
            final Intent i = new Intent(getApplicationContext(), ReviewsActivity.class);
            i.putExtra(Extra.NAME.getTypeExtra(), placeItem.getName());
            i.putExtra(Extra.ROAD.getTypeExtra(), placeItem.getRoad());
            i.putExtra(Extra.ADDRESS.getTypeExtra(), placeItem.getAddress());
            i.putExtra(Extra.LAT.getTypeExtra(), placeItem.getLat());
            i.putExtra(Extra.LON.getTypeExtra(), placeItem.getLon());
            i.putExtra(Extra.CITY.getTypeExtra(), placeItem.getCity());
            i.putExtra(Extra.TYPE.getTypeExtra(), placeType);
            startActivity(i);
        });

        Utility.setUpToolbar(this, name);
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    private void getImages(final PlaceItem placeItem, ImageSliderAdapter adapter){
        final Map<String, String> params = new HashMap<>();
        params.put("type", "getPlaceImages");
        params.put("city", placeItem.getCity());
        params.put("road", placeItem.getRoad());
        params.put("name", placeItem.getName());
        params.put("user_id", FileUtilities.getData( getCacheDir()+"/mtj.txt").get(UserManagement.ID.getId()));
        this.getPlaceImages(params, adapter);
    }

    private void getPlaceImages(final Map<String, String> params, final ImageSliderAdapter adapter){
        final String url = "https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("result").equals("success")) {
                            if(!jsonObject.getString("message").equals("nessuna foto")) {
                                final JSONArray jsonArray = new JSONArray(jsonObject.getString("values"));
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    final JSONObject image = (JSONObject) jsonArray.get(i);
                                    final String imageString = image.getString("image");
                                    final String vote = image.getString("vote");
                                    final String id = image.getString("id");
                                    final boolean isVoted = !(image.getString("isVoted")).equals("0");
                                    final ImageItem imageItem = new ImageItem(id, imageString, vote, isVoted);
                                    images.add(imageItem);
                                }
                            }
                            adapter.renewItems(images);
                        }else{
                            Utility.showNeutralAlert(getString(R.string.attenzione),jsonObject.getString("message"), PlaceActivity.this);
                        }
                    } catch (final JSONException e) {
                        Log.e(getString(R.string.error), e.toString());
                    }
                },
                error -> Log.e(getString(R.string.error), error.toString())){
            @Override
            protected Map<String,String> getParams(){
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

    private void addVisitedPlace(){
        final String url = "https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("result").equals("success")) {
                            Utility.showToast(jsonObject.getString("message"), PlaceActivity.this);
                        } else {
                            Utility.showNeutralAlert(getString(R.string.attenzione), jsonObject.getString("message"), PlaceActivity.this);
                        }
                    } catch (final JSONException e) {
                        Log.e(getString(R.string.error), e.toString());
                    }
                },
                error -> Log.e(getString(R.string.error), error.toString())) {
            @Override
            protected Map<String, String> getParams() {
                final Map<String, String> params = new HashMap<>();
                params.put("type", "addVisitedPlace");
                params.put("user_id", FileUtilities.getData( getCacheDir()+"/mtj.txt").get(UserManagement.ID.getId()));
                params.put("name", placeItem.getName());
                params.put("road", placeItem.getRoad());
                params.put("address", placeItem.getAddress());
                params.put("city", placeItem.getCity());
                params.put("lat", placeItem.getLat());
                params.put("lon", placeItem.getLon());
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }
}
