package com.example.call_er;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.call_er.Adapters.ImageSliderAdapter.PublicItineraryImageSliderAdapter;
import com.example.call_er.Items.ImageItem;
import com.example.call_er.Items.ItineraryItem;
import com.example.call_er.Utilities.Extra;
import com.example.call_er.Utilities.InternetUtilities;
import com.example.call_er.Utilities.Utility;
import com.smarteist.autoimageslider.SliderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

public class PublicItineraryActivity extends AppCompatActivity {

    private ItineraryItem itineraryItem;
    private List<ImageItem> images;
    private PublicItineraryImageSliderAdapter adapter;

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            this.onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if(this.getClass().equals(PublicItineraryActivity.class)){
            final Intent i = new Intent(getApplicationContext(), PublicItinerariesActivity.class);
            i.putExtra(Extra.NAME.getTypeExtra(), itineraryItem.getName());
            i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "PublicItineraryActivity");
            i.putExtra(Extra.CREATOR_NAME.getTypeExtra(), itineraryItem.getCreatorName());
            i.putExtra(Extra.ID.getTypeExtra(), itineraryItem.getId());
            i.putExtra(Extra.CITY.getTypeExtra(), itineraryItem.getCity());
            i.putExtra(Extra.AVG_VOTE.getTypeExtra(), itineraryItem.getAvg());
            i.putExtra(Extra.USER_COUNT.getTypeExtra(), itineraryItem.getVotes());
            this.startActivity(i);
        }else{
            finish();
        }
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_public_itinerary);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.publicItineraryConstraintLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI(){
        final String title = this.getIntent().getStringExtra(Extra.NAME.getTypeExtra());
        final String itineraryId = this.getIntent().getStringExtra(Extra.ID.getTypeExtra());
        final String city = this.getIntent().getStringExtra(Extra.CITY.getTypeExtra());
        final String creator_name = this.getIntent().getStringExtra(Extra.CREATOR_NAME.getTypeExtra());
        final String votes = this.getIntent().getStringExtra(Extra.USER_COUNT.getTypeExtra());
        final String avg = this.getIntent().getStringExtra(Extra.AVG_VOTE.getTypeExtra());

        this.itineraryItem = new ItineraryItem(itineraryId, title, city);
        this.itineraryItem.setCreatorName(creator_name);
        this.itineraryItem.setAvg(avg);
        this.itineraryItem.setVotes(votes);

        final RadioGroup radioGroup = this.findViewById(R.id.searchRadioGroup);
        final int cityId;
        if (city != null) {
            if (city.equals("Cesena")) {
                cityId = 0;
            }else{
                cityId = 1;
            }
        }else{
            cityId = -1;
        }

        radioGroup.check(radioGroup.getChildAt(cityId).getId());

        for (int i = 0; i < radioGroup.getChildCount(); i++) {
            radioGroup.getChildAt(i).setEnabled(false);
        }

        this.images = new ArrayList<>();

        final SliderView sliderView = findViewById(R.id.imageSlider);
        this.adapter = new PublicItineraryImageSliderAdapter(this, this);
        sliderView.setSliderAdapter(adapter);

        if (InternetUtilities.getIsNetworkConnected()) {
            this.getPublicItineraryImages(this.itineraryItem.getId());
        } else {
            InternetUtilities.getSnackbar().show();
        }

        final RatingBar ratingBar = findViewById(R.id.ratingBar);

        if(!itineraryItem.getAvg().equals("null")){
            ratingBar.setRating(Float.parseFloat(itineraryItem.getAvg()));
        }else{
            ratingBar.setRating(0);
        }


        final TextView voteAvgTextView = this.findViewById(R.id.voteAvgTextView);
        final TextView countTextView = this.findViewById(R.id.voteTextView);
        if(!itineraryItem.getAvg().equals("null")){
            voteAvgTextView.setText(String.format(Locale.ITALIAN, "%.2f",Float.parseFloat(itineraryItem.getAvg())));
        }else{
            voteAvgTextView.setText("0");
        }
        countTextView.setText(String.format("(%s)", this.itineraryItem.getVotes()));

        this.findViewById(R.id.addItineraryTextView).setOnClickListener(v -> {
            final Intent i = new Intent(getApplicationContext(), ShareItineraryActivity.class);
            i.putExtra(Extra.NAME.getTypeExtra(), itineraryItem.getName());
            i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "PublicItineraryActivity");
            i.putExtra(Extra.CREATOR_NAME.getTypeExtra(), itineraryItem.getCreatorName());
            i.putExtra(Extra.ID.getTypeExtra(), itineraryItem.getId());
            i.putExtra(Extra.CITY.getTypeExtra(), itineraryItem.getCity());
            i.putExtra(Extra.AVG_VOTE.getTypeExtra(), itineraryItem.getAvg());
            i.putExtra(Extra.USER_COUNT.getTypeExtra(), itineraryItem.getVotes());
            this.startActivity(i);
        });

        this.findViewById(R.id.reviewButton).setOnClickListener(v -> {
            final Intent i = new Intent(getApplicationContext(), ReviewsActivity.class);
            i.putExtra(Extra.NAME.getTypeExtra(), itineraryItem.getName());
            i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "PublicItineraryActivity");
            i.putExtra(Extra.CREATOR_NAME.getTypeExtra(), itineraryItem.getCreatorName());
            i.putExtra(Extra.ID.getTypeExtra(), itineraryItem.getId());
            i.putExtra(Extra.CITY.getTypeExtra(), itineraryItem.getCity());
            i.putExtra(Extra.AVG_VOTE.getTypeExtra(), itineraryItem.getAvg());
            i.putExtra(Extra.USER_COUNT.getTypeExtra(), itineraryItem.getVotes());
            this.startActivity(i);
        });

        this.findViewById(R.id.placesListButton).setOnClickListener(v -> {
            final Intent i = new Intent(getApplicationContext(), PlacesInItineraryActivity.class);
            i.putExtra(Extra.NAME.getTypeExtra(), itineraryItem.getName());
            i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "PublicItineraryActivity");
            i.putExtra(Extra.CREATOR_NAME.getTypeExtra(), itineraryItem.getCreatorName());
            i.putExtra(Extra.ID.getTypeExtra(), itineraryItem.getId());
            i.putExtra(Extra.CITY.getTypeExtra(), itineraryItem.getCity());
            i.putExtra(Extra.AVG_VOTE.getTypeExtra(), itineraryItem.getAvg());
            i.putExtra(Extra.USER_COUNT.getTypeExtra(), itineraryItem.getVotes());
            this.startActivity(i);
        });

        Utility.setUpToolbar(this, this.itineraryItem.getName());
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    private void getPublicItineraryImages(final String itinerary_id){
        final String url = "https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("result").equals("success")) {
                            if(!jsonObject.getString("message").equals("nessuna foto")) {
                                final JSONArray jsonArray = new JSONArray(jsonObject.getString("values"));
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    final JSONObject image = (JSONObject) jsonArray.get(i);
                                    final String imageString = image.getString("image");
                                    final String id = image.getString("id");
                                    final ImageItem imageItem = new ImageItem(id, imageString);
                                    images.add(imageItem);
                                }
                            }
                            adapter.renewItems(images);
                        }else{
                            Utility.showNeutralAlert(getString(R.string.attenzione),jsonObject.getString("message"), PublicItineraryActivity.this);
                        }
                    } catch (final JSONException e) {
                        Log.e(getString(R.string.error), e.toString());
                    }
                },
                error -> Log.e(getString(R.string.error), error.toString())){
            @Override
            protected Map<String,String> getParams(){
                final Map<String, String> params = new HashMap<>();
                params.put("type", "getPublicItineraryImages");
                params.put("itinerary_id", itinerary_id);
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }
}
