package com.example.call_er.Utilities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.view.View;

import androidx.annotation.NonNull;

import com.android.volley.RequestQueue;
import com.example.call_er.R;
import com.google.android.material.snackbar.Snackbar;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class InternetUtilities {

    private static Boolean isNetworkConnected = false;
    private static Snackbar snackbar;
    private static RequestQueue requestQueue;

    private static ConnectivityManager.NetworkCallback networkCallback = new ConnectivityManager.NetworkCallback(){
        @Override
        public void onAvailable(@NonNull Network network) {
            super.onAvailable(network);
            isNetworkConnected = true;
            snackbar.dismiss();
        }

        @Override
        public void onLost(@NonNull Network network) {
            super.onLost(network);
            isNetworkConnected = false;
            snackbar.show();
        }
    };

    public static void registerNetworkCallback(final Activity activity) {
        final ConnectivityManager connectivityManager =
                (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            //api 24, android 7
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                connectivityManager.registerDefaultNetworkCallback(networkCallback);
            } else {
                //Class deprecated since API 29 (android 10) but used for android 5 and 6
                final NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
                isNetworkConnected = networkInfo != null && networkInfo.isConnected();
            }
        } else {
            isNetworkConnected = false;
        }
    }

    public static RequestQueue getRequestQueue() {
        return requestQueue;
    }

    public static void setRequestQueue(final RequestQueue request) {
        requestQueue = request;
    }

    public static Boolean getIsNetworkConnected() {
        return isNetworkConnected;
    }

    public static void makeSnackbar(final Activity activity, final int id){
        snackbar = Snackbar.make(
                activity.findViewById(id),
                R.string.no_internet_available,
                Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.settings, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // Build intent that displays the App settings screen.
                        setSettingsIntent(activity);
                    }
                });
    }

    static ConnectivityManager.NetworkCallback getNetworkCallback(){
        return networkCallback;
    }

    public static Snackbar getSnackbar(){
        return snackbar;
    }

    public static void onActivityStop(final Context context){
        if (requestQueue != null) {
            //cancel the request to osm
            requestQueue.cancelAll(OsmUtilities.OSM_REQUEST_TAG);
        }
        final ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            //unregistered the callback
            connectivityManager.unregisterNetworkCallback(getNetworkCallback());
        }
    }

    private static void setSettingsIntent(final Activity activity) {
        final Intent intent = new Intent();
        intent.setAction(Settings.ACTION_WIRELESS_SETTINGS);
        intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
        if (intent.resolveActivity(activity.getPackageManager()) != null) {
            activity.startActivity(intent);
        }
    }
}

