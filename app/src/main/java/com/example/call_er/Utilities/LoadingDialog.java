package com.example.call_er.Utilities;

import android.app.Activity;
import android.view.LayoutInflater;

import androidx.appcompat.app.AlertDialog;

import com.example.call_er.R;


public class LoadingDialog {

    private final Activity activity;
    private AlertDialog alertDialog;

    public LoadingDialog(final Activity activity) {
        this.activity = activity;
    }

    public void startLoadingDialog(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        final LayoutInflater inflater = activity.getLayoutInflater();
        builder.setView(inflater.inflate(R.layout.custom_progressdialog, null));
        builder.setCancelable(false);

        this.alertDialog = builder.create();
        this.alertDialog.show();
    }

    public void dismissDialog(){
        this.alertDialog.dismiss();
    }
}
