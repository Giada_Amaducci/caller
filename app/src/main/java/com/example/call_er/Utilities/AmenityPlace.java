package com.example.call_er.Utilities;

public enum AmenityPlace {

    RESTAURANTS("restaurant", "Ristoranti"),
    CAFE("cafe", "Caffè"),
    FAST_FOOD("fast+food", "Fast-Food"),
    PUB("pub", "Pub"),
    ICE_CREAM_SHOPS("ice+cream", "Gelaterie"),

    BANKS("bank", "Banche"),
    POST_OFFICES("post+office", "Uffici Postali"),
    HOSPITALS("hospital", "Ospedali"),
    PHARMACIES("pharmacy", "Farmacie"),
    FIRE_STATIONS("fire+station", "Pompieri"),
    POLICE("police", "Carabinieri e Polizia"),
    SUPERMARKETS("supermarket", "Supermercati"),

    CINEMA("cinema", "Cinema"),
    LIBRARIES("library", "Biblioteche"),
    CHURCHES("church", "Chiese"),
    THEATERS("theatre", "Teatri"),
    ARCHAEOLOGICAL_SITES("archaeological+site", "Siti Archeologici"),
    MONUMENTS("monument", "Monumenti"),
    ARTWOKS("artwork", "Opere d'Arte"),
    ATTRACTIONS("attraction", "Attrazioni Turistiche");

    private String amenityName;
    private String placeName;

    AmenityPlace(final String amenityName, final String placeName) {
        this.amenityName = amenityName;
        this.placeName = placeName;
    }

    public String getAmenityName() {
        return amenityName;
    }

    public String getPlaceName() {
        return placeName;
    }
}
