package com.example.call_er.Utilities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;

import com.google.android.gms.location.LocationResult;

import java.util.List;

public class LocationBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, final Intent intent) {
        final LocationResult result = LocationResult.extractResult(intent);
        //if the location is not null
        if (result != null) {
            final List<Location> locations = result.getLocations();
            final String latitude = Double.toString(locations.get(locations.size()-1).getLatitude());
            final String longitude = Double.toString(locations.get(locations.size()-1).getLongitude());
            //send the intent to anyone that is listening for the action "SEND_DATA"
            final Intent i = new Intent("SEND_DATA");
            i.putExtra("location_latitude", latitude);
            i.putExtra("location_longitude", longitude);
            context.sendBroadcast(i);
        }
    }
}
