package com.example.call_er.Utilities;

public enum Extra {
    NAME("name"),
    ACTIVITY_NAME("activityName"),
    EMAIL("email"),
    IMAGE("image"),
    ADDRESS("address"),
    ROAD("road"),
    CITY("city"),
    LAT("lat"),
    LON("lon"),
    TYPE("type"),
    ID("id"),
    DATE("date"),
    DEPARTURE_DATE("departureDate"),
    PLACES("places"),
    NAME_PLACE("placeName"),
    CREATOR_NAME("creator_name"),
    USER_COUNT("user_count"),
    AVG_VOTE("avg_vote"),
    RETURN_DATE("returnDate");

    private String typeExtra;

    Extra(final String typeExtra){
        this.typeExtra = typeExtra;
    }

    public String getTypeExtra() {
        return this.typeExtra;
    }
}
