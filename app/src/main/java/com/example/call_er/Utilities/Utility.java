package com.example.call_er.Utilities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.example.call_er.Items.PlaceItem;
import com.example.call_er.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Random;

public class Utility {

    private static final String ALLOWED_CHARACTERS ="0123456789qwertyuiopasdfghjklzxcvbnm";

    public static void setUpToolbar(AppCompatActivity activity, String title) {
        Toolbar toolbar = activity.findViewById(R.id.app_bar);
        toolbar.setTitle(title);
        activity.setSupportActionBar(toolbar);
    }

    public static void insertFragment(final AppCompatActivity activity, final Fragment fragment, final String tag, final int id) {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .add(id, fragment, tag)
                .commit();
    }

    public static void showNeutralAlert(final String message, final Activity activity) {
        final AlertDialog dialog = new AlertDialog.Builder(activity)
                .setMessage(message)
                .setCancelable(false)
                .setNeutralButton(activity.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
                .create();
        dialog.show();
    }

    public static void showNeutralAlert(final String title, final String message, final Activity activity) {
        final AlertDialog dialog = new AlertDialog.Builder(activity)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setNeutralButton(activity.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
                .create();
        dialog.show();
    }

    public static void showToast(final String message, final Activity activity){
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }

    public static void showToastIntent(final String message, final Activity activity, final Intent i){
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
        activity.startActivity(i);
    }

    public static String formatOSMSring(String string){
        return string.replace(",", ";");
    }

    public static Intent shareImage(final Bitmap image, final Context context){
        final String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ITALY).format(new Date());
        final String name = "JPEG_" + timeStamp + "_.png";
        final ContentResolver resolver = context.getContentResolver();
        final ContentValues contentValues = new ContentValues();
        contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, name + ".jpg");
        contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpg");
        final Uri imageUri = resolver.insert(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
        OutputStream fos = null;
        try {
            fos = resolver.openOutputStream(Objects.requireNonNull(imageUri));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        image.compress(Bitmap.CompressFormat.JPEG, 100, fos);
        if (fos != null) {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        final Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/jpeg");
        share.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        share.putExtra(Intent.EXTRA_STREAM, imageUri);
        return share;
    }

    public static Intent shareText(final String text, final String subject){
        final Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, text);
        return intent;
    }

    public static JSONArray listToJSONArray(final Activity activity, final List<String> list){
        final JSONArray jsonArr = new JSONArray();
        for (int i = 0; i < list.size(); ++i) {
            JSONObject obj;
            try {
                obj = new JSONObject();
                obj.put("name", list.get(i));
                jsonArr.put(obj);
            } catch (final JSONException e) {
                Log.e(activity.getString(R.string.error), e.toString());
            }
        }
        return jsonArr;
    }

    public static List<String> JSONArrayToList(final Activity activity, final JSONArray jsonArray) {
        final List<String> list = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                final JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                list.add(jsonObject.getString("name"));
            } catch (final JSONException e) {
                Log.e(activity.getString(R.string.error), e.toString());
            }
        }
        return list;
    }

    public static String randomPassword() {
        final Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        char tempChar;
        for (int i = 0; i < 9; i++){
            tempChar = (char) (generator.nextInt(96) + 32);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
    }

    public static void saveUserData(final Activity activity, final String email, final String username, final String image, final String number, final String id, final String role){
        final String nameFile = activity.getCacheDir()+"/mtj.txt";
        final List<String> values = new ArrayList<>();
        values.add(email);
        values.add(username);
        values.add(number);
        values.add(id);
        values.add(role);
        values.add(image);
        FileUtilities.saveData(values,nameFile);
    }
}
