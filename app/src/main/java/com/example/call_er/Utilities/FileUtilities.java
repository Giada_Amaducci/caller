package com.example.call_er.Utilities;

import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class FileUtilities {

    public static List<String> getData(final String nameFile) {
        final List<String> lines = new ArrayList<>();
        try {
            final InputStream inputStream = new FileInputStream(nameFile);
            final InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            final BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String receiveString;

            while ( (receiveString = bufferedReader.readLine()) != null ) {
                lines.add(receiveString);
            }
            inputStream.close();
        } catch (IOException e) {
            Log.e("ERROR", e.toString());
        }
        return lines;
    }

    public static void saveData(final List<String> values, final String nameFile){
        createFile(nameFile);
        try(PrintStream ps = new PrintStream(nameFile)){
            for(int i=0; i<values.size(); i++){
                ps.println(values.get(i));
            }
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    public static void removeFile(final String filename){
        File file = new File(filename);
        if (file.exists()){
            file.delete();
        }
    }

    private static void createFile(final String filename){
        File file = new File(filename);
        if (!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
