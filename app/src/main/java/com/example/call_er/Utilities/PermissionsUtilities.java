package com.example.call_er.Utilities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.view.View;

import androidx.core.app.ActivityCompat;

import com.android.volley.BuildConfig;
import com.example.call_er.R;
import com.google.android.material.snackbar.Snackbar;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class PermissionsUtilities {

    public static final int REQUEST_PERMISSIONS_REQUEST_CODE = 0;
    private static final String REQUEST_PERMISSIONS = Manifest.permission.ACCESS_FINE_LOCATION;

    public static boolean checkPermissions(Activity activity) {
        final int permissionState = ActivityCompat.checkSelfPermission(activity, REQUEST_PERMISSIONS);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    public static void requestPermissions(final Activity activity) {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(activity, REQUEST_PERMISSIONS);

        if (shouldProvideRationale) {
            Snackbar.make(
                    activity.findViewById(R.id.mapLayout),
                    R.string.permission_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            requestLocation(activity);
                        }
                    })
                    .show();
        } else {
            requestLocation(activity);
        }
    }

    private static void requestLocation(Activity activity) {
        ActivityCompat.requestPermissions(activity,
                new String[]{REQUEST_PERMISSIONS}, REQUEST_PERMISSIONS_REQUEST_CODE);
    }

    public static void createSnackBar(final Activity activity) {
        Snackbar.make(
                activity.findViewById(R.id.mapLayout),
                R.string.permission_denied_explanation,
                Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.settings, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final Intent intent = new Intent();
                        intent.setAction(
                                Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        final Uri uri = Uri.fromParts("package",
                                BuildConfig.APPLICATION_ID, null);
                        intent.setData(uri);
                        intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                        activity.startActivity(intent);
                    }
                })
                .show();
    }
}
