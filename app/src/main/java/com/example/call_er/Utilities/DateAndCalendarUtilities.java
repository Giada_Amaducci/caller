package com.example.call_er.Utilities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.example.call_er.Items.ItineraryItem;
import com.example.call_er.Items.VisitedPlaceItem;
import com.example.call_er.R;
import com.google.android.material.textfield.TextInputLayout;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

public class DateAndCalendarUtilities {

    public static boolean checkDataInput(String date){
        if (date.length() == 10 && (date.contains("/") || date.contains("-"))) {
            date = date.replace("/", "-");
            if (date.charAt(2) == '-' && date.charAt(5) == '-') {
                String[] values = date.split("-");
                int month = Integer.parseInt(values[1]);
                int year = Integer.parseInt(values[2]);
                int day = Integer.parseInt(values[0]);
                if (month>0 && month<13 && year>1950 && year<2100){
                    Calendar myCalendar = new GregorianCalendar();
                    myCalendar.set(Calendar.YEAR, year);
                    myCalendar.set(Calendar.MONTH, month-1);
                    int numDays = myCalendar.getActualMaximum(Calendar.DATE);
                    return day <= numDays;
                }
            }
        }
        return false;
    }

    public static void showDatePickerDialog(final Context context, final TextView textView,
                                            final VisitedPlaceItem item, final Activity activity){
        final Calendar myCalendar = Calendar.getInstance();
        final int day = myCalendar.get(Calendar.DAY_OF_MONTH);
        final int month = myCalendar.get(Calendar.MONTH);
        final int year = myCalendar.get(Calendar.YEAR);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                final Date date = getDate(item.getDate());
                final Date currentDate = getDate(getDateStringFromInt(year,month,dayOfMonth, false));
                if(Objects.requireNonNull(currentDate).equals(date) || currentDate.after(date)){
                    textView.setText(getDateStringFromInt(year,month,dayOfMonth,true));
                    item.setDate(textView.getText().toString());
                }else{
                    Utility.showNeutralAlert(activity.getString(R.string.attenzione),"La data inserita non è corretta", activity);
                }
            }
        }, year, month, day);
        datePickerDialog.show();
    }

    public static void showDatePickerDialog(final Context context, final TextInputLayout textInputLayout){
        final Calendar myCalendar = Calendar.getInstance();
        final int day = myCalendar.get(Calendar.DAY_OF_MONTH);
        final int month = myCalendar.get(Calendar.MONTH);
        final int year = myCalendar.get(Calendar.YEAR);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                final EditText dateEditText = textInputLayout.getEditText();
                Objects.requireNonNull(dateEditText).setText(getDateStringFromInt(year,month,dayOfMonth, true));
            }
        }, year, month, day);
        datePickerDialog.show();
    }

    public static void showDatePickerDialog(final Context context, final TextView textView,
                                            final VisitedPlaceItem item, final Activity activity,
                                            final String departureDateString, final String returnDateString){
        final Calendar myCalendar = Calendar.getInstance();
        final int day = myCalendar.get(Calendar.DAY_OF_MONTH);
        final int month = myCalendar.get(Calendar.MONTH);
        final int year = myCalendar.get(Calendar.YEAR);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                final Date departureDate = getDateNotItalyLocale(departureDateString);
                final Date returnDate = getDateNotItalyLocale(returnDateString);
                final Date currentDate = getDateNotItalyLocale(getDateStringFromInt(year,month,dayOfMonth, true));
                if((Objects.requireNonNull(currentDate).equals(departureDate) || currentDate.after(departureDate)) &&
                        (currentDate.before(returnDate) || currentDate.equals(returnDate))){
                    textView.setText(getDateStringFromInt(year,month,dayOfMonth,true));
                    item.setDate(textView.getText().toString());
                }else{
                    Utility.showNeutralAlert(activity.getString(R.string.attenzione),activity.getString(R.string.data_non_compresa), activity);
                }
            }
        }, year, month, day);
        datePickerDialog.show();
    }

    public static boolean checkDates(final String departureDateString, final String returnDateString){
        final DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.SHORT, Locale.ITALY);
        try {
            final Date departureDate = dateFormat.parse(departureDateString.replace("-", "/"));
            final Date returnDate = dateFormat.parse(returnDateString.replace("-", "/"));
            if (Objects.requireNonNull(departureDate).compareTo(returnDate) <= 0){
                return true;
            }
        } catch (ParseException e) {
            Log.e("Errore!", e.toString());
        }
        return false;
    }

    public static void showDatePickerDialog(final Context context, final EditText editText,
                                            final Activity activity, final ItineraryItem tripItem,
                                            final boolean isDepartureDate){
        final Calendar myCalendar = Calendar.getInstance();
        final int day = myCalendar.get(Calendar.DAY_OF_MONTH);
        final int month = myCalendar.get(Calendar.MONTH);
        final int year = myCalendar.get(Calendar.YEAR);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                final Date departureDate = getDate(tripItem.getDepartureDate());
                final Date returnDate = getDate(tripItem.getReturnDate());
                final Date currentDate = getDate(getDateStringFromInt(year,month,dayOfMonth, false));
                if(isDepartureDate && (Objects.requireNonNull(currentDate).equals(returnDate) || currentDate.before(returnDate))){
                    editText.setText(getDateStringFromInt(year,month,dayOfMonth, true));
                    tripItem.setDepartureDate(getDateStringFromInt(year,month,dayOfMonth, false));
                }else if(!isDepartureDate && (Objects.requireNonNull(currentDate).equals(departureDate) || currentDate.after(departureDate))){
                    editText.setText(getDateStringFromInt(year,month,dayOfMonth, true));
                    tripItem.setReturnDate(getDateStringFromInt(year,month,dayOfMonth, false));
                }else{
                    Utility.showNeutralAlert(activity.getString(R.string.attenzione),activity.getString(R.string.data_non_valida), activity);
                }
            }
        }, year, month, day);
        datePickerDialog.show();
    }

    public static String getCorrectDateFormatForOutput(final String date){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ITALY);
        try {
            Date myDate = dateFormat.parse(date);
            dateFormat.applyPattern(("dd-MM-yyyy"));
            return dateFormat.format(Objects.requireNonNull(myDate)).replace("-", "/");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static long getMillisTime(final String date){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ITALY);
        try {
            Date myDate = dateFormat.parse(date);
            return Objects.requireNonNull(myDate).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public static long getDaysBetweenTwoDates(final long firstDate, final long lastDate){
        return Math.round((lastDate-firstDate) / 86400000.0);
    }

    public static String addDay(final String string){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ITALY);
        try {
            Date date = dateFormat.parse(string);
            Calendar c = Calendar.getInstance();
            c.setTime(Objects.requireNonNull(date));
            c.add(Calendar.DATE, 1);
            return dateFormat.format(c.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String getDateStringFromInt(final int year, int month, final int day, final boolean isAEditText){
        String date;
        month = month + 1;
        if(isAEditText){
            if(day<10){
                date = "0"+day+"-";
            }else{
                date = day + "-";
            }
            if(month<10){
                date = date + "0" + month + "-" + year;
            }else{
                date = date + month + "-" + year;
            }
        }else{
            date = year + "-";
            if(month<10){
                date = date + "0" + month + "-";
            }else{
                date = date + month + "-";
            }
            if(day<10){
                date = date +"0"+day;
            }else{
                date = date + day;
            }

        }
        return date;
    }

    private static Date getDate(final String dateStr){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ITALY);
        try {
            return dateFormat.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static Date getDateNotItalyLocale(final String dateStr){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ITALY);
        try {
            return dateFormat.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
