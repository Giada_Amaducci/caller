package com.example.call_er.Utilities;

import android.app.Activity;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.call_er.Adapters.ListAdapter.SearchPlaceListAdapter;
import com.example.call_er.Items.ItineraryItem;
import com.example.call_er.Items.PlaceItem;
import com.example.call_er.Items.VisitedPlaceItem;
import com.example.call_er.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class OsmUtilities {

    public final static String OSM_REQUEST_TAG = "OSM_REQUEST";

    public static void getAmenities(final String city, final String place, final List<PlaceItem> list){
        final String url = "https://nominatim.openstreetmap.org/search.php?q="+place+"+in+"+city+"&addressdetails=1&format=json";
        final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest
                (Request.Method.GET, url, null, response -> {
                    try {
                        list.clear();
                        for(int i=0; i<response.length();i++){
                            final JSONObject jsonobject = response.getJSONObject(i);
                            final JSONObject addressObject = (JSONObject)jsonobject.get("address");
                            if(addressObject.has("province") && addressObject.getString("province").equals("Forlì-Cesena")){
                                final PlaceItem placeItem = getPlaceFromOSMString(jsonobject.get("display_name").toString(), city,
                                                                                  addressObject.getString("road"), jsonobject.get("lon").toString(),
                                                                                  jsonobject.get("lat").toString() );
                                list.add(placeItem);
                            }
                        }
                    } catch (final JSONException e) {
                        Log.e("Errore", e.toString());
                    }
                }, error -> Log.e("Errore", error.toString()));
        jsonArrayRequest.setTag(OSM_REQUEST_TAG);
        InternetUtilities.getRequestQueue().add(jsonArrayRequest);
    }

    public static PlaceItem getPlaceFromOSMString(final String string, final String city, final String road, final String lon, final String lat){
        String[] place = string.split(",");
        final String name = place[0];
        final StringBuilder stringBuilder = new StringBuilder();
        for (int i=1; i<place.length; i++) {
            stringBuilder.append(place[i]);
            stringBuilder.append(",");
        }
        stringBuilder.deleteCharAt(0);
        stringBuilder.deleteCharAt(stringBuilder.length()-1);
        final String address = stringBuilder.toString();
        return new PlaceItem(name, address, city, road, lon, lat);
    }

    public static void getAmenity(final String city, final String place, final String road,
                                  final TextView telephoneAttrValueTextView, final TextView websiteAttrValueTextView,
                                  final TextView emailAttrValueTextView){
        final String url = "https://nominatim.openstreetmap.org/search.php?q="+place+"+in+"+road+"+"+city+"&addressdetails=1&extratags=1&format=json";
        final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest
                (Request.Method.GET, url, null, response -> {
                    try {
                        for(int i=0; i<response.length();i++){
                            final JSONObject jsonobject = response.getJSONObject(i);
                            if(jsonobject.has("extratags")){
                                final JSONObject extraTag = (JSONObject) jsonobject.get("extratags");
                                String value;
                                if(extraTag.has("contact:phone") || extraTag.has("phone")){
                                    if(extraTag.has("phone")){
                                        value = extraTag.get("phone").toString();
                                    }else{
                                        value = extraTag.get("contact:phone").toString();
                                    }

                                }else{
                                    value = "Non Trovato";
                                }
                                telephoneAttrValueTextView.setText(value);
                                if(extraTag.has("email")){
                                    value = extraTag.get("email").toString();
                                } else{
                                    value = "Non Trovata";
                                }
                                emailAttrValueTextView.setText(value);
                                if(extraTag.has("website") || extraTag.has("contact:website")){
                                    if(extraTag.has("website")){
                                        value = extraTag.get("website").toString();
                                    }else{
                                        value = extraTag.get("contact:website").toString();
                                    }

                                }else{
                                    value = "Non Trovato";
                                }
                                websiteAttrValueTextView.setText(value);
                            }
                        }
                    } catch (final JSONException e) {
                        Log.e("Errore", e.toString());
                    }
                }, error -> Log.e("Errore", error.toString()));
        jsonArrayRequest.setTag(OSM_REQUEST_TAG);
        InternetUtilities.getRequestQueue().add(jsonArrayRequest);
    }

    public static void searchAmenity(final String place, final List<PlaceItem> list, final Activity activity,
                                     final SearchPlaceListAdapter listAdapter, final String city, final LoadingDialog loadingDialog) {
        if(place.length()!=0){
            final String url = "https://nominatim.openstreetmap.org/search?q="+place+"+in+"+city+"&addressdetails=1&extratags=1&format=json";
            final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest
                    (Request.Method.GET, url, null, response -> {
                        try {
                            for(int i=0; i<response.length();i++){
                                final JSONObject jsonobject = response.getJSONObject(i);
                                final JSONObject addressObject = (JSONObject)jsonobject.get("address");
                                if(jsonobject.get("class").equals("historic") || jsonobject.get("class").equals("tourism")
                                        || (jsonobject.get("class").equals("shop") && jsonobject.get("type").equals("supermarket"))
                                        || (jsonobject.get("class").equals("building") && jsonobject.get("type").equals("church"))
                                        || (jsonobject.get("class").equals("amenity") && (jsonobject.get("type").equals("restaurant") || jsonobject.get("type").equals("cafe")
                                        || jsonobject.get("type").equals("fast_food") || jsonobject.get("type").equals("pub") || jsonobject.get("type").equals("ice_cream")
                                        || jsonobject.get("type").equals("bank") || jsonobject.get("type").equals("post_office") || jsonobject.get("type").equals("hospital")
                                        || jsonobject.get("type").equals("pharmacy") || jsonobject.get("type").equals("fire_station") || jsonobject.get("type").equals("police")
                                        || jsonobject.get("type").equals("cinema") || jsonobject.get("type").equals("library") || jsonobject.get("type").equals("theatre")
                                ))){
                                    final PlaceItem placeItem = getPlaceFromOSMString(jsonobject.get("display_name").toString(), city,
                                            addressObject.getString("road"), jsonobject.get("lon").toString(),
                                            jsonobject.get("lat").toString() );
                                    list.add(placeItem);
                                }
                            }
                            loadingDialog.dismissDialog();
                            if(list.isEmpty()){
                                Utility.showToast("Nessun posto trovato!", activity);
                            }
                            listAdapter.setData(list);
                        } catch (final JSONException e) {
                            Log.e(activity.getString(R.string.error), e.toString());
                            loadingDialog.dismissDialog();
                        }
                    }, error -> Log.e(activity.getString(R.string.error), error.toString()));
            jsonArrayRequest.setTag(OSM_REQUEST_TAG);
            InternetUtilities.getRequestQueue().add(jsonArrayRequest);
        }
    }
}
