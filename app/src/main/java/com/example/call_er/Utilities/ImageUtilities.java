package com.example.call_er.Utilities;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageDecoder;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class ImageUtilities {

    public static final int PICK_IMAGE_REQUEST = 1;
    public static final int REQUEST_IMAGE_CAPTURE = 2;

    //scegli immagine galleria
    public static Intent showFileChooser() {
        final Intent pickImageIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickImageIntent.setType("image/*");
        pickImageIntent.putExtra("aspectX", 1);
        pickImageIntent.putExtra("aspectY", 1);
        pickImageIntent.putExtra("scale", true);
        pickImageIntent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        return pickImageIntent;
    }

    //apri fotocamera per scattare la foto
    public static Intent getPhoto() {
        return new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
    }

    //salvare l'immagine fatta nella galleria
    public static void saveImage(final Bitmap bitmap, final Context context)throws IOException {
        final String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ITALY).format(new Date());
        final String name = "JPEG_" + timeStamp + "_.png";

        final ContentResolver resolver = context.getContentResolver();
        final ContentValues contentValues = new ContentValues();
        contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, name + ".jpg");
        contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpg");
        final Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
        final OutputStream fos = resolver.openOutputStream(Objects.requireNonNull(imageUri));
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
        if (fos != null) {
            fos.close();
        }
    }

    //conversione in base64 dell'immagine
    public static String getStringImage(final Bitmap bmp, final boolean isPicked) {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if(isPicked){
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        }else{
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        }
        final byte[] imageBytes = baos.toByteArray();
        return Base64.encodeToString(imageBytes, Base64.DEFAULT);
    }

    public static Bitmap getBitmap(final Uri filePath, final ContentResolver contentResolver){
        Bitmap bitmap = null;
        try{
            if (Build.VERSION.SDK_INT >= 28){
                final ImageDecoder.Source source = ImageDecoder.createSource(contentResolver, filePath);
                bitmap=ImageDecoder.decodeBitmap(source);
            }else{
                bitmap = MediaStore.Images.Media.getBitmap(contentResolver, filePath);
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    public static Bitmap decodeBitmap(final List<String> strBase64){
        final StringBuilder stringBuilder = new StringBuilder();

        for (int i=UserManagement.IMAGE.getId(); i<strBase64.size(); i++){
            stringBuilder.append(strBase64.get(i)).append("\n");
        }
        final byte[] decodedString = Base64.decode(stringBuilder.toString(), Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
    }

    public static Bitmap decodeBitmap(final String strBase64){
        final byte[] decodedString = Base64.decode(strBase64, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
    }
}
