package com.example.call_er.Utilities;

public enum UserManagement {

    EMAIL(0),
    USERNAME(1),
    PHONE(2),
    ID(3),
    ROLE(4),
    IMAGE(5);

    private int id;

    UserManagement(final int id){
        this.id=id;
    }

    public int getId() {
        return id;
    }
}

