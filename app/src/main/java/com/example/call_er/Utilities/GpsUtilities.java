package com.example.call_er.Utilities;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.provider.Settings;

import androidx.appcompat.app.AlertDialog;

import com.example.call_er.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class GpsUtilities {

    private static LocationRequest locationRequest;
    private static FusedLocationProviderClient fusedLocationProviderClient;

    public static void setLocation(Activity activity){
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(activity);
        locationRequest = new LocationRequest();
        locationRequest.setInterval(1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    public static void requestLocation(final Activity activity, final BroadcastReceiver broadcastReceiver) {
        try {
            statusCheck(activity);
            fusedLocationProviderClient.requestLocationUpdates(locationRequest, getPendingIntent(activity));
            activity.registerReceiver(broadcastReceiver, new IntentFilter("SEND_DATA"));
        } catch (SecurityException e) {
            removeLocationUpdates(activity, broadcastReceiver);
            e.printStackTrace();
        }
    }

    public static void removeLocationUpdates(final Activity activity, final BroadcastReceiver broadcastReceiver) {
        fusedLocationProviderClient.removeLocationUpdates(getPendingIntent(activity));
        activity.unregisterReceiver(broadcastReceiver);
    }

    private static PendingIntent getPendingIntent(final Activity activity) {
        Intent intent = new Intent(activity, LocationBroadcastReceiver.class);
        return PendingIntent
                .getBroadcast(activity, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private static void statusCheck(final Activity activity) {
        final LocationManager manager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        if (manager != null && !manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps(activity);
        }
    }

    private static void buildAlertMessageNoGps(final Activity activity) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage("Il GPS risulta spento, per utilizzare l'app si richiede di attivarlo!")
                .setCancelable(false)
                .setPositiveButton(activity.getString(R.string.si), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        activity.startActivity(
                                new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton(activity.getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }
}
