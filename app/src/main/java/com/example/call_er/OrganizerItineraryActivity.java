package com.example.call_er;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.call_er.Adapters.ListAdapter.PlacesInItineraryListAdapter;
import com.example.call_er.Items.ItineraryItem;
import com.example.call_er.Items.ItineraryPlaceItem;
import com.example.call_er.Items.PlaceItem;
import com.example.call_er.Utilities.Extra;
import com.example.call_er.Utilities.FileUtilities;
import com.example.call_er.Utilities.InternetUtilities;
import com.example.call_er.Utilities.UserManagement;
import com.example.call_er.Utilities.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class OrganizerItineraryActivity extends AppCompatActivity {

    private ItineraryItem itineraryItem;

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            this.onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if(this.getClass().equals(OrganizerItineraryActivity.class)){
            final AlertDialog dialog = new AlertDialog.Builder(this)
                    .setMessage("Sei sicuro/a di non voler modificare il tuo itinerario?")
                    .setCancelable(false)
                    .setPositiveButton(this.getString(R.string.si), (dialog1, which) -> {
                        final Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(i);
                    })
                    .setNegativeButton(this.getString(R.string.no), (dialog12, which) -> dialog12.dismiss())
                    .create();
            dialog.show();
        }else{
            finish();
        }
    }


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_organizer_itinerary);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.itineraryOrganizerInfoConstraintLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI(){
        final String title = this.getIntent().getStringExtra(Extra.NAME.getTypeExtra());
        final String itineraryId = this.getIntent().getStringExtra(Extra.ID.getTypeExtra());
        final String city = this.getIntent().getStringExtra(Extra.CITY.getTypeExtra());
        final String count = this.getIntent().getStringExtra(Extra.USER_COUNT.getTypeExtra());
        String places = this.getIntent().getStringExtra(Extra.PLACES.getTypeExtra());
        String prevActivity = this.getIntent().getStringExtra(Extra.ACTIVITY_NAME.getTypeExtra());

        this.itineraryItem = new ItineraryItem(itineraryId, title, city);
        this.itineraryItem.setUsers(count);

        final ListView listView = this.findViewById(R.id.placesInItineraryListView);
        final PlacesInItineraryListAdapter listAdapter = new PlacesInItineraryListAdapter(this, this, itineraryItem, prevActivity);
        listView.setAdapter(listAdapter);

        if(places != null){
            this.itineraryItem.setPlaces(places);
            listAdapter.setData(this.itineraryItem.getPlaces());
        }else{
            if (InternetUtilities.getIsNetworkConnected()) {
                this.getPlacesInItinerary(this.itineraryItem.getId(), listAdapter);
            } else {
                InternetUtilities.getSnackbar().show();
            }
        }

        final RadioGroup radioGroup = this.findViewById(R.id.searchRadioGroup);
        final TextView countTextView = this.findViewById(R.id.userTextView);

        countTextView.setText(String.format("%s: %s", "Utenti che lo hanno provato", count));

        final int cityId;
        if (city != null) {
            if (city.equals("Cesena")) {
                cityId = 0;
            }else{
                cityId = 1;
            }
        }else{
            cityId = -1;
        }

        radioGroup.check(radioGroup.getChildAt(cityId).getId());

        for (int i = 0; i < radioGroup.getChildCount(); i++) {
            radioGroup.getChildAt(i).setEnabled(false);
        }

        this.findViewById(R.id.addPlaceInItineraryTextView).setOnClickListener(v -> {
            final Intent i = new Intent(getApplicationContext(), AddPlaceInItineraryActivity.class);
            i.putExtra(Extra.NAME.getTypeExtra(), itineraryItem.getName());
            i.putExtra(Extra.ID.getTypeExtra(), itineraryItem.getId());
            i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "OrganizerItineraryActivity");
            i.putExtra(Extra.CITY.getTypeExtra(), itineraryItem.getCity());
            i.putExtra(Extra.PLACES.getTypeExtra(), itineraryItem.getAllPlaces());
            i.putExtra(Extra.USER_COUNT.getTypeExtra(), itineraryItem.getUsers());
            startActivity(i);
        });

        this.findViewById(R.id.fabUpdateItinerary).setOnClickListener(v -> {
                if (InternetUtilities.getIsNetworkConnected()) {
                    updateOrganizerItinerary(itineraryItem);
                } else {
                    InternetUtilities.getSnackbar().show();
                }
        });

        Utility.setUpToolbar(this, title);
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    private void updateOrganizerItinerary(final ItineraryItem itineraryItem){
        final String url = "https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("result").equals("success")) {
                            final Intent i = new Intent(getApplicationContext(), ItinerariesActivity.class);
                            i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "OrganizerItineraryActivity");
                            startActivity(i);
                        }else{
                            Utility.showNeutralAlert(getString(R.string.attenzione), jsonObject.getString("message"), OrganizerItineraryActivity.this);
                        }
                    } catch (final JSONException e) {
                        Log.e(getString(R.string.error), e.toString());
                    }
                },
                error -> Log.e(getString(R.string.error), error.toString())){
            @Override
            protected Map<String,String> getParams(){
                final Map<String,String> params = new HashMap<>();
                params.put("type", "updateOrganizerItinerary");
                params.put("itinerary_id", itineraryItem.getId());
                params.put("user_id", FileUtilities.getData( getCacheDir()+"/mtj.txt").get(UserManagement.ID.getId()));
                params.put("places", itineraryItem.getAllPlaces());
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

    private void getPlacesInItinerary(final String itinerary_id, final PlacesInItineraryListAdapter adapter){
        final String url = "https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("result").equals("success")) {
                            final List<ItineraryPlaceItem> placesList = new ArrayList<>();
                            if(!jsonObject.getString("message").equals("nessun luogo")) {
                                final JSONArray jsonArray = new JSONArray(jsonObject.getString("values"));
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    final JSONObject placeInItinerary = (JSONObject) jsonArray.get(i);
                                    final String id = placeInItinerary.getString("place_in_itinerary_id");
                                    final String name = placeInItinerary.getString("place_name");
                                    final String address = placeInItinerary.getString("address");
                                    final String city = placeInItinerary.getString("city");
                                    final String road = placeInItinerary.getString("road");
                                    final String lat = placeInItinerary.getString("lat");
                                    final String lon = placeInItinerary.getString("lon");
                                    final PlaceItem placeItem = new PlaceItem(name, address, city, road, lon, lat);
                                    final ItineraryPlaceItem visitedPlaceItem = new ItineraryPlaceItem(id, placeItem);
                                    placesList.add(visitedPlaceItem);
                                }
                            }
                            itineraryItem.setPlaces(placesList);
                            adapter.setData(placesList);
                        }else{
                            Utility.showNeutralAlert(getString(R.string.attenzione),jsonObject.getString("message"), OrganizerItineraryActivity.this);
                        }
                    } catch (final JSONException e) {
                        Log.e(getString(R.string.error), e.toString());
                    }
                },
                error -> Log.e(getString(R.string.error), error.toString())){
            @Override
            protected Map<String,String> getParams(){
                final Map<String, String> params = new HashMap<>();
                params.put("type", "getPlacesInItinerary");
                params.put("itinerary_id", itinerary_id);
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }
}
