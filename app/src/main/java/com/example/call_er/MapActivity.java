package com.example.call_er;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.call_er.Adapters.RecyclerView.LocationRecyclerViewAdapter;
import com.example.call_er.Items.PlaceItem;
import com.example.call_er.Items.SingleRecyclerViewLocation;
import com.example.call_er.Utilities.GpsUtilities;
import com.example.call_er.Utilities.InternetUtilities;
import com.example.call_er.Utilities.LoadingDialog;
import com.example.call_er.Utilities.PermissionsUtilities;
import com.example.call_er.Utilities.Utility;
import com.mapbox.api.directions.v5.DirectionsCriteria;
import com.mapbox.api.directions.v5.MapboxDirections;
import com.mapbox.api.directions.v5.models.DirectionsResponse;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.LineString;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.style.layers.LineLayer;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.mapboxsdk.utils.BitmapUtils;

import static com.mapbox.mapboxsdk.style.layers.Property.LINE_JOIN_ROUND;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAllowOverlap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconIgnorePlacement;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconImage;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconSize;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineJoin;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineWidth;
import static com.mapbox.core.constants.Constants.PRECISION_6;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MapActivity extends AppCompatActivity {

    private static final String TAG = "MAP_ACTIVITY";
    private static final String SYMBOL_ICON_ID = "SYMBOL_ICON_ID";
    private static final String PERSON_ICON_ID = "PERSON_ICON_ID";
    private static final String MARKER_SOURCE_ID = "MARKER_SOURCE_ID";
    private static final String PERSON_SOURCE_ID = "PERSON_SOURCE_ID";
    private static final String DASHED_DIRECTIONS_LINE_LAYER_SOURCE_ID = "DASHED_DIRECTIONS_LINE_LAYER_SOURCE_ID";
    private static final String LAYER_ID = "LAYER_ID";
    private static final String PERSON_LAYER_ID = "PERSON_LAYER_ID";
    private static final String DASHED_DIRECTIONS_LINE_LAYER_ID = "DASHED_DIRECTIONS_LINE_LAYER_ID";

    public final List<DirectionsRoute> directionsRouteList = new ArrayList<>();
    private MapboxMap mapboxMap;
    private Bundle savedInstanceState;
    private MapView mapView;
    private FeatureCollection dashedLineDirectionsFeatureCollection;
    private List<PlaceItem> placeItems;
    private Point directionsOriginPoint;
    private LoadingDialog loadingDialog;
    private List<LatLng> possibleDestinations;
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            if (intent.getAction() != null && intent.getAction().equals("SEND_DATA")) {
                final String lat = intent.getStringExtra("location_latitude");
                final String lon = intent.getStringExtra("location_longitude");

                if (!InternetUtilities.getIsNetworkConnected()) {
                    InternetUtilities.getSnackbar().show();
                } else{
                    getNearbyPlaces(lat, lon);
                }
                GpsUtilities.removeLocationUpdates(MapActivity.this, broadcastReceiver);
            }
        }
    };

    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == PermissionsUtilities.REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length > 0) {
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    PermissionsUtilities.createSnackBar(this);
                }
            }
        }
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(this, getString(R.string.mapbox_access_token));
        this.setContentView(R.layout.activity_map);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.savedInstanceState = savedInstanceState;
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.mapLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
        this.mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        this.mapView.onStop();
    }

    private void initUI() {
        this.loadingDialog = new LoadingDialog(MapActivity.this);
        this.loadingDialog.startLoadingDialog();
        this.placeItems = new ArrayList<>();
        this.possibleDestinations = new ArrayList<>();

        if (!PermissionsUtilities.checkPermissions(this)) {
            PermissionsUtilities.requestPermissions(this);
        }

        if (InternetUtilities.getIsNetworkConnected()) {
            GpsUtilities.setLocation(this);
            GpsUtilities.requestLocation(this, broadcastReceiver);
        } else {
            InternetUtilities.getSnackbar().show();
        }

        Utility.setUpToolbar(this, "La Tua Mappa");
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    private void getNearbyPlaces(final String latitude, final String longitude){
        final String url ="https://api.tomtom.com/search/2/nearbySearch/.JSON?key=fwsfvw4XHMYjnIlrieyfyzi90So5klE7&lat="+latitude+"&lon="+longitude;
        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, response -> {
                    try {
                        final JSONArray placesArray = response.getJSONArray("results");
                        for(int i=0; i<placesArray.length();i++){
                            final JSONObject place =  placesArray.getJSONObject(i);
                            final String name = place.getJSONObject("poi").getString("name");
                            final String address = place.getJSONObject("address").getString("freeformAddress");
                            final String road = place.getJSONObject("address").getString("streetName");
                            final String city = place.getJSONObject("address").getString("localName");
                            final String lat = place.getJSONObject("position").getString("lat");
                            final String lon = place.getJSONObject("position").getString("lon");

                            final PlaceItem placeitem = new PlaceItem(name,address,city,road,lon,lat);
                            Log.d("PlaceItem", placeitem.getName());
                            this.placeItems.add(placeitem);
                        }
                        this.loadingDialog.dismissDialog();
                        setMap(latitude, longitude);
                    } catch (final JSONException e) {
                        Log.e(getString(R.string.error), e.toString());
                    }
                }, error -> Log.e(getString(R.string.error), error.toString()));
        InternetUtilities.getRequestQueue().add(jsonObjectRequest);
    }

    private void setMap(final String lat, final String lon){
        mapView = findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(mapboxMap -> {

            this.directionsOriginPoint = Point.fromLngLat(Double.parseDouble(lon), Double.parseDouble(lat));

            for(PlaceItem place : this.placeItems){
                final double place_lat = Double.parseDouble(place.getLat());
                final double place_lon = Double.parseDouble(place.getLon());
                this.possibleDestinations.add(new LatLng(place_lat, place_lon));
            }

            final Drawable drawablePerson = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_person_24dp, null);
            final Bitmap personBitmap = BitmapUtils.getBitmapFromDrawable(drawablePerson);
            final Drawable drawablePlace = ResourcesCompat.getDrawable(getResources(), R.drawable.baseline_location_on_24, null);
            final Bitmap placeBitmap = BitmapUtils.getBitmapFromDrawable(drawablePlace);

            this.mapboxMap = mapboxMap;
            final CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(Double.parseDouble(lat), Double.parseDouble(lon)))
                    .zoom(15)
                    .build();

            this.mapboxMap.setCameraPosition(cameraPosition);

            mapboxMap.setStyle(new Style.Builder().fromUri(Style.OUTDOORS)
                    .withImage(PERSON_ICON_ID, personBitmap)
                    .withSource(new GeoJsonSource(PERSON_SOURCE_ID,
                            Feature.fromGeometry(directionsOriginPoint)))
                    .withLayer(new SymbolLayer(PERSON_LAYER_ID, PERSON_SOURCE_ID).withProperties(
                            iconImage(PERSON_ICON_ID),
                            iconSize(2f),
                            iconAllowOverlap(true),
                            iconIgnorePlacement(true)
                    ))

                    .withImage(SYMBOL_ICON_ID, placeBitmap)
                    .withSource(new GeoJsonSource(MARKER_SOURCE_ID,
                            initDestinationFeatureCollection()))
                    .withLayer(new SymbolLayer(LAYER_ID, MARKER_SOURCE_ID).withProperties(
                            iconImage(SYMBOL_ICON_ID),
                            iconAllowOverlap(true),
                            iconIgnorePlacement(true),
                            iconSize(2f)
                    ))

                    .withSource(new GeoJsonSource(DASHED_DIRECTIONS_LINE_LAYER_SOURCE_ID))
                    .withLayerBelow(
                            new LineLayer(DASHED_DIRECTIONS_LINE_LAYER_ID, DASHED_DIRECTIONS_LINE_LAYER_SOURCE_ID)
                                    .withProperties(
                                            lineWidth(7f),
                                            lineJoin(LINE_JOIN_ROUND),
                                            lineColor(Color.parseColor("#00675B"))
                                    ), PERSON_LAYER_ID), style -> {
                                        getRoutesToAllPoints();
                                        initRecyclerView();
                                    });
        });

    }

    private void getRoutesToAllPoints() {
        for (LatLng singleLatLng : this.possibleDestinations) {
            getRoute(Point.fromLngLat(singleLatLng.getLongitude(), singleLatLng.getLatitude()));
        }
    }

    private void getRoute(Point destination) {
        MapboxDirections client = MapboxDirections.builder()
                .origin(directionsOriginPoint)
                .destination(destination)
                .overview(DirectionsCriteria.OVERVIEW_FULL)
                .profile(DirectionsCriteria.PROFILE_DRIVING)
                .accessToken(getString(R.string.mapbox_access_token))
                .build();
        client.enqueueCall(new Callback<DirectionsResponse>() {
            @Override
            public void onResponse(@NonNull Call<DirectionsResponse> call, @NonNull Response<DirectionsResponse> response) {
                if (response.body() == null) {
                    Log.d(TAG, "No routes found, make sure you set the right user and access token.");
                    return;
                } else if (response.body().routes().size() < 1) {
                    Log.d(TAG, "No routes found");
                    return;
                }
                directionsRouteList.add(response.body().routes().get(0));
            }

            @Override
            public void onFailure(@NonNull Call<DirectionsResponse> call, @NonNull Throwable throwable) {
                Log.d(TAG, "Error: " + throwable.getMessage());
            }
        });
    }

    public void drawNavigationPolylineRoute(final DirectionsRoute route) {
        if (mapboxMap != null) {
            mapboxMap.getStyle(style -> {
                List<Feature> directionsRouteFeatureList = new ArrayList<>();
                LineString lineString = LineString.fromPolyline(Objects.requireNonNull(route.geometry()), PRECISION_6);
                List<Point> lineStringCoordinates = lineString.coordinates();
                for (int i = 0; i < lineStringCoordinates.size(); i++) {
                    directionsRouteFeatureList.add(Feature.fromGeometry(
                            LineString.fromLngLats(lineStringCoordinates)));
                }
                dashedLineDirectionsFeatureCollection =
                        FeatureCollection.fromFeatures(directionsRouteFeatureList);
                GeoJsonSource source = style.getSourceAs(DASHED_DIRECTIONS_LINE_LAYER_SOURCE_ID);
                if (source != null) {
                    source.setGeoJson(dashedLineDirectionsFeatureCollection);
                }
            });
        }
    }

    private FeatureCollection initDestinationFeatureCollection() {
        List<Feature> featureList = new ArrayList<>();
        for (LatLng latLng : this.possibleDestinations) {
            featureList.add(Feature.fromGeometry(
                    Point.fromLngLat(latLng.getLongitude(), latLng.getLatitude())));
        }
        return FeatureCollection.fromFeatures(featureList);
    }

    private void initRecyclerView() {
        final RecyclerView recyclerView = findViewById(R.id.rv_on_top_of_map);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(),
                LinearLayoutManager.HORIZONTAL, true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(new LocationRecyclerViewAdapter(this,
                createRecyclerViewLocations()));
        new LinearSnapHelper().attachToRecyclerView(recyclerView);
    }

    private List<SingleRecyclerViewLocation> createRecyclerViewLocations() {
        ArrayList<SingleRecyclerViewLocation> locationList = new ArrayList<>();
        for (int x = 0; x < possibleDestinations.size(); x++) {
            SingleRecyclerViewLocation singleLocation = new SingleRecyclerViewLocation();
            singleLocation.setName(this.placeItems.get(x).getName());
            singleLocation.setAddress(this.placeItems.get(x).getAddress());
            locationList.add(singleLocation);
        }
        return locationList;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }
}
