package com.example.call_er;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Filterable;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.call_er.Adapters.ListAdapter.ChatsListAdapter;
import com.example.call_er.Items.ChatItem;
import com.example.call_er.Utilities.Extra;
import com.example.call_er.Utilities.FileUtilities;
import com.example.call_er.Utilities.InternetUtilities;
import com.example.call_er.Utilities.LoadingDialog;
import com.example.call_er.Utilities.OnItemListener;
import com.example.call_er.Utilities.UserManagement;
import com.example.call_er.Utilities.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ChatsActivity extends AppCompatActivity{

    private List<ChatItem> chatItemList;
    private ChatsListAdapter chatsListAdapter;
    private LoadingDialog loadingDialog;

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.nav_onlysearch, menu);
        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(final String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(final String newText) {
                chatsListAdapter.getFilter().filter(newText);
                return true;
            }
        });
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_chats);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }


    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.chatsLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    private void initUI(){
        final ListView listView = findViewById(R.id.chatsListView);
        this.chatsListAdapter = new ChatsListAdapter(this.getApplicationContext(), this);
        listView.setAdapter(this.chatsListAdapter);
        this.chatItemList = new ArrayList<>();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                final String name = chatItemList.get(position).getName();
                final Intent i = new Intent(getApplicationContext(), MessageListActivity.class);
                i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "ChatsActivity");
                i.putExtra(Extra.NAME.getTypeExtra(), name);
                i.putExtra(Extra.ID.getTypeExtra(), chatItemList.get(position).getId());
                startActivity(i);
            }
        });

        this.loadingDialog = new LoadingDialog(this);
        loadingDialog.startLoadingDialog();

        this.findViewById(R.id.fabAddChatsActivity).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent i = new Intent(getApplicationContext(), SearchUserActivity.class);
                startActivity(i);
            }
        });

        final String user_id = FileUtilities.getData( this.getCacheDir()+"/mtj.txt").get(UserManagement.ID.getId());

        if (InternetUtilities.getIsNetworkConnected()) {
            getChats(user_id);
        } else {
            InternetUtilities.getSnackbar().show();
        }

        Utility.setUpToolbar(this, "Le Tue Chat");
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    private void getChats(final String user_id){
        final Map<String, String> params = new HashMap<>();
        params.put("type", "getChats");
        params.put("user_id", user_id);
        this.getChats(params);
    }

    private void getChats(final Map<String, String> params){
        final String url = "https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("result").equals("success")) {
                            if(!jsonObject.getString("message").equals("Nessun messaggio")){
                                final JSONArray jsonArray = new JSONArray(jsonObject.getString("values"));
                                for (int i = 0; i <jsonArray.length(); i++){
                                    final JSONObject chatItem = (JSONObject)jsonArray.get(i);
                                    final String userId = chatItem.getString("user_id");
                                    final String name = chatItem.getString("name");
                                    final String email = chatItem.getString("email");
                                    final String lastMessage =  chatItem.getString("lastMessage");
                                    final String image = chatItem.getString("image");
                                    final int unreadMessages = chatItem.getInt("messagesToRead");
                                    final ChatItem chat = new ChatItem(name, image, lastMessage, userId, email, unreadMessages);
                                    chatItemList.add(chat);
                                }
                            }
                            chatsListAdapter.setData(chatItemList);
                        }else{
                            Utility.showNeutralAlert(getString(R.string.attenzione),jsonObject.getString("message"), ChatsActivity.this);
                        }
                        this.loadingDialog.dismissDialog();
                    } catch (final JSONException e) {
                        Log.e(getString(R.string.error), e.toString());
                    }
                },
                error -> Log.e(getString(R.string.error), error.toString())){
            @Override
            protected Map<String,String> getParams(){
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }
}
