package com.example.call_er;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageButton;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.call_er.Adapters.ImageSliderAdapter.VisitedPlaceImageSliderAdapter;
import com.example.call_er.Items.ImageItem;
import com.example.call_er.Items.VisitedPlaceItem;
import com.example.call_er.Utilities.DateAndCalendarUtilities;
import com.example.call_er.Utilities.Extra;
import com.example.call_er.Utilities.ImageUtilities;
import com.example.call_er.Utilities.InternetUtilities;
import com.example.call_er.Utilities.LoadingDialog;
import com.example.call_er.Utilities.Utility;
import com.smarteist.autoimageslider.SliderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class VisitedPlaceActivity extends AppCompatActivity {

    private VisitedPlaceItem placeItem;
    private List<ImageItem> images;
    private List<AppCompatImageButton> imageButtons;
    private VisitedPlaceImageSliderAdapter adapter;
    private int count;

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            this.onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if(this.getClass().equals(VisitedPlaceActivity.class)){
            final AlertDialog dialog = new AlertDialog.Builder(this)
                    .setMessage("Sei sicuro/a di non voler modificare il posto visitato?")
                    .setCancelable(false)
                    .setPositiveButton(this.getString(R.string.si), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            final Intent i = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(i);
                        }
                    })
                    .setNegativeButton(this.getString(R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .create();
            dialog.show();
        }else{
            finish();
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String image;
        if (requestCode == ImageUtilities.PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            final Uri filePath = data.getData();
            final Bitmap bitmap = ImageUtilities.getBitmap(filePath,getContentResolver());
            image = ImageUtilities.getStringImage(bitmap, true);
            this.addVisitedPlaceImage(image);
        } else if(requestCode == ImageUtilities.REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK && data != null){
            final Bitmap bitmap = (Bitmap) Objects.requireNonNull(data.getExtras()).get("data");
            try {
                assert bitmap != null;
                ImageUtilities.saveImage(bitmap,this);
                image = ImageUtilities.getStringImage(bitmap, false);
                this.addVisitedPlaceImage(image);
            } catch (IOException e) {
                Log.e(getString(R.string.error), e.toString());
            }
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_visited_place);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.visitedPlaceConstraintLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI(){
        final String name = this.getIntent().getStringExtra(Extra.NAME.getTypeExtra());
        final String address = this.getIntent().getStringExtra(Extra.ADDRESS.getTypeExtra());
        final String road = this.getIntent().getStringExtra(Extra.ROAD.getTypeExtra());
        final String city = this.getIntent().getStringExtra(Extra.CITY.getTypeExtra());
        final String id = this.getIntent().getStringExtra(Extra.ID.getTypeExtra());
        final String lat = this.getIntent().getStringExtra(Extra.LAT.getTypeExtra());
        final String lon = this.getIntent().getStringExtra(Extra.LON.getTypeExtra());
        final String date = this.getIntent().getStringExtra(Extra.DATE.getTypeExtra());

        this.placeItem = new VisitedPlaceItem(id, name,address,city,road,false,lat,lon, date);

        final TextView visitedDateFixTextView = this.findViewById(R.id.visitedDateFixTextView);
        visitedDateFixTextView.setText(DateAndCalendarUtilities.getCorrectDateFormatForOutput(this.placeItem.getDate()));

        this.images = new ArrayList<>();

        final SliderView sliderView = findViewById(R.id.imageSlider);
        this.adapter = new VisitedPlaceImageSliderAdapter(this, this);
        sliderView.setSliderAdapter(adapter);

        this.findViewById(R.id.visitedDateImageButton).setOnClickListener(v -> DateAndCalendarUtilities.showDatePickerDialog(VisitedPlaceActivity.this, visitedDateFixTextView, placeItem, VisitedPlaceActivity.this));

        final EditText commentVisitedPlaceEditText = this.findViewById(R.id.commentVisitedPlaceEditText);

        final LoadingDialog loadingDialog = new LoadingDialog(this);
        loadingDialog.startLoadingDialog();

        if (InternetUtilities.getIsNetworkConnected()) {
            this.getImages(this.placeItem, adapter, loadingDialog);
            this.getVisitedPlaceReview(commentVisitedPlaceEditText);
        } else {
            InternetUtilities.getSnackbar().show();
        }

        this.findViewById(R.id.addUpdatePhotoImageButton).setOnClickListener(v -> startActivityForResult(ImageUtilities.showFileChooser(), ImageUtilities.PICK_IMAGE_REQUEST));

        this.findViewById(R.id.makeUpdatePhotoImageButton).setOnClickListener(v -> startActivityForResult(ImageUtilities.getPhoto(), ImageUtilities.REQUEST_IMAGE_CAPTURE));

        final AppCompatImageButton oneStarButton = this.findViewById(R.id.starOneImageButton);
        final AppCompatImageButton twoStarButton = this.findViewById(R.id.starTwoImageButton);
        final AppCompatImageButton threeStarButton = this.findViewById(R.id.starThreeImageButton);
        final AppCompatImageButton fourStarButton = this.findViewById(R.id.starFourImageButton);
        final AppCompatImageButton fiveStarButton = this.findViewById(R.id.starFiveImageButton);
        this.imageButtons = new ArrayList<>();
        this.images = new ArrayList<>();

        this.imageButtons.add(0, oneStarButton);
        this.imageButtons.add(1, twoStarButton);
        this.imageButtons.add(2, threeStarButton);
        this.imageButtons.add(3, fourStarButton);
        this.imageButtons.add(4, fiveStarButton);

        for(int i=0; i< this.imageButtons.size(); i++){
            final int index = i;
            this.imageButtons.get(i).setOnClickListener(view -> setClicked(index));
        }

        this.findViewById(R.id.fab_add_visitedplace).setOnClickListener(v -> updateVisitedPlace(count, commentVisitedPlaceEditText));

        this.findViewById(R.id.placeInfoButton).setOnClickListener(v -> {
            final Intent i = new Intent(getApplicationContext(), PlaceActivity.class);
            i.putExtra(Extra.NAME.getTypeExtra(), placeItem.getName());
            i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "VisitedPlaceActivity");
            i.putExtra(Extra.ADDRESS.getTypeExtra(), placeItem.getAddress());
            i.putExtra(Extra.ROAD.getTypeExtra(), placeItem.getRoad());
            i.putExtra(Extra.CITY.getTypeExtra(), placeItem.getCity());
            i.putExtra(Extra.LAT.getTypeExtra(), placeItem.getLat());
            i.putExtra(Extra.LON.getTypeExtra(), placeItem.getLon());
            startActivity(i);
        });

        Utility.setUpToolbar(this, name);
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    private void setClicked(final int position){
        if(position == -1){
            for(int i=0; i< this.imageButtons.size(); i++){
                this.imageButtons.get(i).setImageResource(R.drawable.baseline_star_border_24);
            }
            this.count = 0;
        }else{
            for(int i=0; i< this.imageButtons.size(); i++){
                if(i<=position){
                    this.imageButtons.get(i).setImageResource(R.drawable.baseline_star_24);
                }else{
                    this.imageButtons.get(i).setImageResource(R.drawable.baseline_star_border_24);
                }
            }
            this.count = position+1;
        }
    }

    private void getImages(final VisitedPlaceItem placeItem, final VisitedPlaceImageSliderAdapter adapter, final LoadingDialog loadingDialog){
        final Map<String, String> params = new HashMap<>();
        params.put("type", "getVisitedPlaceImages");
        params.put("visited_place_id", placeItem.getId());
        this.getPlaceImages(params, adapter, loadingDialog);
    }

    private void getPlaceImages(final Map<String, String> params, final VisitedPlaceImageSliderAdapter adapter, final LoadingDialog loadingDialog){
        final String url = "https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("result").equals("success")) {
                            if(!jsonObject.getString("message").equals("nessuna foto")) {
                                final JSONArray jsonArray = new JSONArray(jsonObject.getString("values"));
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    final JSONObject image = (JSONObject) jsonArray.get(i);
                                    final String imageString = image.getString("image");
                                    final String id = image.getString("id");
                                    final ImageItem imageItem = new ImageItem(id, imageString);
                                    images.add(imageItem);
                                }
                            }
                            adapter.renewItems(images);
                        }else{
                            Utility.showNeutralAlert(getString(R.string.attenzione),jsonObject.getString("message"), VisitedPlaceActivity.this);
                        }
                        loadingDialog.dismissDialog();
                    } catch (final JSONException e) {
                        Log.e(getString(R.string.error), e.toString());
                    }
                },
                error -> Log.e(getString(R.string.error), error.toString())){
            @Override
            protected Map<String,String> getParams(){
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

    private void getVisitedPlaceReview(final EditText comments){
        final String url = "https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("result").equals("success")) {
                            int preference;
                            if(!jsonObject.getString("message").equals("nessuna recensione")) {
                                final JSONObject review = (JSONObject)jsonObject.get("place");
                                final String note = review.getString("message");
                                comments.setText(note);
                                preference = review.getInt("vote");
                                setClicked(preference-1);
                            }else{
                                preference = -1;
                                setClicked(preference);
                            }
                        }else{
                            Utility.showNeutralAlert(getString(R.string.attenzione),jsonObject.getString("message"), VisitedPlaceActivity.this);
                        }
                    } catch (final JSONException e) {
                        Log.e(getString(R.string.error), e.toString());
                    }
                },
                error -> Log.e(getString(R.string.error), error.toString())){
            @Override
            protected Map<String,String> getParams(){
                final Map<String, String> params = new HashMap<>();
                params.put("type", "getVisitedPlaceReview");
                params.put("visited_place_id", placeItem.getId());
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

    private void addVisitedPlaceImage(final String image){
        final String url = "https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("result").equals("success")) {
                            final String image_id = jsonObject.getString("image_id");
                            final String image1 = jsonObject.getString("image");
                            final ImageItem imageItem = new ImageItem(image_id, image1);
                            images.add(imageItem);
                            adapter.renewItems(images);
                        } else {
                            Utility.showNeutralAlert(getString(R.string.attenzione), jsonObject.getString("message"), VisitedPlaceActivity.this);
                        }
                    } catch (final JSONException e) {
                        Log.e(getString(R.string.error), e.toString());
                    }
                },
                error -> Log.e(getString(R.string.error), error.toString())) {
            @Override
            protected Map<String, String> getParams() {
                final Map<String, String> params = new HashMap<>();
                params.put("type", "addVisitedPlaceImage");
                params.put("visited_place_id", placeItem.getId());
                params.put("image", image);
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

    private void updateVisitedPlace(final int vote, final EditText comment){
        final String url = "https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("result").equals("success")) {
                            Utility.showToast("Luogo visitato aggiornato con successo!", VisitedPlaceActivity.this);
                        } else {
                            Utility.showNeutralAlert(getString(R.string.attenzione), jsonObject.getString("message"), VisitedPlaceActivity.this);
                        }
                    } catch (final JSONException e) {
                        Log.e(getString(R.string.error), e.toString());
                    }
                },
                error -> Log.e(getString(R.string.error), error.toString())) {
            @Override
            protected Map<String, String> getParams() {
                final Map<String, String> params = new HashMap<>();
                params.put("type", "updateVisitedPlace");
                params.put("visited_place_id", placeItem.getId());
                if(!comment.getText().toString().isEmpty()){
                    params.put("note", comment.getText().toString());
                }
                params.put("vote", String.valueOf(vote));
                params.put("date", placeItem.getDate());
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }
}
