package com.example.call_er;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.call_er.Adapters.ListAdapter.PlacesInItineraryListAdapter;
import com.example.call_er.Items.ItineraryItem;
import com.example.call_er.Items.ItineraryPlaceItem;
import com.example.call_er.Items.PlaceItem;
import com.example.call_er.Items.VisitedPlaceItem;
import com.example.call_er.Utilities.DateAndCalendarUtilities;
import com.example.call_er.Utilities.Extra;
import com.example.call_er.Utilities.FileUtilities;
import com.example.call_er.Utilities.InternetUtilities;
import com.example.call_er.Utilities.UserManagement;
import com.example.call_er.Utilities.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class PlacesInItineraryActivity extends AppCompatActivity {

    private String prevActivity;
    private ItineraryItem itineraryItem;

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            this.onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if(this.getClass().equals(PlacesInItineraryActivity.class)){
            final Intent i;
            if(this.prevActivity.equals("ItineraryActivity")){
                i = new Intent(getApplicationContext(), VisitedItineraryActivity.class);
            }else if(this.prevActivity.equals("PublicItineraryActivity")){
                i = new Intent(getApplicationContext(), PublicItineraryActivity.class);
            }else{
                i = new Intent(getApplicationContext(), AddItineraryUserActivity.class);
            }

            i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "PlacesInItinerary");
            i.putExtra(Extra.NAME.getTypeExtra(), this.itineraryItem.getName());
            i.putExtra(Extra.CITY.getTypeExtra(), this.itineraryItem.getCity());
            i.putExtra(Extra.ID.getTypeExtra(), itineraryItem.getId());
            i.putExtra(Extra.CREATOR_NAME.getTypeExtra(), itineraryItem.getCreatorName());

            if(this.prevActivity.equals("PublicItineraryActivity")){
                i.putExtra(Extra.AVG_VOTE.getTypeExtra(), itineraryItem.getAvg());
                i.putExtra(Extra.USER_COUNT.getTypeExtra(), itineraryItem.getVotes());
            }else{
                i.putExtra(Extra.DEPARTURE_DATE.getTypeExtra(), this.itineraryItem.getDepartureDate());
                i.putExtra(Extra.RETURN_DATE.getTypeExtra(), this.itineraryItem.getReturnDate());
                i.putExtra(Extra.PLACES.getTypeExtra(), this.itineraryItem.getAllPlaces());
            }

            this.startActivity(i);
        }else{
            this.finish();
        }
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_places_in_itinerary);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.placesInItineraryConstraintLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI(){
        this.prevActivity = this.getIntent().getStringExtra(Extra.ACTIVITY_NAME.getTypeExtra());
        final String title = this.getIntent().getStringExtra(Extra.NAME.getTypeExtra());
        final String city = this.getIntent().getStringExtra(Extra.CITY.getTypeExtra());
        final String id;

        if(this.prevActivity.equals("ItineraryActivity") || this.prevActivity.equals("PublicItineraryActivity")){
            id = this.getIntent().getStringExtra(Extra.ID.getTypeExtra());
        }else{
            id = "nessuno";
        }
        this.itineraryItem  = new ItineraryItem(id, title, city);

        if(this.prevActivity.equals("PublicItineraryActivity")){
            final String avg = this.getIntent().getStringExtra(Extra.AVG_VOTE.getTypeExtra());
            final String count = this.getIntent().getStringExtra(Extra.USER_COUNT.getTypeExtra());
            this.itineraryItem.setAvg(avg);
            this.itineraryItem.setVotes(count);
        }else{
            final String departureDate = this.getIntent().getStringExtra(Extra.DEPARTURE_DATE.getTypeExtra());
            final String returnDate = this.getIntent().getStringExtra(Extra.RETURN_DATE.getTypeExtra());
            this.itineraryItem.setDepartureDate(departureDate);
            this.itineraryItem.setReturnDate(returnDate);
        }

        final String creatorName = this.getIntent().getStringExtra(Extra.CREATOR_NAME.getTypeExtra());

        if(creatorName != null){
            this.itineraryItem.setCreatorName(creatorName);
        }

        final ListView listView = this.findViewById(R.id.placesListView);
        final PlacesInItineraryListAdapter listAdapter = new PlacesInItineraryListAdapter(this, this, itineraryItem, prevActivity);
        listView.setAdapter(listAdapter);
        final String places = this.getIntent().getStringExtra(Extra.PLACES.getTypeExtra());

        if(places != null && !places.equals("[]")){
            this.itineraryItem.setPlaces(places);
            if(!this.itineraryItem.getPlaces().isEmpty()){
                listAdapter.setData(this.itineraryItem.getPlaces());
            }
        }else{
            if (InternetUtilities.getIsNetworkConnected()) {
                getPlacesInItinerary(this.itineraryItem.getId(), listAdapter);
            } else {
                InternetUtilities.getSnackbar().show();
            }
        }

        this.findViewById(R.id.addPlaceInItineraryTextView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent i = new Intent(getApplicationContext(), AddPlaceInItineraryActivity.class);
                i.putExtra(Extra.NAME.getTypeExtra(), itineraryItem.getName());
                i.putExtra(Extra.DEPARTURE_DATE.getTypeExtra(), itineraryItem.getDepartureDate());
                i.putExtra(Extra.RETURN_DATE.getTypeExtra(), itineraryItem.getReturnDate());
                i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), prevActivity);
                i.putExtra(Extra.CREATOR_NAME.getTypeExtra(), itineraryItem.getCreatorName());
                i.putExtra(Extra.CITY.getTypeExtra(), itineraryItem.getCity());
                i.putExtra(Extra.ID.getTypeExtra(), itineraryItem.getId());
                i.putExtra(Extra.PLACES.getTypeExtra(), itineraryItem.getAllPlaces());
                startActivity(i);
            }
        });

        final String username = FileUtilities.getData( getCacheDir()+"/mtj.txt").get(UserManagement.USERNAME.getId());

        if(this.prevActivity.equals("PublicItineraryActivity") || (creatorName!=null && !username.equals(creatorName))){
            this.findViewById(R.id.addPlaceInItineraryTextView).setVisibility(View.GONE);
        }

        Utility.setUpToolbar(this, title);
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    private void getPlacesInItinerary(final String itinerary_id, final PlacesInItineraryListAdapter adapter){
        final String url = "https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("result").equals("success")) {
                            final List<ItineraryPlaceItem> placesList = new ArrayList<>();
                            if(!jsonObject.getString("message").equals("nessun luogo")) {
                                final JSONArray jsonArray = new JSONArray(jsonObject.getString("values"));
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    final JSONObject placeInItinerary = (JSONObject) jsonArray.get(i);
                                    final String id = placeInItinerary.getString("place_in_itinerary_id");
                                    final String name = placeInItinerary.getString("place_name");
                                    final String address = placeInItinerary.getString("address");
                                    final String city = placeInItinerary.getString("city");
                                    final String road = placeInItinerary.getString("road");
                                    final String lat = placeInItinerary.getString("lat");
                                    final String lon = placeInItinerary.getString("lon");
                                    final PlaceItem placeItem = new PlaceItem(name, address, city, road, lon, lat);
                                    final ItineraryPlaceItem visitedPlaceItem = new ItineraryPlaceItem(id, placeItem);
                                    placesList.add(visitedPlaceItem);
                                }
                            }
                            itineraryItem.setPlaces(placesList);
                            adapter.setData(placesList);
                        }else{
                            Utility.showNeutralAlert(getString(R.string.attenzione),jsonObject.getString("message"), PlacesInItineraryActivity.this);
                        }
                    } catch (final JSONException e) {
                        Log.e(getString(R.string.error), e.toString());
                    }
                },
                error -> Log.e(getString(R.string.error), error.toString())){
            @Override
            protected Map<String,String> getParams(){
                final Map<String, String> params = new HashMap<>();
                params.put("type", "getPlacesInItinerary");
                params.put("itinerary_id", itinerary_id);
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }
}
