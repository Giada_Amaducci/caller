package com.example.call_er;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.call_er.Utilities.Extra;
import com.example.call_er.Utilities.FileUtilities;
import com.example.call_er.Utilities.ImageUtilities;
import com.example.call_er.Utilities.InternetUtilities;
import com.example.call_er.Utilities.UserManagement;
import com.example.call_er.Utilities.Utility;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    private NavigationView navigationView;
    private int notificationNumber = 0;
    private TextView notification;

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.nav_message, menu);

        final MenuItem item = menu.findItem(R.id.messages);
        final RelativeLayout menu_message = (RelativeLayout) item.getActionView();
        notification = menu_message.findViewById(R.id.hotlist_hot);
        updateNotificationCount(notificationNumber);
        menu_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent i = new Intent(getApplicationContext(), ChatsActivity.class);
                startActivity(i);
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    private void updateNotificationCount(final int new_hot_number) {
        notificationNumber = new_hot_number;
        runOnUiThread(() -> {
            if (new_hot_number == 0)
                notification.setVisibility(View.INVISIBLE);
            else {
                notification.setVisibility(View.VISIBLE);
                final String message = "" + new_hot_number;
                notification.setText(message);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_main);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }


    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.homeDrawerLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        final String title = item.getTitle().toString();
        final Intent i;
        switch(title) {
            case "I Tuoi Itinerari":
                if (InternetUtilities.getIsNetworkConnected()) {
                    i = new Intent(this.getApplicationContext(), ItinerariesActivity.class);
                    i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "MainActivity");
                    this.startActivity(i);
                } else {
                    InternetUtilities.getSnackbar().show();
                }
                break;
            case "I Tuoi Luoghi":
                if (InternetUtilities.getIsNetworkConnected()) {
                    i = new Intent(this.getApplicationContext(), VisitedPlacesActivity.class);
                    this.startActivity(i);
                } else {
                    InternetUtilities.getSnackbar().show();
                }
                break;
            case "I Tuoi Luoghi Preferiti":
                if (InternetUtilities.getIsNetworkConnected()) {
                    i = new Intent(this.getApplicationContext(), FavoritePlacesActivity.class);
                    this.startActivity(i);
                } else {
                    InternetUtilities.getSnackbar().show();
                }
                break;
            case "Mappa del Territorio":
                if (InternetUtilities.getIsNetworkConnected()) {
                    i = new Intent(this.getApplicationContext(), MapActivity.class);
                    this.startActivity(i);
                } else {
                    InternetUtilities.getSnackbar().show();
                }
                break;
            case "Il Tuo Calendario":
                if (InternetUtilities.getIsNetworkConnected()) {
                    i = new Intent(this.getApplicationContext(), CalendarActivity.class);
                    i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "MainActivity");
                    this.startActivity(i);
                } else {
                    InternetUtilities.getSnackbar().show();
                }
                break;
            case "Ricerca Luoghi":
                if (InternetUtilities.getIsNetworkConnected()) {
                    i = new Intent(this.getApplicationContext(), SearchPlaceActivity.class);
                    i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "MainActivity");
                    this.startActivity(i);
                } else {
                    InternetUtilities.getSnackbar().show();
                }
                break;
            case "Il Tuo Profilo":
                if (InternetUtilities.getIsNetworkConnected()) {
                    i = new Intent(this.getApplicationContext(), AccountActivity.class);
                    this.startActivity(i);
                } else {
                    InternetUtilities.getSnackbar().show();
                }
                break;
            case "Logout":
                FileUtilities.removeFile(this.getCacheDir() + "/mtj.txt");
                i = new Intent(this.getApplicationContext(), LoginActivity.class);
                this.startActivity(i);
                break;
            default:
                break;
        }
        return true;
    }

    private void initUI(){
        final DrawerLayout drawerLayout = this.findViewById(R.id.homeDrawerLayout);
        this.navigationView = this.findViewById(R.id.navView);
        final Toolbar toolbar = this.findViewById(R.id.app_bar);
        this.setSupportActionBar(toolbar);

        Utility.setUpToolbar(this, this.getString(R.string.app_name));
        final ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                toolbar,
                R.string.open_nav_drawer,
                R.string.close_nav_drawer
        );

        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        this.navigationView.setNavigationItemSelectedListener(this);
        this.getUserData();

        this.findViewById(R.id.foodLinearLayout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), PlacesActivity.class);
                i.putExtra(Extra.TYPE.getTypeExtra(), "Food");
                startActivity(i);
            }
        });

        this.findViewById(R.id.serviceLinearLayout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), PlacesActivity.class);
                i.putExtra(Extra.TYPE.getTypeExtra(), "Service");
                startActivity(i);
            }
        });

        this.findViewById(R.id.cultureLinearLayout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), PlacesActivity.class);
                i.putExtra(Extra.TYPE.getTypeExtra(), "Culture");
                startActivity(i);
            }
        });

        this.findViewById(R.id.itineraryLinearLayout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), PublicItinerariesActivity.class);
                startActivity(i);
            }
        });

        if (InternetUtilities.getIsNetworkConnected()) {
            this.getNumberOfMessagesToRead();
        } else {
            InternetUtilities.getSnackbar().show();
        }
    }

    private void getUserData() {
        final List<String> values = FileUtilities.getData(this.getCacheDir() + "/mtj.txt");
        final View headerView = this.navigationView.getHeaderView(0);

        final TextView navUsername = headerView.findViewById(R.id.mnUserTextView);
        navUsername.setText(values.get(UserManagement.USERNAME.getId()));

        final TextView navPhone = headerView.findViewById(R.id.mnPhoneTextView);
        navPhone.setText(values.get(UserManagement.PHONE.getId()));

        final ImageView userImage = headerView.findViewById(R.id.profileCircleImageView);
        userImage.setImageBitmap(ImageUtilities.decodeBitmap(values));
    }

    private void getNumberOfMessagesToRead(){
        final String url = "https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        try {
                            final JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("result").equals("success")) {
                                final int number = jsonObject.getInt("messagesToRead");
                                updateNotificationCount(number);
                            }else{
                                Utility.showNeutralAlert(getString(R.string.attenzione),jsonObject.getString("message"), MainActivity.this);
                            }
                        } catch (final JSONException e) {
                            Log.e(getString(R.string.error), e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError error) {
                        Log.e(getString(R.string.error), error.toString());
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                final Map<String, String> params = new HashMap<>();
                params.put("type", "getNumberOfMessagesToRead");
                params.put("user_id", FileUtilities.getData( getCacheDir()+"/mtj.txt").get(UserManagement.ID.getId()));
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

}
