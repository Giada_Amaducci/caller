package com.example.call_er;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioGroup;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.text.HtmlCompat;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.call_er.MailManager.SendMail;
import com.example.call_er.Utilities.ImageUtilities;
import com.example.call_er.Utilities.InternetUtilities;
import com.example.call_er.Utilities.Utility;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class RegistrationActivity extends AppCompatActivity {

    private ImageView imageView;
    private String image;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_registration);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ImageUtilities.PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            final Uri filePath = data.getData();
            final Bitmap bitmap = ImageUtilities.getBitmap(filePath,getContentResolver());
            this.imageView.setImageBitmap(bitmap);
            this.image = ImageUtilities.getStringImage(bitmap, true);
        } else if(requestCode == ImageUtilities.REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK && data != null){
            final Bitmap bitmap = (Bitmap) Objects.requireNonNull(data.getExtras()).get("data");
            try {
                assert bitmap != null;
                ImageUtilities.saveImage(bitmap,this);
                this.imageView.setImageBitmap(bitmap);
                this.image = ImageUtilities.getStringImage(bitmap, false);
            } catch (IOException e) {
                Log.e(getString(R.string.error), e.toString());
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.registrationLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI(){
        this.imageView = this.findViewById(R.id.profileCircleImageView);

        this.findViewById(R.id.registrationContainedButton).setOnClickListener(v -> {
            final TextInputLayout emailField = findViewById(R.id.emailOutlinedTextField);
            final TextInputLayout password1Field = findViewById(R.id.passwordOutlinedTextField);
            final TextInputLayout usernameField = findViewById(R.id.nameOutlinedTextField);
            final TextInputLayout password2Field = findViewById(R.id.password1OutlinedTextField);
            final TextInputLayout numberField = findViewById(R.id.numberOutlinedTextField);
            final RadioGroup radioGroup = findViewById(R.id.roleRadioGroup);

            final String username = Objects.requireNonNull(usernameField.getEditText()).getText().toString();
            final String email = Objects.requireNonNull(emailField.getEditText()).getText().toString();
            final String password1 = Objects.requireNonNull(password1Field.getEditText()).getText().toString();
            final String password2 = Objects.requireNonNull(password2Field.getEditText()).getText().toString();
            final String number = Objects.requireNonNull(numberField.getEditText()).getText().toString();

            final int selectedRadio = radioGroup.getCheckedRadioButtonId();
            final String role;
            switch(selectedRadio) {
                case R.id.userRadioButton:
                    role = "user";
                    break;
                case R.id.organizerRadioButton:
                    role = "organizer_to_approve";
                    break;
                default:
                    role = "error";
                    break;
            }

            if (InternetUtilities.getIsNetworkConnected()) {
                if (!password1.equals(password2)) {
                    Utility.showNeutralAlert(getString(R.string.attenzione),getString(R.string.due_password_non_coincidono), RegistrationActivity.this);
                } else {
                    if (!username.isEmpty() && !email.isEmpty() && !password1.isEmpty() && image != null){
                        addRegistration(username, password1, email, image, number, role);
                    }else {
                        Utility.showNeutralAlert(getString(R.string.attenzione), getString(R.string.dati_non_inseriti), RegistrationActivity.this);
                    }
                }
            } else {
                InternetUtilities.getSnackbar().show();
            }
        });

        this.findViewById(R.id.addPhotoImageButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(ImageUtilities.showFileChooser(), ImageUtilities.PICK_IMAGE_REQUEST);
            }
        });

        this.findViewById(R.id.makePhotoImageButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(ImageUtilities.getPhoto(), ImageUtilities.REQUEST_IMAGE_CAPTURE);
            }
        });

        Utility.setUpToolbar(this, this.getString(R.string.registrati1));
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    private void addRegistration(final String username, final String password, final String email, final String image, final String number, final String role) {
        final Map<String, String> params = new HashMap<>();
        params.put("type", "registration");
        params.put("username", username);
        params.put("email", email);
        params.put("number", number);
        params.put("password", password);
        params.put("image", image);
        params.put("role", role);

        this.addRegistration(this, params);
    }

    private void addRegistration(final Activity activity, final Map<String, String> params){
        final String url = "https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    Log.e("VALUE", response);
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("result").equals("success")) {
                            Utility.saveUserData(activity, params.get("email"), params.get("username"), params.get("image"), params.get("number"), jsonObject.getString("id"), params.get("role"));
                            sendEmail(params.get("email"), params.get("username"));
                            final Intent i = new Intent(activity.getApplicationContext(), MainActivity.class);
                            activity.startActivity(i);
                        }else{
                            Utility.showNeutralAlert(activity.getString(R.string.attenzione),jsonObject.getString("message"), activity);
                        }
                    } catch (final JSONException e) {
                        Log.e(activity.getString(R.string.error), e.toString());
                    }
                },
                error -> Log.e(activity.getString(R.string.error), error.toString())){
            @Override
            protected Map<String,String> getParams(){
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

    private void sendEmail(final String email, final String name) {
        final String subject = "Benvenuto su Call-ER";
        String message;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            message = Html.fromHtml("<p style=\"color:#005B9F\">Registrazione completata!</p>" + name + ", grazie per esserti registrato/a su <font color=\"#00675B\">CallER</font>!<br /><br />Conosci gi&agrave; tutto di CallER? Una volta registrato puoi cercare gli eventi pi&ugrave; vicini a te e trascorrere in un modo alternativo il tuo tempo libero, all'insegna del divertimento e della cultura. Puoi inoltre condividere ogni posto visitato con i tuoi amici!<br/><br/>Sei gi&agrave; curioso di cosa abbiamo da offrirti? Entra subito nell'applicazione. Ti aspettiamo! <br/><br/> Questa è un'email autogenerata. Non rispondere a questa email.", HtmlCompat.FROM_HTML_MODE_LEGACY).toString();
        }else{
            message = Html.fromHtml("<p style=\"color:#005B9F\">Registrazione completata!</p>" + name + ", grazie per esserti registrato/a su <font color=\"#00675B\">CallER</font>!<br /><br />Conosci gi&agrave; tutto di CallER? Una volta registrato puoi cercare gli eventi pi&ugrave; vicini a te e trascorrere in un modo alternativo il tuo tempo libero, all'insegna del divertimento e della cultura. Puoi inoltre condividere ogni posto visitato con i tuoi amici!<br/><br/>Sei gi&agrave; curioso di cosa abbiamo da offrirti? Entra subito nell'applicazione. Ti aspettiamo! <br/><br/> Questa è un'email autogenerata. Non rispondere a questa email.").toString();
        }

        SendMail sm = new SendMail(email, subject, message);

        sm.execute();
    }

}
