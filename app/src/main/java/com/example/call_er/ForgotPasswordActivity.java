package com.example.call_er;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.text.HtmlCompat;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.call_er.MailManager.SendMail;
import com.example.call_er.Utilities.InternetUtilities;
import com.example.call_er.Utilities.Utility;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ForgotPasswordActivity extends AppCompatActivity {
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_forgot_password);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.forgotPasswordConstraintLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI(){
        this.findViewById(R.id.passwordContainedButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final TextInputLayout emailField = findViewById(R.id.emailOutlinedTextField);

                if(emailField.getEditText() != null){
                    final String email = emailField.getEditText().getText().toString();
                    if(!email.isEmpty()){
                        final String password = Utility.randomPassword();

                        if (InternetUtilities.getIsNetworkConnected()) {
                            forgotPassword(email, password);
                        } else {
                            InternetUtilities.getSnackbar().show();
                        }
                    }else{
                        Utility.showNeutralAlert(getString(R.string.attenzione), "Non hai inserito nessuna email!", ForgotPasswordActivity.this);
                    }
                }
            }
        });

        Utility.setUpToolbar(this, "Richiedi Password");
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    private void forgotPassword(final String email, final String password){
        final String url ="https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("result").equals("success")) {
                            sendEmail(email, password);
                            Utility.showToast("La nuova password è stata inviata sulla tua email!", ForgotPasswordActivity.this);
                            final Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(i);
                        }else{
                            Utility.showNeutralAlert(getString(R.string.attenzione), jsonObject.getString("message"), ForgotPasswordActivity.this);
                        }
                    } catch (final JSONException e) {
                        Log.e(getString(R.string.error), e.toString());
                    }
                },
                error -> Log.e(getString(R.string.error), error.toString())){
            @Override
            protected Map<String,String> getParams(){
                final Map<String,String> params = new HashMap<>();
                params.put("type", "forgot_password");
                params.put("email", email);
                params.put("password", password);
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

    private void sendEmail(final String email, final String password) {
        final String subject = "La tua password è stata modificata";

        String message;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            message = Html.fromHtml("<p style=\"color:#005B9F\">Buongiorno!</p></br></br> Come da tua richiesta, abbiamo provveduto al cambio di password. </br></br> La nuova password è: </br> </br> <center>" + password + "</center> </br></br> Ti suggeriamo, di cambiarla il prima possibile! Buona Giornata, lo staff di CallER. <br/><br/> Questa è un'email autogenerata. Non rispondere a questa email." , HtmlCompat.FROM_HTML_MODE_LEGACY).toString();
        }else{
            message = Html.fromHtml("<p style=\"color:#005B9F\">Buongiorno!</p></br></br> Come da tua richiesta, abbiamo provveduto al cambio di password. </br></br> La nuova password è: </br> </br> <center>" + password + "</center> </br></br> Ti suggeriamo, di cambiarla il prima possibile! Buona Giornata, lo staff di CallER. <br/><br/> Questa è un'email autogenerata. Non rispondere a questa email.").toString();
        }
        SendMail sm = new SendMail(email, subject, message);

        sm.execute();
    }
}
