package com.example.call_er;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.call_er.Adapters.RecyclerView.MessageListAdapter;
import com.example.call_er.Items.MessageItem;
import com.example.call_er.Items.UserItem;
import com.example.call_er.Utilities.Extra;
import com.example.call_er.Utilities.FileUtilities;
import com.example.call_er.Utilities.InternetUtilities;
import com.example.call_er.Utilities.UserManagement;
import com.example.call_er.Utilities.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class MessageListActivity extends AppCompatActivity {

    private MessageListAdapter mMessageAdapter;
    private List<MessageItem> messageItemList;
    private EditText chatBoxEditText;
    private String activityName;
    private UserItem userItem;


    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            this.onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if(this.getClass().equals(MessageListActivity.class)){
            final Intent i;
            if(this.activityName.equals("ChatsActivity")){
                i = new Intent(getApplicationContext(), ChatsActivity.class);
            }else if(this.activityName.equals("SearchUserActivity")){
                i = new Intent(getApplicationContext(), SearchUserActivity.class);
            }else{
                i = new Intent(getApplicationContext(), UserInfoActivity.class);
                i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "MessageListActivity");
                i.putExtra(Extra.ID.getTypeExtra(), this.userItem.getId());
                i.putExtra(Extra.NAME.getTypeExtra(), this.userItem.getName());
                i.putExtra(Extra.IMAGE.getTypeExtra(), this.userItem.getImage());
                i.putExtra(Extra.EMAIL.getTypeExtra(), this.userItem.getEmail());
            }
            startActivity(i);
        }else{
            finish();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_message_list);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }


    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.messageListConstraintLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    private void initUI(){
        final String name = this.getIntent().getStringExtra(Extra.NAME.getTypeExtra());
        final String userId = this.getIntent().getStringExtra(Extra.ID.getTypeExtra());
        this.activityName = this.getIntent().getStringExtra(Extra.ACTIVITY_NAME.getTypeExtra());

        if(this.activityName != null && this.activityName.equals("UserInfoActivity")){
            final String email = this.getIntent().getStringExtra(Extra.EMAIL.getTypeExtra());
            this.userItem = new UserItem(name, email, userId);
        }else{
            this.userItem = new UserItem(name, userId);
        }

        final RecyclerView mMessageRecycler = this.findViewById(R.id.reyclerviewMessageList);
        mMessageRecycler.setHasFixedSize(true);
        mMessageRecycler.setLayoutManager(new LinearLayoutManager(this));
        this.mMessageAdapter = new MessageListAdapter(this);
        mMessageRecycler.setAdapter(this.mMessageAdapter);

        this.messageItemList = new ArrayList<>();

        final String id = FileUtilities.getData(this.getCacheDir()+"/mtj.txt").get(UserManagement.ID.getId());
        if (InternetUtilities.getIsNetworkConnected()) {
            this.getMessages(id, userId);
        } else {
            InternetUtilities.getSnackbar().show();
        }

        this.findViewById(R.id.buttonChatboxSend).setOnClickListener(v -> {
            chatBoxEditText = findViewById(R.id.chatBoxEditText);

            final String message = chatBoxEditText.getText().toString();
            if (InternetUtilities.getIsNetworkConnected()) {
                addMessage(id, userId, message);
            } else {
                InternetUtilities.getSnackbar().show();
            }
        });

        Utility.setUpToolbar(this, name);
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    private void addMessage(final String id, final String userId, final String message){
        final Map<String, String> params = new HashMap<>();
        params.put("type", "addMessage");
        params.put("id", id);
        params.put("userId", userId);
        params.put("message", message);
        this.addMessage(params);
    }

    private void addMessage(final Map<String, String> params){
        final String url ="https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("result").equals("success")) {
                            final MessageItem messageItem = new MessageItem(params.get("id"), params.get("userId"),params.get("message"), jsonObject.getString("createdAt"));
                            messageItemList.add(messageItem);
                            mMessageAdapter.setData(messageItemList);
                            chatBoxEditText.getText().clear();

                        }else{
                            Utility.showNeutralAlert(getString(R.string.attenzione), jsonObject.getString("message"), MessageListActivity.this);
                        }
                    } catch (final JSONException e) {
                        Log.e(getString(R.string.error), e.toString());
                    }
                },
                error -> Log.e(getString(R.string.error), error.toString())){
            @Override
            protected Map<String,String> getParams(){
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

    private void getMessages(final String id, final String userId){
        final Map<String, String> params = new HashMap<>();
        params.put("type", "getMessages");
        params.put("id", id);
        params.put("userId", userId);
        this.getMessages(params);
    }

    private void getMessages(final Map<String, String> params){
        final String url = "https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("result").equals("success")) {
                            final JSONArray jsonArray = new JSONArray(jsonObject.getString("values"));
                            for (int i = 0; i <jsonArray.length(); i++){
                                final JSONObject message = (JSONObject)jsonArray.get(i);
                                final String receiverId = message.getString("receiver_id");
                                final String senderId = message.getString("sender_id");
                                final String mess = message.getString("message");
                                final String createdAt = message.getString("createdAt");
                                final MessageItem messageItem = new MessageItem(senderId, receiverId, mess, createdAt);
                                messageItemList.add(messageItem);
                            }
                            mMessageAdapter.setData(messageItemList);
                        }else{
                            Utility.showNeutralAlert(getString(R.string.attenzione),jsonObject.getString("message"), MessageListActivity.this);
                        }
                    } catch (final JSONException e) {
                        Log.e(getString(R.string.error), e.toString());
                    }
                },
                error -> Log.e(getString(R.string.error), error.toString())){
            @Override
            protected Map<String,String> getParams(){
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

}
