package com.example.call_er.Items;

public class ReviewItem {
    private final UserItem userItem;
    private final String vote;
    private final String message;

    public ReviewItem(final UserItem userItem, final String vote, final String message){
        this.userItem = userItem;
        this.vote = vote;
        this.message = message;
    }

    public UserItem getUserItem() {
        return this.userItem;
    }

    public String getVote() {
        return vote;
    }

    public String getMessage() {
        return message;
    }
}
