package com.example.call_er.Items;

public class MessageItem {

    private String receiverId;
    private String senderId;
    private String message;
    private String createdAt;

    public MessageItem(final String senderId, final String receiverId, final String message, final String createdAt) {
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.message = message;
        this.createdAt = createdAt;
    }

    public String getMessage() {
        return message;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public String getSenderId() {
        return senderId;
    }
}
