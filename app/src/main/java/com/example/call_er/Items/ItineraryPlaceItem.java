package com.example.call_er.Items;

import org.json.JSONException;
import org.json.JSONObject;

public class ItineraryPlaceItem {

    private final String id;
    private final PlaceItem placeItem;

    public ItineraryPlaceItem(final String id, final PlaceItem placeItem) {
        this.id = id;
        this.placeItem = placeItem;
    }

    public String getId() {
        return id;
    }

    public PlaceItem getPlaceItem() {
        return placeItem;
    }

    public JSONObject getJSONPlaceItem(){
        final JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("id",this.id);
            jsonObject.put("name", this.placeItem.getName());
            jsonObject.put("address", this.placeItem.getAddress());
            jsonObject.put("road", this.placeItem.getRoad());
            jsonObject.put("city", this.placeItem.getCity());
            jsonObject.put("lat", this.placeItem.getLat());
            jsonObject.put("lon", this.placeItem.getLon());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }
}
