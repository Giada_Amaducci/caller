package com.example.call_er.Items;

public class VisitedPlaceOrItineraryItem {

    private VisitedPlaceItem visitedPlaceItem;
    private ItineraryItem itineraryItem;
    private final boolean isPlace;

    public VisitedPlaceOrItineraryItem(final VisitedPlaceItem visitedPlaceItem){
        this.visitedPlaceItem = visitedPlaceItem;
        this.isPlace = true;
    }

    public VisitedPlaceOrItineraryItem(final ItineraryItem itineraryItem){
        this.itineraryItem = itineraryItem;
        this.isPlace = false;
    }

    public ItineraryItem getItineraryItem() {
        return this.itineraryItem;
    }

    public VisitedPlaceItem getVisitedPlaceItem() {
        return this.visitedPlaceItem;
    }


    public boolean isPlace() {
        return this.isPlace;
    }
}
