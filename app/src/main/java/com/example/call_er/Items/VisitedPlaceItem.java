package com.example.call_er.Items;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class VisitedPlaceItem {
    private final String id;
    private final String name;
    private final String address;
    private final String road;
    private final String lat;
    private final String lon;
    private final String city;
    private String date;
    private boolean isPrefered;
    private ImageItem image;

    public VisitedPlaceItem(final String id, final String name, final String address, final String city, final String road, final boolean isPrefered, final String lat, final String lon, final String date) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.city = city;
        this.road = road;
        this.lat = lat;
        this.lon = lon;
        this.isPrefered = isPrefered;
        this.date = date;
    }

    public String getLat() {
        return lat;
    }

    public String getLon() {
        return lon;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setImage(final ImageItem image) {
        this.image = image;
    }

    public ImageItem getImage() {
        return image;
    }

    public String getId(){
        return this.id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public String getRoad() {
        return this.road;
    }

    public boolean isPrefered() {
        return this.isPrefered;
    }

    public void setIsPrefered(final boolean isPrefered) {
        this.isPrefered = isPrefered;
    }

    public JSONObject getJSONPlaceItem(){
        final JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("id", this.id);
            jsonObject.put("name", this.name);
            jsonObject.put("address", this.address);
            jsonObject.put("date", this.date);
            jsonObject.put("road", this.road);
            jsonObject.put("city", this.city);
            jsonObject.put("lat", this.lat);
            jsonObject.put("lon", this.lon);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }
}
