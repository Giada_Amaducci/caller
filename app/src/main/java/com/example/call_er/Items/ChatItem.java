package com.example.call_er.Items;

public class ChatItem {

    private final String image;
    private final String name;
    private final String lastMessage;
    private final String email;
    private final String id;
    private final int unreadMessages;

    public ChatItem(final String name, final String image, final String lastMessage, final String id, final String email, final int unreadMessages) {
        this.name = name;
        this.image = image;
        this.lastMessage = lastMessage;
        this.id = id;
        this.email = email;
        this.unreadMessages = unreadMessages;
    }

    public String getImage() {
        return this.image;
    }

    public String getName() {
        return this.name;
    }

    public String getLastMessage() {
        return this.lastMessage;
    }

    public String getId() {
        return this.id;
    }

    public String getEmail() {
        return this.email;
    }

    public int getUnreadMessages() {
        return this.unreadMessages;
    }
}
