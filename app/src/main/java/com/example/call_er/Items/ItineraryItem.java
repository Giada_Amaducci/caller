package com.example.call_er.Items;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ItineraryItem {

    private final String id;
    private final String name;
    private String returnDate;
    private String departureDate;
    private String creatorName;
    private String votes;
    private String avg;
    private final String city;
    private String users;
    private List<ItineraryPlaceItem> places;
    private ImageItem imageItem;

    public ItineraryItem(final String id, final String name, final String city) {
        this.id = id;
        this.name = name;
        this.city = city;
        this.places = new ArrayList<>();
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getName() {
        return this.name;
    }

    public String getCity() {
        return this.city;
    }

    public String getDepartureDate() {
        return this.departureDate;
    }

    public String getReturnDate() {
        return this.returnDate;
    }

    public List<ItineraryPlaceItem> getPlaces(){
        return this.places;
    }

    public void setPlaces(final List<ItineraryPlaceItem> cities) {
        this.places = cities;
    }

    public String getId(){
        return this.id;
    }

    public void setPlaces(final String jsonArray) {
        try {
            final JSONArray places = new JSONArray(jsonArray);
            for(int i=0; i<places.length(); i++){
                final JSONObject place = places.getJSONObject(i);
                final PlaceItem placeItem = new PlaceItem(place.getString("name"), place.getString("address"), place.getString("city"),place.getString("road"), place.getString("lon"), place.getString("lat"));
                final ItineraryPlaceItem itineraryPlaceItem = new ItineraryPlaceItem(place.getString("id"), placeItem);
                this.places.add(itineraryPlaceItem);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getAllPlaces(){
        final JSONArray jsonArray = new JSONArray();
        for (ItineraryPlaceItem itineraryPlaceItem: this.places) {
            jsonArray.put(itineraryPlaceItem.getJSONPlaceItem());
        }
        return jsonArray.toString();
    }

    public void setCreatorName(final String creator_name) {
        this.creatorName = creator_name;
    }

    public String getCreatorName() {
        return this.creatorName;
    }

    public String getUsers() {
        return this.users;
    }

    public void setUsers(final String users) {
        this.users = users;
    }

    public String getVotes() {
        return votes;
    }

    public void setVotes(String votes) {
        this.votes = votes;
    }

    public String getAvg() {
        return avg;
    }

    public void setAvg(String avg) {
        this.avg = avg;
    }

    public ImageItem getImage() {
        return imageItem;
    }

    public void setImage(ImageItem imageItem) {
        this.imageItem = imageItem;
    }
}
