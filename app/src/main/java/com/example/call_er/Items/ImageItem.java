package com.example.call_er.Items;

public class ImageItem {
    private final String image;
    private String vote;
    private boolean isVoted;
    private final String id;

    public ImageItem(final String id, final String image, final String vote, final boolean isVoted){
        this.image = image;
        this.id = id;
        this.vote = vote;
        this.isVoted = isVoted;
    }

    public ImageItem(final String id, final String image, final boolean isVoted){
        this.image = image;
        this.id = id;
        this.isVoted = isVoted;
    }

    public ImageItem(final String id, final String image){
        this.image = image;
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public String getVote() {
        return vote;
    }

    public String getId() {
        return id;
    }

    public void setVote(final String vote) {
        this.vote = vote;
    }

    public boolean isVoted(){
        return this.isVoted;
    }

    public void setIsVoted(final boolean isVoted){
        this.isVoted = isVoted;
    }
}
