package com.example.call_er.Items;

import org.json.JSONException;
import org.json.JSONObject;

public class PlaceItem {
    private final String name;
    private final String address;
    private final String road;
    private final String city;
    private String lat;
    private String lon;

    public PlaceItem(final String name, final String address, final String city, final String road, final String lon, final String lat) {
        this.name = name;
        this.address = address;
        this.city = city;
        this.road = road;
        this.lat = lat;
        this.lon = lon;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public String getRoad() {
        return this.road;
    }

    public String getLat(){
        return this.lat;
    }

    public String getLon(){
        return this.lon;
    }
}
