package com.example.call_er.Items;

import com.example.call_er.Utilities.DateAndCalendarUtilities;

public class GenericItem {

    private final String name;
    private final String date;
    private final boolean isItinerary;

    public GenericItem(final String name, final String departureDate, final String returnDate) {
        this.name = name;
        this.isItinerary = true;
        this.date = String.format("%s - %s", DateAndCalendarUtilities.getCorrectDateFormatForOutput(departureDate), DateAndCalendarUtilities.getCorrectDateFormatForOutput(returnDate));
    }

    public GenericItem(final String name, final String date) {
        this.name = name;
        this.date =  DateAndCalendarUtilities.getCorrectDateFormatForOutput(date);
        this.isItinerary = false;
    }

    public boolean isItinerary() {
        return isItinerary;
    }

    public String getName() {
        return this.name;
    }

    public String getDate() {
        return this.date;
    }

}
