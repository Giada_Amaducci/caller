package com.example.call_er.Items;

public class UserItem {

    private String image;
    private String email;
    private String phone;
    private final String name;
    private final String id;

    public UserItem(final String name, final String image, final String email, final String id, final String phone) {
        this.name = name;
        this.image = image;
        this.phone = phone;
        this.email = email;
        this.id = id;
    }

    public UserItem(final String name, final String image, final String email, final String id) {
        this.name = name;
        this.image = image;
        this.email = email;
        this.id = id;
    }

    public UserItem(final String name, final String id) {
        this.name = name;
        this.id = id;
    }

    public UserItem(final String name, final String email, final String id) {
        this.name = name;
        this.email = email;
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }
}
