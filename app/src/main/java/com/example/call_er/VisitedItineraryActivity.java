package com.example.call_er;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.call_er.Adapters.ImageSliderAdapter.VisitedItineraryImageSliderAdapter;
import com.example.call_er.Items.ImageItem;
import com.example.call_er.Items.ItineraryItem;
import com.example.call_er.Utilities.DateAndCalendarUtilities;
import com.example.call_er.Utilities.Extra;
import com.example.call_er.Utilities.FileUtilities;
import com.example.call_er.Utilities.ImageUtilities;
import com.example.call_er.Utilities.InternetUtilities;
import com.example.call_er.Utilities.UserManagement;
import com.example.call_er.Utilities.Utility;
import com.google.android.material.textfield.TextInputLayout;
import com.smarteist.autoimageslider.SliderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class VisitedItineraryActivity extends AppCompatActivity {

    private String prevActivity;
    private ItineraryItem itineraryItem;
    private List<ImageItem> images;
    private VisitedItineraryImageSliderAdapter adapter;
    private String places;

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            this.onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if(this.getClass().equals(VisitedItineraryActivity.class)){
            Intent i;
            if(prevActivity.equals("CalendarActivity")){
                i = new Intent(getApplicationContext(), CalendarActivity.class);
            }else{
                i = new Intent(getApplicationContext(), ItinerariesActivity.class);
                i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "ItineraryActivity");
            }
            startActivity(i);
        }else{
            finish();
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String image;
        if (requestCode == ImageUtilities.PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            final Uri filePath = data.getData();
            final Bitmap bitmap = ImageUtilities.getBitmap(filePath,getContentResolver());
            image = ImageUtilities.getStringImage(bitmap, true);
            this.addItineraryImage(image, adapter);
        } else if(requestCode == ImageUtilities.REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK && data != null){
            final Bitmap bitmap = (Bitmap) Objects.requireNonNull(data.getExtras()).get("data");
            try {
                assert bitmap != null;
                ImageUtilities.saveImage(bitmap,this);
                image = ImageUtilities.getStringImage(bitmap, false);
                this.addItineraryImage(image, adapter);
            } catch (IOException e) {
                Log.e(getString(R.string.error), e.toString());
            }
        }
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_visited_itinerary);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.itineraryInfoConstraintLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI(){
        final String title = this.getIntent().getStringExtra(Extra.NAME.getTypeExtra());
        final String departureDate = this.getIntent().getStringExtra(Extra.DEPARTURE_DATE.getTypeExtra());
        final String returnDate = this.getIntent().getStringExtra(Extra.RETURN_DATE.getTypeExtra());
        final String itineraryId = this.getIntent().getStringExtra(Extra.ID.getTypeExtra());
        final String city = this.getIntent().getStringExtra(Extra.CITY.getTypeExtra());
        this.places = this.getIntent().getStringExtra(Extra.PLACES.getTypeExtra());
        final String creator_name = this.getIntent().getStringExtra(Extra.CREATOR_NAME.getTypeExtra());
        this.prevActivity = this.getIntent().getStringExtra(Extra.ACTIVITY_NAME.getTypeExtra());

        this.itineraryItem = new ItineraryItem(itineraryId, title, city);
        this.itineraryItem.setDepartureDate(departureDate);
        this.itineraryItem.setReturnDate(returnDate);
        this.itineraryItem.setCreatorName(creator_name);

        if(this.places != null){
            this.itineraryItem.setPlaces(places);
        }

        final TextInputLayout departureDateTextInputLayout = this.findViewById(R.id.departureDateTextInputLayout);
        final TextInputLayout returnDateTextInputLayout = this.findViewById(R.id.returnDateTextInputLayout);
        final RadioGroup radioGroup = this.findViewById(R.id.searchRadioGroup);

        Objects.requireNonNull(departureDateTextInputLayout.getEditText())
                .setText(getCorrectFormatData(Objects.requireNonNull(DateAndCalendarUtilities.getCorrectDateFormatForOutput(departureDate))));
        Objects.requireNonNull(returnDateTextInputLayout.getEditText())
                .setText(getCorrectFormatData(Objects.requireNonNull(DateAndCalendarUtilities.getCorrectDateFormatForOutput(returnDate))));

        departureDateTextInputLayout.setEndIconOnClickListener(v -> DateAndCalendarUtilities.showDatePickerDialog(VisitedItineraryActivity.this, departureDateTextInputLayout.getEditText(), VisitedItineraryActivity.this, itineraryItem, true));

        returnDateTextInputLayout.setEndIconOnClickListener(v -> DateAndCalendarUtilities.showDatePickerDialog(VisitedItineraryActivity.this, returnDateTextInputLayout.getEditText(), VisitedItineraryActivity.this, itineraryItem, false));

        final int cityId;
        if (city != null) {
            if (city.equals("Cesena")) {
                cityId = 0;
            }else{
                cityId = 1;
            }
        }else{
            cityId = -1;
        }

        radioGroup.check(radioGroup.getChildAt(cityId).getId());

        for (int i = 0; i < radioGroup.getChildCount(); i++) {
            radioGroup.getChildAt(i).setEnabled(false);
        }

        final TextView creatorTextView = this.findViewById(R.id.creatorFixTextView);
        creatorTextView.setText(this.itineraryItem.getCreatorName());

        this.images = new ArrayList<>();

        final SliderView sliderView = findViewById(R.id.imageSlider);
        this.adapter = new VisitedItineraryImageSliderAdapter(this, this);
        sliderView.setSliderAdapter(adapter);

        if (InternetUtilities.getIsNetworkConnected()) {
            this.getItineraryImages(this.itineraryItem.getId(), adapter);
        } else {
            InternetUtilities.getSnackbar().show();
        }

        this.findViewById(R.id.addUpdatePhotoImageButton).setOnClickListener(v -> startActivityForResult(ImageUtilities.showFileChooser(), ImageUtilities.PICK_IMAGE_REQUEST));

        this.findViewById(R.id.makeUpdatePhotoImageButton).setOnClickListener(v -> startActivityForResult(ImageUtilities.getPhoto(), ImageUtilities.REQUEST_IMAGE_CAPTURE));

        this.findViewById(R.id.placesListButton).setOnClickListener(v -> {
            final Intent i = new Intent(getApplicationContext(), PlacesInItineraryActivity.class);
            i.putExtra(Extra.NAME.getTypeExtra(), itineraryItem.getName());
            i.putExtra(Extra.DEPARTURE_DATE.getTypeExtra(), itineraryItem.getDepartureDate());
            i.putExtra(Extra.RETURN_DATE.getTypeExtra(), itineraryItem.getReturnDate());
            i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "ItineraryActivity");
            i.putExtra(Extra.CREATOR_NAME.getTypeExtra(), itineraryItem.getCreatorName());
            i.putExtra(Extra.ID.getTypeExtra(), itineraryItem.getId());
            i.putExtra(Extra.CITY.getTypeExtra(), itineraryItem.getCity());
            i.putExtra(Extra.PLACES.getTypeExtra(), this.places);
            startActivity(i);
        });

        this.findViewById(R.id.reviewButton).setOnClickListener(v -> {
            final Intent i = new Intent(getApplicationContext(), ItineraryReviewActivity.class);
            i.putExtra(Extra.NAME.getTypeExtra(), itineraryItem.getName());
            i.putExtra(Extra.DEPARTURE_DATE.getTypeExtra(), itineraryItem.getDepartureDate());
            i.putExtra(Extra.RETURN_DATE.getTypeExtra(), itineraryItem.getReturnDate());
            i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "ItineraryActivity");
            i.putExtra(Extra.CREATOR_NAME.getTypeExtra(), itineraryItem.getCreatorName());
            i.putExtra(Extra.ID.getTypeExtra(), itineraryItem.getId());
            i.putExtra(Extra.CITY.getTypeExtra(), itineraryItem.getCity());
            i.putExtra(Extra.PLACES.getTypeExtra(), this.places);
            startActivity(i);
        });

        this.findViewById(R.id.fabUpdateItinerary).setOnClickListener(v -> {
            if(!Objects.requireNonNull(departureDateTextInputLayout.getEditText()).getText().toString().isEmpty()
                    && !Objects.requireNonNull(returnDateTextInputLayout.getEditText()).getText().toString().isEmpty()){

                if (InternetUtilities.getIsNetworkConnected()) {
                    updateItinerary(itineraryItem);
                } else {
                    InternetUtilities.getSnackbar().show();
                }
            }
        });

        Utility.setUpToolbar(this, this.itineraryItem.getName());
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    private String getCorrectFormatData(final String date){
        return date.replace("/",  "-");
    }

    private void getItineraryImages(final String itinerary_id, final VisitedItineraryImageSliderAdapter adapter){
        final String url = "https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("result").equals("success")) {
                            if(!jsonObject.getString("message").equals("nessuna foto")) {
                                final JSONArray jsonArray = new JSONArray(jsonObject.getString("values"));
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    final JSONObject image = (JSONObject) jsonArray.get(i);
                                    final String imageString = image.getString("image");
                                    final String id = image.getString("id");
                                    final ImageItem imageItem = new ImageItem(id, imageString);
                                    images.add(imageItem);
                                }
                            }
                            adapter.renewItems(images);
                        }else{
                            Utility.showNeutralAlert(getString(R.string.attenzione),jsonObject.getString("message"), VisitedItineraryActivity.this);
                        }
                    } catch (final JSONException e) {
                        Log.e(getString(R.string.error), e.toString());
                    }
                },
                error -> Log.e(getString(R.string.error), error.toString())){
            @Override
            protected Map<String,String> getParams(){
                final Map<String, String> params = new HashMap<>();
                params.put("type", "getVisitedItineraryImages");
                params.put("itinerary_id", itinerary_id);
                params.put("user_id", FileUtilities.getData( getCacheDir()+"/mtj.txt").get(UserManagement.ID.getId()));
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

    private void updateItinerary(final ItineraryItem itineraryItem){
        final String url = "https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("result").equals("success")) {
                            final Intent i = new Intent(getApplicationContext(), ItinerariesActivity.class);
                            i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "VisitedItinerary");
                            startActivity(i);
                        }else{
                            Utility.showNeutralAlert(getString(R.string.attenzione), jsonObject.getString("message"), VisitedItineraryActivity.this);
                        }
                    } catch (final JSONException e) {
                        Log.e(getString(R.string.error), e.toString());
                    }
                },
                error -> Log.e(getString(R.string.error), error.toString())){
            @Override
            protected Map<String,String> getParams(){
                final Map<String,String> params = new HashMap<>();
                params.put("type", "updateVisitedItinerary");
                params.put("itinerary_id", itineraryItem.getId());
                params.put("user_id", FileUtilities.getData( getCacheDir()+"/mtj.txt").get(UserManagement.ID.getId()));
                params.put("departureDate", itineraryItem.getDepartureDate());
                params.put("returnDate", itineraryItem.getReturnDate());
                params.put("places", itineraryItem.getAllPlaces());
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

    private void addItineraryImage(final String image, final VisitedItineraryImageSliderAdapter adapter){
        final String url = "https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("result").equals("success")) {
                            final String image_id = jsonObject.getString("image_id");
                            final String image1 = jsonObject.getString("image");
                            final ImageItem imageItem = new ImageItem(image_id, image1);
                            images.add(imageItem);
                            adapter.renewItems(images);
                            Utility.showToast("Immagine aggiunta con successo!", VisitedItineraryActivity.this);
                        } else {
                            Utility.showNeutralAlert(getString(R.string.attenzione), jsonObject.getString("message"), VisitedItineraryActivity.this);
                        }
                    } catch (final JSONException e) {
                        Log.e(getString(R.string.error), e.toString());
                    }
                },
                error -> Log.e(getString(R.string.error), error.toString())) {
            @Override
            protected Map<String, String> getParams() {
                final Map<String, String> params = new HashMap<>();
                params.put("type", "addVisitedItineraryImage");
                params.put("user_id", FileUtilities.getData( getCacheDir()+"/mtj.txt").get(UserManagement.ID.getId()));
                params.put("itinerary_id", itineraryItem.getId());
                params.put("image", image);
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }


}
