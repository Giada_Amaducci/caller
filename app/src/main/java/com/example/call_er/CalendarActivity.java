package com.example.call_er;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.call_er.Adapters.ListAdapter.CalendarListAdapter;
import com.example.call_er.Items.ImageItem;
import com.example.call_er.Items.ItineraryItem;
import com.example.call_er.Items.VisitedPlaceItem;
import com.example.call_er.Items.VisitedPlaceOrItineraryItem;
import com.example.call_er.Utilities.DateAndCalendarUtilities;
import com.example.call_er.Utilities.FileUtilities;
import com.example.call_er.Utilities.InternetUtilities;
import com.example.call_er.Utilities.LoadingDialog;
import com.example.call_er.Utilities.UserManagement;
import com.example.call_er.Utilities.Utility;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.TimeZone;

public class CalendarActivity extends AppCompatActivity {

    private SimpleDateFormat dateFormatMonth;
    private CompactCalendarView compactCalendar;
    private CalendarListAdapter listAdapter;
    private List<VisitedPlaceOrItineraryItem> items;
    private List<VisitedPlaceOrItineraryItem> currentItems;
    private LoadingDialog loadingDialog;

    @Override
    public void onBackPressed() {
        if(this.getClass().equals(CalendarActivity.class)){
            final Intent i = new Intent(getApplicationContext(), MainActivity.class);
            this.startActivity(i);
        }else{
            finish();
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_calendar);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.calendarConstraintLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI() {
        this.loadingDialog = new LoadingDialog(this);

        final ListView listView = this.findViewById(R.id.tripPlaceListView);
        this.listAdapter = new CalendarListAdapter(this ,this);
        listView.setAdapter(this.listAdapter);

        this.dateFormatMonth = new SimpleDateFormat("MMMM - yyyy", Locale.ITALY);
        this.items = new ArrayList<>();
        this.currentItems = new ArrayList<>();

        this.compactCalendar = this.findViewById(R.id.compactcalendar_view);
        this.compactCalendar.setUseThreeLetterAbbreviation(true);
        this.compactCalendar.setFirstDayOfWeek(Calendar.MONDAY);
        final Calendar calendar = new GregorianCalendar();
        final TimeZone timeZone = calendar.getTimeZone();
        this.compactCalendar.setLocale(timeZone, Locale.ITALY);

        final String user_id = FileUtilities.getData( getCacheDir()+"/mtj.txt").get(UserManagement.ID.getId());
        if (InternetUtilities.getIsNetworkConnected()) {
            this.loadingDialog.startLoadingDialog();
            this.getEvents(user_id);
        } else {
            InternetUtilities.getSnackbar().show();
        }

        this.compactCalendar.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                final List<Event> events = compactCalendar.getEvents(dateClicked);
                currentItems.clear();

                for(int i=0; i<events.size(); i++){
                    for(VisitedPlaceOrItineraryItem trip : items){
                        if(trip.isPlace()){
                            final VisitedPlaceItem visitedPlaceItem = trip.getVisitedPlaceItem();
                            if(visitedPlaceItem.getId().equals(events.get(i).getData())){
                                currentItems.add(trip);
                            }
                        }else{
                            final ItineraryItem itineraryItem = trip.getItineraryItem();
                            if(itineraryItem.getId().equals(events.get(i).getData())){
                                currentItems.add(trip);
                            }
                        }
                    }

                }
                listAdapter.setData(currentItems);

                final TextView dateName = findViewById(R.id.dateTextView);
                final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ITALY);
                dateFormat.applyPattern(("dd-MM-yyyy"));
                dateName.setText(dateFormat.format(dateClicked).replace("-", "/"));
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                Objects.requireNonNull(getSupportActionBar()).setTitle(firstCharToUpperCase(dateFormatMonth.format(firstDayOfNewMonth)));
            }
        });

        Utility.setUpToolbar(this, firstCharToUpperCase(this.dateFormatMonth.format(new Date())));
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    private String firstCharToUpperCase(final String str){
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }

    private void getEvents(final String user_id){
        final String url = "https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("result").equals("success")) {
                            if(jsonObject.has("visitedPlaces")){
                                final JSONArray jsonArray = new JSONArray(jsonObject.getString("visitedPlaces"));
                                for (int i = 0; i <jsonArray.length(); i++) {
                                    final JSONObject event = (JSONObject) jsonArray.get(i);
                                    Log.d("event", event.toString());
                                    final String id = event.getString("visited_place_id");
                                    final String name = event.getString("name");
                                    final String address = event.getString("address");
                                    final String road = event.getString("road");
                                    final String lat = event.getString("lat");
                                    final String lon = event.getString("lon");
                                    final String city = event.getString("city");
                                    final String date = event.getString("date");

                                    final VisitedPlaceItem visitedPlaceItem = new VisitedPlaceItem(id,name,address,city,road,false,lat,lon,date);
                                    if (event.has("image")) {
                                        final String image = event.getJSONObject("image").getString("image");
                                        final String image_id = event.getJSONObject("image").getString("image_id");
                                        visitedPlaceItem.setImage(new ImageItem(image_id, image));
                                    }

                                    final Event ev = new Event(ContextCompat.getColor(getApplicationContext(),R.color.colorPrimaryDark), DateAndCalendarUtilities.getMillisTime(date) , id);
                                    compactCalendar.addEvent(ev);

                                    final VisitedPlaceOrItineraryItem item = new VisitedPlaceOrItineraryItem(visitedPlaceItem);
                                    items.add(item);
                                }
                            }
                            if(jsonObject.has("itineraries")){
                                final JSONArray jsonArray = new JSONArray(jsonObject.getString("itineraries"));
                                for (int i = 0; i <jsonArray.length(); i++) {
                                    final JSONObject event = (JSONObject) jsonArray.get(i);
                                    final String id = event.getString("id");
                                    final String name = event.getString("name");
                                    final String city = event.getString("city");
                                    final String departure_date = event.getString("departure_date");
                                    final String return_date = event.getString("return_date");
                                    final String creator_name = event.getString("creator_name");

                                    final ItineraryItem itineraryItem = new ItineraryItem(id, name, city);
                                    itineraryItem.setCreatorName(creator_name);
                                    itineraryItem.setDepartureDate(departure_date);
                                    itineraryItem.setReturnDate(return_date);

                                    if (event.has("image")) {
                                        final String image = event.getJSONObject("image").getString("image");
                                        final String image_id = event.getJSONObject("image").getString("image_id");
                                        itineraryItem.setImage(new ImageItem(image_id, image));
                                    }

                                    final long departureDate = DateAndCalendarUtilities.getMillisTime(departure_date);
                                    final long returnDate = DateAndCalendarUtilities.getMillisTime(return_date);
                                    long days = DateAndCalendarUtilities.getDaysBetweenTwoDates(departureDate, returnDate)-1;
                                    String date = event.getString("departure_date");
                                    for (int j=0; j<days ; j++){
                                        date =  DateAndCalendarUtilities.addDay(date);
                                        final Event ev = new Event(ContextCompat.getColor(getApplicationContext(),R.color.colorPrimaryDark), DateAndCalendarUtilities.getMillisTime(date) ,id);
                                        compactCalendar.addEvent(ev);
                                    }
                                    Event ev = new Event(ContextCompat.getColor(getApplicationContext(),R.color.colorPrimaryDark), departureDate, id);
                                    compactCalendar.addEvent(ev);
                                    ev = new Event(ContextCompat.getColor(getApplicationContext(),R.color.colorPrimaryDark), returnDate, id);
                                    compactCalendar.addEvent(ev);

                                    final VisitedPlaceOrItineraryItem item = new VisitedPlaceOrItineraryItem(itineraryItem);
                                    items.add(item);
                                }
                            }
                        }else{
                            Utility.showNeutralAlert(getString(R.string.attenzione),jsonObject.getString("message"), CalendarActivity.this);
                        }
                        this.loadingDialog.dismissDialog();
                    } catch (final JSONException e) {
                        Log.e(getString(R.string.error), e.toString());
                    }
                },
                error -> Log.e(getString(R.string.error), error.toString())){
            @Override
            protected Map<String,String> getParams(){
                final Map<String, String> params = new HashMap<>();
                params.put("type", "getVisitedPlacesAndItineraries");
                params.put("user_id", user_id);
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

}
