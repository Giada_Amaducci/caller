package com.example.call_er;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.RadioGroup;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.call_er.Adapters.ListAdapter.PlacesInItineraryListAdapter;
import com.example.call_er.Items.ItineraryItem;
import com.example.call_er.Utilities.Extra;
import com.example.call_er.Utilities.FileUtilities;
import com.example.call_er.Utilities.InternetUtilities;
import com.example.call_er.Utilities.UserManagement;
import com.example.call_er.Utilities.Utility;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class AddItineraryOrganizerActivity extends AppCompatActivity {

    private String places;

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            this.onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if(this.getClass().equals(AddItineraryOrganizerActivity.class)){
            final AlertDialog dialog = new AlertDialog.Builder(this)
                    .setMessage(R.string.sicuro_di_non_creare)
                    .setCancelable(false)
                    .setPositiveButton(getString(R.string.si), (dialog1, which) -> {
                        final Intent i = new Intent(getApplicationContext(), ItinerariesActivity.class);
                        i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "AddItineraryOrganizerActivity");
                        startActivity(i);
                    })
                    .setNegativeButton(getString(R.string.no), (dialog12, which) -> dialog12.dismiss())
                    .create();
            dialog.show();
        }else{
            finish();
        }
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_add_itinerary_organizer);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.addItineraryOrganizerConstraintLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI(){
        final TextInputLayout nameField = this.findViewById(R.id.nameTextInputLayout);
        final String prevActivityName = this.getIntent().getStringExtra(Extra.ACTIVITY_NAME.getTypeExtra());
        final RadioGroup radioGroup = this.findViewById(R.id.searchRadioGroup);

        if(Objects.requireNonNull(prevActivityName).equals("AddPlaceInItinerary")){
            final String title = this.getIntent().getStringExtra(Extra.NAME.getTypeExtra());
            final String city = this.getIntent().getStringExtra(Extra.CITY.getTypeExtra());

            Objects.requireNonNull(nameField.getEditText()).setText(title);

            final ItineraryItem itineraryItem = new ItineraryItem("nessuno", title, city);
            itineraryItem.setCreatorName(FileUtilities.getData(getCacheDir()+"/mtj.txt").get(UserManagement.USERNAME.getId()));
            final ListView listView = this.findViewById(R.id.placesInItineraryListView);
            final PlacesInItineraryListAdapter listAdapter = new PlacesInItineraryListAdapter(this, this, itineraryItem, prevActivityName);
            listView.setAdapter(listAdapter);

            if(this.getIntent().getStringExtra(Extra.PLACES.getTypeExtra())!=null){
                this.places = getIntent().getStringExtra(Extra.PLACES.getTypeExtra());
                itineraryItem.setPlaces(places);
                if(!itineraryItem.getPlaces().isEmpty()){
                    listAdapter.setData(itineraryItem.getPlaces());
                }
            }else{
                this.places = "[]";
            }

            final int cityId;
            if (city != null) {
                if (city.equals("Cesena")) {
                    cityId = 0;
                }else{
                    cityId = 1;
                }
            }else{
                cityId = -1;
            }

            radioGroup.check(radioGroup.getChildAt(cityId).getId());

            for (int i = 0; i < radioGroup.getChildCount(); i++) {
                radioGroup.getChildAt(i).setEnabled(false);
            }

            Objects.requireNonNull(nameField.getEditText()).setText(title);
        }else{
            this.places = "[]";
        }

        this.findViewById(R.id.addPlaceInItineraryTextView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String title = Objects.requireNonNull(nameField.getEditText()).getText().toString();
                final String city;

                final int selectedRadio = radioGroup.getCheckedRadioButtonId();
                switch(selectedRadio) {
                    case R.id.cesenaRadioButton:
                        city = "Cesena";
                        break;
                    case R.id.forliRadioButton:
                        city = "Forlì";
                        break;
                    default:
                        city = "error";
                        break;
                }

                if (!title.equals("") && !city.equals("error")){
                    final ItineraryItem itineraryItem = new ItineraryItem("nessuno", title, city);
                    final Intent i = new Intent(getApplicationContext(), AddPlaceInItineraryActivity.class);
                    i.putExtra(Extra.NAME.getTypeExtra(), itineraryItem.getName());
                    i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "AddItineraryOrganizerActivity");
                    i.putExtra(Extra.CITY.getTypeExtra(), itineraryItem.getCity());
                    i.putExtra(Extra.PLACES.getTypeExtra(), places);
                    startActivity(i);
                }else{
                    Utility.showNeutralAlert(getString(R.string.attenzione),getString(R.string.dati_non_inseriti), AddItineraryOrganizerActivity.this);
                }
            }
        });

        this.findViewById(R.id.fabAddItineraryActivity).setOnClickListener(v -> {
            final String user_id = FileUtilities.getData(getCacheDir()+"/mtj.txt").get(UserManagement.ID.getId());

            if(!Objects.requireNonNull(nameField.getEditText()).getText().toString().isEmpty()){
                final String title = nameField.getEditText().getText().toString();
                final String city;

                final int selectedRadio = radioGroup.getCheckedRadioButtonId();
                switch(selectedRadio) {
                    case R.id.cesenaRadioButton:
                        city = "Cesena";
                        break;
                    case R.id.forliRadioButton:
                        city = "Forlì";
                        break;
                    default:
                        city = "error";
                        break;
                }

                if (!title.equals("") && !city.equals("error")) {
                    if (InternetUtilities.getIsNetworkConnected()) {
                        addItinerary(title, places, user_id, city);
                    } else {
                        InternetUtilities.getSnackbar().show();
                    }
                }else{
                    Utility.showNeutralAlert(getString(R.string.attenzione),getString(R.string.dati_inseriti_non_corretti), AddItineraryOrganizerActivity.this);
                }
            }
        });

        Utility.setUpToolbar(this, "Crea un Itineario");
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    private void addItinerary(final String title, final String places, final String user_id, final String city){
        final String url = "https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("result").equals("success")) {
                            final Intent i = new Intent(getApplicationContext(), ItinerariesActivity.class);
                            i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "AddItineraryActivity");
                            startActivity(i);
                        }else{
                            Utility.showNeutralAlert(getString(R.string.attenzione), jsonObject.getString("message"), AddItineraryOrganizerActivity.this);
                        }
                    } catch (final JSONException e) {
                        Log.e(getString(R.string.error), e.toString());
                    }
                },
                error -> Log.e(getString(R.string.error), error.toString())){
            @Override
            protected Map<String,String> getParams(){
                final Map<String,String> params = new HashMap<>();
                params.put("type", "addItineraryOrganizer");
                params.put("title", title);
                if(!places.equals("[]")){
                    params.put("places", places);
                }
                params.put("user_id", user_id);
                params.put("city", city);
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }
}
