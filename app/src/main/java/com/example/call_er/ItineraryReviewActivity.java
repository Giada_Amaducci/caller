package com.example.call_er;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageButton;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.call_er.Items.ItineraryItem;
import com.example.call_er.Utilities.Extra;
import com.example.call_er.Utilities.FileUtilities;
import com.example.call_er.Utilities.InternetUtilities;
import com.example.call_er.Utilities.LoadingDialog;
import com.example.call_er.Utilities.UserManagement;
import com.example.call_er.Utilities.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ItineraryReviewActivity extends AppCompatActivity {

    private List<AppCompatImageButton> imageButtons;
    private String prevActivity;
    private ItineraryItem itineraryItem;
    private int count;

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            this.onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if(this.getClass().equals(ItineraryReviewActivity.class)){
            final Intent i = new Intent(getApplicationContext(), VisitedItineraryActivity.class);
            i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), this.prevActivity);
            i.putExtra(Extra.NAME.getTypeExtra(), this.itineraryItem.getName());
            i.putExtra(Extra.DEPARTURE_DATE.getTypeExtra(), this.itineraryItem.getDepartureDate());
            i.putExtra(Extra.RETURN_DATE.getTypeExtra(), this.itineraryItem.getReturnDate());
            i.putExtra(Extra.CITY.getTypeExtra(), this.itineraryItem.getCity());
            i.putExtra(Extra.ID.getTypeExtra(), itineraryItem.getId());
            i.putExtra(Extra.CREATOR_NAME.getTypeExtra(), itineraryItem.getCreatorName());
            i.putExtra(Extra.PLACES.getTypeExtra(), this.itineraryItem.getAllPlaces());
            this.startActivity(i);
        }else{
            finish();
        }
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_itinerary_review);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.reviewConstraintLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI(){
        this.prevActivity = this.getIntent().getStringExtra(Extra.ACTIVITY_NAME.getTypeExtra());
        final String title = this.getIntent().getStringExtra(Extra.NAME.getTypeExtra());
        final String departureDate = this.getIntent().getStringExtra(Extra.DEPARTURE_DATE.getTypeExtra());
        final String returnDate = this.getIntent().getStringExtra(Extra.RETURN_DATE.getTypeExtra());
        final String creatorName = this.getIntent().getStringExtra(Extra.CREATOR_NAME.getTypeExtra());
        final String city = this.getIntent().getStringExtra(Extra.CITY.getTypeExtra());
        final String id = this.getIntent().getStringExtra(Extra.ID.getTypeExtra());
        final String places  = this.getIntent().getStringExtra(Extra.PLACES.getTypeExtra());

        this.itineraryItem  = new ItineraryItem(id, title, city);
        this.itineraryItem.setDepartureDate(departureDate);
        this.itineraryItem.setReturnDate(returnDate);
        this.itineraryItem.setCreatorName(creatorName);
        if(places != null){
            this.itineraryItem.setPlaces(places);
        }

        final LoadingDialog loadingDialog = new LoadingDialog(ItineraryReviewActivity.this);
        loadingDialog.startLoadingDialog();

        final TextView nameTextView = this.findViewById(R.id.itineraryNameTextView);
        final EditText commentVisitedPlaceEditText = this.findViewById(R.id.commentVisitedPlaceEditText);
        nameTextView.setText(title);

        final AppCompatImageButton oneStarButton = this.findViewById(R.id.starOneImageButton);
        final AppCompatImageButton twoStarButton = this.findViewById(R.id.starTwoImageButton);
        final AppCompatImageButton threeStarButton = this.findViewById(R.id.starThreeImageButton);
        final AppCompatImageButton fourStarButton = this.findViewById(R.id.starFourImageButton);
        final AppCompatImageButton fiveStarButton = this.findViewById(R.id.starFiveImageButton);
        this.imageButtons = new ArrayList<>();

        this.imageButtons.add(0, oneStarButton);
        this.imageButtons.add(1, twoStarButton);
        this.imageButtons.add(2, threeStarButton);
        this.imageButtons.add(3, fourStarButton);
        this.imageButtons.add(4, fiveStarButton);

        for(int i=0; i< this.imageButtons.size(); i++){
            final int index = i;
            this.imageButtons.get(i).setOnClickListener(view -> setClicked(index));
        }

        if (InternetUtilities.getIsNetworkConnected()) {
            getItineraryReview(commentVisitedPlaceEditText, loadingDialog);
        } else {
            InternetUtilities.getSnackbar().show();
        }

        this.findViewById(R.id.itineraryReviewFabAdd).setOnClickListener(v -> {
            if (InternetUtilities.getIsNetworkConnected()) {
                addOrUpdateItineraryReview(commentVisitedPlaceEditText);
            } else {
                InternetUtilities.getSnackbar().show();
            }
        });

        Utility.setUpToolbar(this, "La tua Recensione");
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    private void setClicked(final int position){
        if(position == -1){
            for(int i=0; i< this.imageButtons.size(); i++){
                this.imageButtons.get(i).setImageResource(R.drawable.baseline_star_border_24);
            }
            this.count = 0;
        }else{
            for(int i=0; i< this.imageButtons.size(); i++){
                if(i<=position){
                    this.imageButtons.get(i).setImageResource(R.drawable.baseline_star_24);
                }else{
                    this.imageButtons.get(i).setImageResource(R.drawable.baseline_star_border_24);
                }
            }
            this.count = position+1;
        }
    }

    private void getItineraryReview(final EditText comments, final LoadingDialog loadingDialog){
        final String url = "https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("result").equals("success")) {
                            int preference;
                            if(!jsonObject.getString("message").equals("nessuna recensione")) {
                                final JSONObject review = (JSONObject)jsonObject.get("itinerary");
                                final String note = review.getString("message");
                                comments.setText(note);
                                preference = review.getInt("vote");
                                setClicked(preference-1);
                            }else{
                                preference = -1;
                                setClicked(preference);
                            }
                        }else{
                            Utility.showNeutralAlert(getString(R.string.attenzione),jsonObject.getString("message"), ItineraryReviewActivity.this);
                        }
                        loadingDialog.dismissDialog();
                    } catch (final JSONException e) {
                        Log.e(getString(R.string.error), e.toString());
                    }
                },
                error -> Log.e(getString(R.string.error), error.toString())){
            @Override
            protected Map<String,String> getParams(){
                final Map<String, String> params = new HashMap<>();
                params.put("type", "getItineraryReview");
                params.put("itinerary_id", itineraryItem.getId());
                params.put("user_id", FileUtilities.getData( getCacheDir()+"/mtj.txt").get(UserManagement.ID.getId()));
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

    private void addOrUpdateItineraryReview(final EditText comment){
        final String url = "https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("result").equals("success")) {
                            Utility.showToast("Recensione lasciata con successo!", ItineraryReviewActivity.this);
                        } else {
                            Utility.showNeutralAlert(getString(R.string.attenzione), jsonObject.getString("message"), ItineraryReviewActivity.this);
                        }
                    } catch (final JSONException e) {
                        Log.e(getString(R.string.error), e.toString());
                    }
                },
                error -> Log.e(getString(R.string.error), error.toString())){
            @Override
            protected Map<String,String> getParams(){
                final Map<String, String> params = new HashMap<>();
                params.put("type", "addOrUpdateItineraryReview");
                params.put("itinerary_id", itineraryItem.getId());
                params.put("user_id", FileUtilities.getData( getCacheDir()+"/mtj.txt").get(UserManagement.ID.getId()));
                if(!comment.getText().toString().isEmpty()){
                    params.put("message", comment.getText().toString());
                }
                params.put("vote", String.valueOf(count));
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }
}
