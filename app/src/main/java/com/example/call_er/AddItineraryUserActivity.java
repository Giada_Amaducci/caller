package com.example.call_er;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.RadioGroup;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.call_er.Items.ItineraryItem;
import com.example.call_er.Utilities.DateAndCalendarUtilities;
import com.example.call_er.Utilities.Extra;
import com.example.call_er.Utilities.FileUtilities;
import com.example.call_er.Utilities.InternetUtilities;
import com.example.call_er.Utilities.UserManagement;
import com.example.call_er.Utilities.Utility;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class AddItineraryUserActivity extends AppCompatActivity {

    private String places;

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            this.onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if(this.getClass().equals(AddItineraryUserActivity.class)){
            final AlertDialog dialog = new AlertDialog.Builder(this)
                    .setMessage(R.string.sicuro_di_non_creare)
                    .setCancelable(false)
                    .setPositiveButton(getString(R.string.si), (dialog1, which) -> {
                        final Intent i = new Intent(getApplicationContext(), ItinerariesActivity.class);
                        i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "AddItineraryActivity");
                        startActivity(i);
                    })
                    .setNegativeButton(getString(R.string.no), (dialog12, which) -> dialog12.dismiss())
                    .create();
            dialog.show();
        }else{
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_add_itinerary_user);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.addItineraryConstraintLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI(){
        final TextInputLayout nameField = this.findViewById(R.id.nameTextInputLayout);
        final TextInputLayout departureDateTextInputLayout = this.findViewById(R.id.departureDateTextInputLayout);
        final TextInputLayout returnDateTextInputLayout = this.findViewById(R.id.returnDateTextInputLayout);
        final String prevActivityName = this.getIntent().getStringExtra(Extra.ACTIVITY_NAME.getTypeExtra());
        final RadioGroup radioGroup = this.findViewById(R.id.searchRadioGroup);

        departureDateTextInputLayout.setEndIconOnClickListener(v -> DateAndCalendarUtilities.showDatePickerDialog(AddItineraryUserActivity.this, departureDateTextInputLayout));

        returnDateTextInputLayout.setEndIconOnClickListener(v -> DateAndCalendarUtilities.showDatePickerDialog(AddItineraryUserActivity.this, returnDateTextInputLayout));

        if(Objects.requireNonNull(prevActivityName).equals("PlacesInItinerary")){
            final String title = this.getIntent().getStringExtra(Extra.NAME.getTypeExtra());
            final String departureDate = this.getIntent().getStringExtra(Extra.DEPARTURE_DATE.getTypeExtra());
            final String returnDate = this.getIntent().getStringExtra(Extra.RETURN_DATE.getTypeExtra());
            final String city = this.getIntent().getStringExtra(Extra.CITY.getTypeExtra());

            if(getIntent().getStringExtra(Extra.PLACES.getTypeExtra())!=null){
                this.places = getIntent().getStringExtra(Extra.PLACES.getTypeExtra());
            }else{
                this.places = "[]";
            }

            final int cityId;
            if (city != null) {
                if (city.equals("Cesena")) {
                    cityId = 0;
                }else{
                    cityId = 1;
                }
            }else{
                cityId = -1;
            }

            radioGroup.check(radioGroup.getChildAt(cityId).getId());

            for (int i = 0; i < radioGroup.getChildCount(); i++) {
                radioGroup.getChildAt(i).setEnabled(false);
            }

            Objects.requireNonNull(nameField.getEditText()).setText(title);
            Objects.requireNonNull(departureDateTextInputLayout.getEditText()).setText(departureDate);
            Objects.requireNonNull(returnDateTextInputLayout.getEditText()).setText(returnDate);
        }else{
            this.places = "[]";
        }

        this.findViewById(R.id.placesListButton).setOnClickListener(v -> {
            final String title = Objects.requireNonNull(nameField.getEditText()).getText().toString();
            final String departureDate = Objects.requireNonNull(departureDateTextInputLayout.getEditText()).getText().toString();
            final String returnDate = Objects.requireNonNull(returnDateTextInputLayout.getEditText()).getText().toString();
            final String city;

            final int selectedRadio = radioGroup.getCheckedRadioButtonId();
            switch(selectedRadio) {
                case R.id.cesenaRadioButton:
                    city = "Cesena";
                    break;
                case R.id.forliRadioButton:
                    city = "Forlì";
                    break;
                default:
                    city = "error";
                    break;
            }

            if (!title.equals("") && !departureDate.equals("") && !returnDate.equals("")){
                if(DateAndCalendarUtilities.checkDataInput(departureDate)
                        && DateAndCalendarUtilities.checkDataInput(returnDate)
                        && DateAndCalendarUtilities.checkDates(departureDate, returnDate) && !city.equals("error")){

                    final ItineraryItem tripItem = new ItineraryItem("nessuno", title, city);
                    tripItem.setDepartureDate(departureDate);
                    tripItem.setReturnDate(returnDate);
                    final Intent i = new Intent(getApplicationContext(), PlacesInItineraryActivity.class);
                    i.putExtra(Extra.NAME.getTypeExtra(), tripItem.getName());
                    i.putExtra(Extra.DEPARTURE_DATE.getTypeExtra(), tripItem.getDepartureDate());
                    i.putExtra(Extra.RETURN_DATE.getTypeExtra(), tripItem.getReturnDate());
                    i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "AddItineraryActivity");
                    i.putExtra(Extra.CITY.getTypeExtra(), tripItem.getCity());
                    i.putExtra(Extra.PLACES.getTypeExtra(), places);
                    startActivity(i);
                }else{
                    Utility.showNeutralAlert(getString(R.string.attenzione),getString(R.string.date_non_corrette), AddItineraryUserActivity.this);
                }
            }else{
                Utility.showNeutralAlert(getString(R.string.attenzione),getString(R.string.dati_non_inseriti), AddItineraryUserActivity.this);
            }
        });

        this.findViewById(R.id.fabAddItineraryActivity).setOnClickListener(v -> {
            final String user_id = FileUtilities.getData(getCacheDir()+"/mtj.txt").get(UserManagement.ID.getId());

            if(!Objects.requireNonNull(nameField.getEditText()).getText().toString().isEmpty()
                    && !Objects.requireNonNull(departureDateTextInputLayout.getEditText()).getText().toString().isEmpty()
                    && !Objects.requireNonNull(returnDateTextInputLayout.getEditText()).getText().toString().isEmpty()){
                final String title = nameField.getEditText().getText().toString();
                final String departureDate = departureDateTextInputLayout.getEditText().getText().toString();
                final String returnDate = returnDateTextInputLayout.getEditText().getText().toString();
                final String city;

                final int selectedRadio = radioGroup.getCheckedRadioButtonId();
                switch(selectedRadio) {
                    case R.id.cesenaRadioButton:
                        city = "Cesena";
                        break;
                    case R.id.forliRadioButton:
                        city = "Forlì";
                        break;
                    default:
                        city = "error";
                        break;
                }

                if (!returnDate.equals("") && !title.equals("") && !departureDate.equals("")
                        && DateAndCalendarUtilities.checkDataInput(returnDate)
                        && DateAndCalendarUtilities.checkDataInput(departureDate)
                        && DateAndCalendarUtilities.checkDates(departureDate, returnDate)) {
                    if (InternetUtilities.getIsNetworkConnected()) {
                        addItinerary(title, getCorrectFormatData(departureDate), getCorrectFormatData(returnDate), places, user_id, city);
                    } else {
                        InternetUtilities.getSnackbar().show();
                    }

                }else{
                    Utility.showNeutralAlert(getString(R.string.attenzione),getString(R.string.dati_inseriti_non_corretti), AddItineraryUserActivity.this);
                }
            }
        });

        Utility.setUpToolbar(this, "Crea un Itineario");
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    private String getCorrectFormatData(final String date){
        return date.replace("/",  "-");
    }

    private void addItinerary(final String title, final String departureDate, final String returnDate, final String places, final String user_id, final String city){
        final String url = "https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("result").equals("success")) {
                            final Intent i = new Intent(getApplicationContext(), ItinerariesActivity.class);
                            Utility.showToast("Itinerario aggiunto con successo!", this);
                            i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "AddItineraryActivity");
                            startActivity(i);
                        }else{
                            Utility.showNeutralAlert(getString(R.string.attenzione), jsonObject.getString("message"), AddItineraryUserActivity.this);
                        }
                    } catch (final JSONException e) {
                        Log.e(getString(R.string.error), e.toString());
                    }
                },
                error -> Log.e(getString(R.string.error), error.toString())){
            @Override
            protected Map<String,String> getParams(){
                final Map<String,String> params = new HashMap<>();
                params.put("type", "addItineraryUser");
                params.put("title", title);
                params.put("departureDate", departureDate);
                params.put("returnDate", returnDate);
                if(!places.equals("[]")){
                    params.put("places", places);
                }
                params.put("user_id", user_id);
                params.put("city", city);
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }
}
