package com.example.call_er;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.RadioGroup;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.call_er.Adapters.ListAdapter.PlacesInItineraryListAdapter;
import com.example.call_er.Items.ItineraryItem;
import com.example.call_er.Items.ItineraryPlaceItem;
import com.example.call_er.Items.PlaceItem;
import com.example.call_er.Utilities.DateAndCalendarUtilities;
import com.example.call_er.Utilities.Extra;
import com.example.call_er.Utilities.FileUtilities;
import com.example.call_er.Utilities.InternetUtilities;
import com.example.call_er.Utilities.UserManagement;
import com.example.call_er.Utilities.Utility;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ShareItineraryActivity extends AppCompatActivity {

    private ItineraryItem itineraryItem;

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            this.onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if(this.getClass().equals(ShareItineraryActivity.class)){
            final Intent i = new Intent(getApplicationContext(), PublicItineraryActivity.class);
            i.putExtra(Extra.NAME.getTypeExtra(), itineraryItem.getName());
            i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "ShareItineraryActivity");
            i.putExtra(Extra.CREATOR_NAME.getTypeExtra(), itineraryItem.getCreatorName());
            i.putExtra(Extra.ID.getTypeExtra(), itineraryItem.getId());
            i.putExtra(Extra.CITY.getTypeExtra(), itineraryItem.getCity());
            i.putExtra(Extra.AVG_VOTE.getTypeExtra(), itineraryItem.getAvg());
            i.putExtra(Extra.USER_COUNT.getTypeExtra(), itineraryItem.getVotes());
            this.startActivity(i);
        }else{
            finish();
        }
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_share_itinerary);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.shareItineraryConstraintLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI(){
        final String name = this.getIntent().getStringExtra(Extra.NAME.getTypeExtra());
        final String city = this.getIntent().getStringExtra(Extra.CITY.getTypeExtra());
        final String id = this.getIntent().getStringExtra(Extra.ID.getTypeExtra());
        final String creator_name = this.getIntent().getStringExtra(Extra.CREATOR_NAME.getTypeExtra());
        final String avg = this.getIntent().getStringExtra(Extra.AVG_VOTE.getTypeExtra());
        final String count = this.getIntent().getStringExtra(Extra.USER_COUNT.getTypeExtra());

        this.itineraryItem = new ItineraryItem(id, name, city);
        this.itineraryItem.setCreatorName(creator_name);
        this.itineraryItem.setAvg(avg);
        this.itineraryItem.setVotes(count);

        final RadioGroup radioGroup = this.findViewById(R.id.searchRadioGroup);
        final int cityId;
        if (city != null) {
            if (city.equals("Cesena")) {
                cityId = 0;
            }else{
                cityId = 1;
            }
        }else{
            cityId = -1;
        }

        radioGroup.check(radioGroup.getChildAt(cityId).getId());

        for (int i = 0; i < radioGroup.getChildCount(); i++) {
            radioGroup.getChildAt(i).setEnabled(false);
        }

        final TextInputLayout departureDateTextInputLayout = this.findViewById(R.id.departureDateTextInputLayout);
        final TextInputLayout returnDateTextInputLayout = this.findViewById(R.id.returnDateTextInputLayout);

        departureDateTextInputLayout.setEndIconOnClickListener(v -> DateAndCalendarUtilities.showDatePickerDialog(ShareItineraryActivity.this, departureDateTextInputLayout));

        returnDateTextInputLayout.setEndIconOnClickListener(v -> DateAndCalendarUtilities.showDatePickerDialog(ShareItineraryActivity.this, returnDateTextInputLayout));

        final ListView listView = this.findViewById(R.id.placesInItineraryListView);
        final PlacesInItineraryListAdapter listAdapter = new PlacesInItineraryListAdapter(this, this, itineraryItem, "PublicItineraryActivity");
        listView.setAdapter(listAdapter);

        if (InternetUtilities.getIsNetworkConnected()) {
            getPlacesInItinerary(this.itineraryItem.getId(), listAdapter);
        } else {
            InternetUtilities.getSnackbar().show();
        }

        this.findViewById(R.id.fabAddItineraryActivity).setOnClickListener(v -> {
            final String user_id = FileUtilities.getData(getCacheDir()+"/mtj.txt").get(UserManagement.ID.getId());

            if(!Objects.requireNonNull(departureDateTextInputLayout.getEditText()).getText().toString().isEmpty()
                    && !Objects.requireNonNull(returnDateTextInputLayout.getEditText()).getText().toString().isEmpty()){
                final String departureDate = departureDateTextInputLayout.getEditText().getText().toString();
                final String returnDate = returnDateTextInputLayout.getEditText().getText().toString();

                if (!returnDate.equals("") && !departureDate.equals("")
                        && DateAndCalendarUtilities.checkDataInput(returnDate)
                        && DateAndCalendarUtilities.checkDataInput(departureDate)
                        && DateAndCalendarUtilities.checkDates(departureDate, returnDate)) {
                    if (InternetUtilities.getIsNetworkConnected()) {
                        addPublicItineraryUser(itineraryItem.getId(), getCorrectFormatData(departureDate), getCorrectFormatData(returnDate), user_id);
                    } else {
                        InternetUtilities.getSnackbar().show();
                    }

                }else{
                    Utility.showNeutralAlert(getString(R.string.attenzione),getString(R.string.dati_inseriti_non_corretti), ShareItineraryActivity.this);
                }
            }else{
                Utility.showNeutralAlert(getString(R.string.attenzione),getString(R.string.dati_inseriti_non_corretti), ShareItineraryActivity.this);
            }
        });

        Utility.setUpToolbar(this, this.itineraryItem.getName());
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    private String getCorrectFormatData(final String date){
        return date.replace("/",  "-");
    }

    private void addPublicItineraryUser(final String itinerary_id, final String departureDate, final String returnDate, final String user_id){
        final String url = "https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("result").equals("success")) {
                            final Intent i = new Intent(getApplicationContext(), ItinerariesActivity.class);
                            i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "ShareItineraryActivity");
                            startActivity(i);
                        }else{
                            Utility.showNeutralAlert(getString(R.string.attenzione), jsonObject.getString("message"), ShareItineraryActivity.this);
                        }
                    } catch (final JSONException e) {
                        Log.e(getString(R.string.error), e.toString());
                    }
                },
                error -> Log.e(getString(R.string.error), error.toString())){
            @Override
            protected Map<String,String> getParams(){
                final Map<String,String> params = new HashMap<>();
                params.put("type", "addPublicItineraryUser");
                params.put("itinerary_id", itinerary_id);
                params.put("departure_date", departureDate);
                params.put("return_date", returnDate);
                params.put("user_id", user_id);
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }

    private void getPlacesInItinerary(final String itinerary_id, final PlacesInItineraryListAdapter adapter){
        final String url = "https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("result").equals("success")) {
                            final List<ItineraryPlaceItem> placesList = new ArrayList<>();
                            if(!jsonObject.getString("message").equals("nessun luogo")) {
                                final JSONArray jsonArray = new JSONArray(jsonObject.getString("values"));
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    final JSONObject placeInItinerary = (JSONObject) jsonArray.get(i);
                                    final String id = placeInItinerary.getString("place_in_itinerary_id");
                                    final String name = placeInItinerary.getString("place_name");
                                    final String address = placeInItinerary.getString("address");
                                    final String city = placeInItinerary.getString("city");
                                    final String road = placeInItinerary.getString("road");
                                    final String lat = placeInItinerary.getString("lat");
                                    final String lon = placeInItinerary.getString("lon");
                                    final PlaceItem placeItem = new PlaceItem(name, address, city, road, lon, lat);
                                    final ItineraryPlaceItem visitedPlaceItem = new ItineraryPlaceItem(id, placeItem);
                                    placesList.add(visitedPlaceItem);
                                }
                            }
                            itineraryItem.setPlaces(placesList);
                            adapter.setData(placesList);
                        }else{
                            Utility.showNeutralAlert(getString(R.string.attenzione),jsonObject.getString("message"), ShareItineraryActivity.this);
                        }
                    } catch (final JSONException e) {
                        Log.e(getString(R.string.error), e.toString());
                    }
                },
                error -> Log.e(getString(R.string.error), error.toString())){
            @Override
            protected Map<String,String> getParams(){
                final Map<String, String> params = new HashMap<>();
                params.put("type", "getPlacesInItinerary");
                params.put("itinerary_id", itinerary_id);
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }
}
