package com.example.call_er;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.toolbox.Volley;
import com.example.call_er.Items.PlaceItem;
import com.example.call_er.Utilities.Extra;
import com.example.call_er.Utilities.GpsUtilities;
import com.example.call_er.Utilities.InternetUtilities;
import com.example.call_er.Utilities.PermissionsUtilities;
import com.example.call_er.Utilities.Utility;
import com.mapbox.api.directions.v5.DirectionsCriteria;
import com.mapbox.api.directions.v5.MapboxDirections;
import com.mapbox.api.directions.v5.models.DirectionsResponse;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import com.mapbox.api.directions.v5.models.LegStep;
import com.mapbox.core.constants.Constants;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.LineString;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.MapboxMapOptions;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.maps.SupportMapFragment;
import com.mapbox.mapboxsdk.style.layers.LineLayer;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.mapboxsdk.utils.BitmapUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mapbox.api.directions.v5.DirectionsCriteria.GEOMETRY_POLYLINE;
import static com.mapbox.mapboxsdk.style.layers.Property.LINE_CAP_ROUND;
import static com.mapbox.mapboxsdk.style.layers.Property.LINE_JOIN_ROUND;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAllowOverlap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconIgnorePlacement;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconImage;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconSize;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineCap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineJoin;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineOpacity;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineWidth;

public class PlaceMapActivity extends AppCompatActivity {

    private static final String SYMBOL_ICON_ID = "SYMBOL_ICON_ID";
    private static final String PERSON_ICON_ID = "PERSON_ICON_ID";
    private static final String PERSON_SOURCE_ID = "PERSON_SOURCE_ID";
    private static final String PERSON_LAYER_ID = "PERSON_LAYER_ID";
    private static final String MARKER_SOURCE_ID = "MARKER_SOURCE_ID";
    private static final String LAYER_ID = "LAYER_ID";
    private static final float NAVIGATION_LINE_WIDTH = 6;
    private static final float NAVIGATION_LINE_OPACITY = .8f;
    private static final String DRIVING_ROUTE_POLYLINE_LINE_LAYER_ID = "DRIVING_ROUTE_POLYLINE_LINE_LAYER_ID";
    private static final String DRIVING_ROUTE_POLYLINE_SOURCE_ID = "DRIVING_ROUTE_POLYLINE_SOURCE_ID";
    private static final int DRAW_SPEED_MILLISECONDS = 500;

    private Bundle savedInstanceState;
    private MapboxMap mapboxMap;
    private MapboxDirections mapboxDirectionsClient;
    private Handler handler = new Handler();
    private Runnable runnable;
    private PlaceItem placeItem;
    private String placeType;
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            if (intent.getAction() != null && intent.getAction().equals("SEND_DATA")) {
                final String lat = intent.getStringExtra("location_latitude");
                final String lon = intent.getStringExtra("location_longitude");

                if (!InternetUtilities.getIsNetworkConnected()) {
                    InternetUtilities.getSnackbar().show();
                } else{
                    setMap(lat, lon);
                }
                GpsUtilities.removeLocationUpdates(PlaceMapActivity.this, broadcastReceiver);
            }
        }
    };

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            this.onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if(this.getClass().equals(PlaceMapActivity.class)){
            final Intent i = new Intent(getApplicationContext(), PlaceActivity.class);
            i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "PlaceMapActivity");
            i.putExtra(Extra.NAME.getTypeExtra(), this.placeItem.getName());
            i.putExtra(Extra.ROAD.getTypeExtra(), this.placeItem.getRoad());
            i.putExtra(Extra.ADDRESS.getTypeExtra(), this.placeItem.getAddress());
            i.putExtra(Extra.LAT.getTypeExtra(), this.placeItem.getLat());
            i.putExtra(Extra.LON.getTypeExtra(), this.placeItem.getLon());
            i.putExtra(Extra.CITY.getTypeExtra(), this.placeItem.getCity());
            i.putExtra(Extra.TYPE.getTypeExtra(), this.placeType);
            this.startActivity(i);
        }else{
            finish();
        }
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == PermissionsUtilities.REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length > 0) {
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    PermissionsUtilities.createSnackBar(this);
                }
            }
        }
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_place_map);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.savedInstanceState = savedInstanceState;
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.mapLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI() {
        this.placeType = this.getIntent().getStringExtra(Extra.TYPE.getTypeExtra());
        final String name = this.getIntent().getStringExtra(Extra.NAME.getTypeExtra());
        final String address = this.getIntent().getStringExtra(Extra.ADDRESS.getTypeExtra());
        final String road = this.getIntent().getStringExtra(Extra.ROAD.getTypeExtra());
        final String city = this.getIntent().getStringExtra(Extra.CITY.getTypeExtra());
        final String lat = this.getIntent().getStringExtra(Extra.LAT.getTypeExtra());
        final String lon = this.getIntent().getStringExtra(Extra.LON.getTypeExtra());
        this.placeItem = new PlaceItem(name,address,city,road,lon,lat);

        Mapbox.getInstance(this, getString(R.string.mapbox_access_token));

        if (!PermissionsUtilities.checkPermissions(this)) {
            PermissionsUtilities.requestPermissions(this);
        }

        if (InternetUtilities.getIsNetworkConnected()) {
            GpsUtilities.setLocation(this);
            GpsUtilities.requestLocation(this, broadcastReceiver);
        } else {
            InternetUtilities.getSnackbar().show();
        }

        Utility.setUpToolbar(this, "Come arrivarci");
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    private void setMap(final String lat, final String lon){
        final SupportMapFragment mapFragment;
        if (this.savedInstanceState == null) {
            final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            final MapboxMapOptions options = MapboxMapOptions.createFromAttributes(this, null);
            options.camera(new CameraPosition.Builder()
                    .target(new LatLng(Double.parseDouble(lat), Double.parseDouble(lon)))
                    .zoom(12)
                    .build());

            mapFragment = SupportMapFragment.newInstance(options);

            transaction.add(R.id.container, mapFragment, "com.mapbox.map");
            transaction.commit();
        } else {
            mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentByTag("com.mapbox.map");
        }
        if (mapFragment != null) {
            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(@NonNull MapboxMap mapMap) {

                    final Point directionsOriginPoint = Point.fromLngLat(Double.parseDouble(lon), Double.parseDouble(lat));
                    Log.d("Lon",lon);
                    final Point placePoint = Point.fromLngLat(Double.parseDouble(placeItem.getLon()), Double.parseDouble(placeItem.getLat()));

                    mapboxMap = mapMap;

                    final Drawable drawablePerson = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_person_24dp, null);
                    final Bitmap personBitmap = BitmapUtils.getBitmapFromDrawable(drawablePerson);
                    final Drawable drawablePlace = ResourcesCompat.getDrawable(getResources(), R.drawable.baseline_location_on_24, null);
                    final Bitmap placeBitmap = BitmapUtils.getBitmapFromDrawable(drawablePlace);

                    mapboxMap.setStyle(new Style.Builder().fromUri(Style.OUTDOORS)
                            .withImage(PERSON_ICON_ID, personBitmap)
                            .withSource(new GeoJsonSource(PERSON_SOURCE_ID,
                                    Feature.fromGeometry(directionsOriginPoint)))
                            .withLayer(new SymbolLayer(PERSON_LAYER_ID, PERSON_SOURCE_ID).withProperties(
                                    iconImage(PERSON_ICON_ID),
                                    iconSize(2f),
                                    iconAllowOverlap(true),
                                    iconIgnorePlacement(true)
                            ))

                            .withImage(SYMBOL_ICON_ID, placeBitmap)
                            .withSource(new GeoJsonSource(MARKER_SOURCE_ID,
                                    Feature.fromGeometry(placePoint)))
                            .withLayer(new SymbolLayer(LAYER_ID, MARKER_SOURCE_ID).withProperties(
                                    iconImage(SYMBOL_ICON_ID),
                                    iconAllowOverlap(true),
                                    iconIgnorePlacement(true),
                                    iconSize(2f)
                            ))

                            .withSource(new GeoJsonSource(DRIVING_ROUTE_POLYLINE_SOURCE_ID))
                            .withLayerBelow(new LineLayer(DRIVING_ROUTE_POLYLINE_LINE_LAYER_ID,
                                    DRIVING_ROUTE_POLYLINE_SOURCE_ID)
                                    .withProperties(
                                            lineWidth(NAVIGATION_LINE_WIDTH),
                                            lineOpacity(NAVIGATION_LINE_OPACITY),
                                            lineCap(LINE_CAP_ROUND),
                                            lineJoin(LINE_JOIN_ROUND),
                                            lineColor(Color.parseColor("#00675B"))
                                    ), "layer-id"), style -> getDirectionsRoute(directionsOriginPoint, placePoint));
                }
            });
        }
    }

    private void getDirectionsRoute(Point origin, Point destination) {
        mapboxDirectionsClient = MapboxDirections.builder()
                .origin(origin)
                .destination(destination)
                .overview(DirectionsCriteria.OVERVIEW_FULL)
                .profile(DirectionsCriteria.PROFILE_DRIVING)
                .geometries(GEOMETRY_POLYLINE)
                .alternatives(true)
                .steps(true)
                .accessToken(getString(R.string.mapbox_access_token))
                .build();

        mapboxDirectionsClient.enqueueCall(new Callback<DirectionsResponse>() {
            @Override
            public void onResponse(@NonNull Call<DirectionsResponse> call, @NonNull Response<DirectionsResponse> response) {
                // Create log messages in case no response or routes are present
                if (response.body() == null) {
                    Log.e("Error","No routes found, make sure you set the right user and access token.");
                    return;
                } else if (response.body().routes().size() < 1) {
                    Log.e("Error", "No routes found");
                    return;
                }

                DirectionsRoute currentRoute = response.body().routes().get(0);

                runnable = new DrawRouteRunnable(mapboxMap, currentRoute.legs().get(0).steps(), handler);
                handler.postDelayed(runnable, DRAW_SPEED_MILLISECONDS);
            }

            @Override
            public void onFailure(@NonNull Call<DirectionsResponse> call, @NonNull Throwable throwable) {
                Toast.makeText(PlaceMapActivity.this,
                        "Errore nella creazione del percorso", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Runnable class which goes through the route and draws each {@link LegStep} of the Directions API route
     */
    private static class DrawRouteRunnable implements Runnable {
        private MapboxMap mapboxMap;
        private List<LegStep> steps;
        private List<Feature> drivingRoutePolyLineFeatureList;
        private Handler handler;
        private int counterIndex;

        DrawRouteRunnable(MapboxMap mapboxMap, List<LegStep> steps, Handler handler) {
            this.mapboxMap = mapboxMap;
            this.steps = steps;
            this.handler = handler;
            this.counterIndex = 0;
            drivingRoutePolyLineFeatureList = new ArrayList<>();
        }

        @Override
        public void run() {
            if (counterIndex < steps.size()) {
                LegStep singleStep = steps.get(counterIndex);
                if (singleStep != null && singleStep.geometry() != null) {
                    LineString lineStringRepresentingSingleStep = LineString.fromPolyline(
                            singleStep.geometry(), Constants.PRECISION_5);
                    Feature featureLineString = Feature.fromGeometry(lineStringRepresentingSingleStep);
                    drivingRoutePolyLineFeatureList.add(featureLineString);
                }
                if (mapboxMap.getStyle() != null) {
                    GeoJsonSource source = mapboxMap.getStyle().getSourceAs(DRIVING_ROUTE_POLYLINE_SOURCE_ID);
                    if (source != null) {
                        source.setGeoJson(FeatureCollection.fromFeatures(drivingRoutePolyLineFeatureList));
                    }
                }
                counterIndex++;
                handler.postDelayed(this, DRAW_SPEED_MILLISECONDS);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (handler != null) {
            handler.removeCallbacks(runnable);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mapboxDirectionsClient != null) {
            mapboxDirectionsClient.cancelCall();
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }
}