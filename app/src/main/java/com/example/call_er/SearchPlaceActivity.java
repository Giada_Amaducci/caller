package com.example.call_er;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioGroup;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.toolbox.Volley;
import com.example.call_er.Adapters.ListAdapter.SearchPlaceListAdapter;
import com.example.call_er.Items.PlaceItem;
import com.example.call_er.Utilities.Extra;
import com.example.call_er.Utilities.InternetUtilities;
import com.example.call_er.Utilities.LoadingDialog;
import com.example.call_er.Utilities.OsmUtilities;
import com.example.call_er.Utilities.Utility;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SearchPlaceActivity extends AppCompatActivity {

    private List<PlaceItem> placeItems;
    private SearchPlaceListAdapter listAdapter;
    private RadioGroup radioGroup;
    private PlaceItem selectedPlace;
    private String activityName;
    private int selectedRadio;
    private LoadingDialog loadingDialog;

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            this.onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if(this.getClass().equals(SearchPlaceActivity.class)) {
            final Intent i;
            if (this.activityName.equals("VisitedPlacesActivity")) {
                i = new Intent(getApplicationContext(), VisitedPlacesActivity.class);
            } else {
                i = new Intent(getApplicationContext(), MainActivity.class);
            }
            startActivity(i);
        }else{
            finish();
        }
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_search_place);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.searchLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI(){
        this.activityName = this.getIntent().getStringExtra(Extra.ACTIVITY_NAME.getTypeExtra());
        this.loadingDialog = new LoadingDialog(this);

        final ListView listView = this.findViewById(R.id.searchedPlaceListView);
        final TextInputLayout searchInputLayout = findViewById(R.id.searchOutlinedTextField);

        this.radioGroup = this.findViewById(R.id.searchRadioGroup);
        this.placeItems = new ArrayList<>();

        this.listAdapter = new SearchPlaceListAdapter(this);
        listView.setAdapter(listAdapter);

        listView.setOnItemClickListener((adapterView, view, position, id) -> {
            selectedPlace = placeItems.get(position);
            final Intent i = new Intent(getApplicationContext(), PlaceActivity.class);
            i.putExtra(Extra.NAME.getTypeExtra(), selectedPlace.getName());
            i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "SearchPlaceActivity");
            i.putExtra(Extra.ADDRESS.getTypeExtra(), selectedPlace.getAddress());
            i.putExtra(Extra.ROAD.getTypeExtra(), selectedPlace.getRoad());
            i.putExtra(Extra.CITY.getTypeExtra(), selectedPlace.getCity());
            i.putExtra(Extra.LAT.getTypeExtra(), selectedPlace.getLat());
            i.putExtra(Extra.LON.getTypeExtra(), selectedPlace.getLon());
            startActivity(i);
        });

        this.findViewById(R.id.searchImageButton).setOnClickListener(v -> {
            final String place = Objects.requireNonNull(searchInputLayout.getEditText()).getText().toString();
            this.loadingDialog.startLoadingDialog();

            placeItems.clear();
            selectedRadio = radioGroup.getCheckedRadioButtonId();
            switch(selectedRadio) {
                case -1:
                    Utility.showNeutralAlert(getString(R.string.attenzione),"Nessuna città selezionata!", SearchPlaceActivity.this);
                    loadingDialog.dismissDialog();
                    break;
                case R.id.cesenaRadioButton:
                    if (InternetUtilities.getIsNetworkConnected()) {
                        OsmUtilities.searchAmenity(place, placeItems, SearchPlaceActivity.this, listAdapter, "Cesena", loadingDialog);
                    } else {
                        InternetUtilities.getSnackbar().show();
                    }
                    break;
                case R.id.forliRadioButton:
                    if (InternetUtilities.getIsNetworkConnected()) {
                        OsmUtilities.searchAmenity(place, placeItems, SearchPlaceActivity.this, listAdapter, "Forlì", loadingDialog);
                    } else {
                        InternetUtilities.getSnackbar().show();
                    }
                    break;
            }
        });

        Utility.setUpToolbar(this, "Ricerca Luoghi");
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }
}
