package com.example.call_er.Holders;

import android.view.View;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.call_er.R;

public class LocationViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private final TextView name;
    private final TextView address;
    private final CardView singleCard;
    private ItemClickListener clickListener;

    public LocationViewHolder(final View view) {
        super(view);
        this.name = view.findViewById(R.id.location_title_tv);
        address = view.findViewById(R.id.location_num_of_beds_tv);
        singleCard = view.findViewById(R.id.single_location_cardview);
        singleCard.setOnClickListener(this);
    }

    public ItemClickListener getClickListener() {
        return clickListener;
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    @Override
    public void onClick(View view) {
        clickListener.onClick(view, getLayoutPosition());
    }

    public TextView getName() {
        return name;
    }

    public TextView getAddress() {
        return address;
    }
}
