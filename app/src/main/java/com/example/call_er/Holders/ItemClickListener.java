package com.example.call_er.Holders;

import android.view.View;

public interface ItemClickListener {
    void onClick(View view, int position);
}
