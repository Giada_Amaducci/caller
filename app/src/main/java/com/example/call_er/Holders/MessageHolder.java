package com.example.call_er.Holders;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.call_er.R;

public class MessageHolder extends RecyclerView.ViewHolder {

    private  TextView messageText;
    private TextView timeText;

    public MessageHolder(View itemView) {
        super(itemView);
        messageText = (TextView) itemView.findViewById(R.id.textMessageBody);
        timeText = (TextView) itemView.findViewById(R.id.textMessageTime);
    }

    public TextView getMessageText() {
        return messageText;
    }

    public TextView getTimeText() {
        return timeText;
    }
}
