package com.example.call_er.Holders;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.example.call_er.R;
import com.smarteist.autoimageslider.SliderViewAdapter;

public class ImageViewHolder extends SliderViewAdapter.ViewHolder {

    private final View itemView;
    private final ImageView imageView;
    private final ImageView imageGifContainer;
    private final ImageButton deleteButton;
    private final ImageButton votePhotoButton;
    private final ImageButton saveButton;
    private final ImageButton shareButton;

    public ImageViewHolder(@NonNull View itemView) {
        super(itemView);
        this.itemView = itemView;
        this.imageView = itemView.findViewById(R.id.iv_auto_image_slider);
        this.imageGifContainer = itemView.findViewById(R.id.iv_gif_container);
        this.deleteButton = itemView.findViewById(R.id.deletePhotoImageButton);
        this.votePhotoButton = itemView.findViewById(R.id.votePhotoImageButton);
        this.saveButton = itemView.findViewById(R.id.savePlaceImageButton);
        this.shareButton = itemView.findViewById(R.id.sharePlaceImageButton);
    }

    public View getItemView() {
        return itemView;
    }

    public ImageView getImageGifContainer() {
        return imageGifContainer;
    }

    public ImageView getImageView() {
        return this.imageView;
    }

    public ImageButton getDeleteButton() {
        return this.deleteButton;
    }

    public ImageButton getVotePhotoButton() {
        return votePhotoButton;
    }

    public ImageButton getSaveButton() {
        return this.saveButton;
    }

    public ImageButton getShareButton() {
        return this.shareButton;
    }
}
