package com.example.call_er;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.call_er.Utilities.FileUtilities;
import com.example.call_er.Utilities.ImageUtilities;
import com.example.call_er.Utilities.InternetUtilities;
import com.example.call_er.Utilities.UserManagement;
import com.example.call_er.Utilities.Utility;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class AccountActivity extends AppCompatActivity {

    private String image;
    private ImageView userImage;
    private TextInputLayout usernameField;
    private TextInputLayout emailField;
    private TextInputLayout phoneField;
    private boolean changeImage;

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            this.onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if(this.getClass().equals(AccountActivity.class)){
            final AlertDialog dialog = new AlertDialog.Builder(this)
                    .setMessage("Sei sicuro/a di non voler modificare i tuoi dati?")
                    .setCancelable(false)
                    .setPositiveButton(this.getString(R.string.si), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            final Intent i = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(i);
                        }
                    })
                    .setNegativeButton(this.getString(R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .create();
            dialog.show();
        }else{
            finish();
        }
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_account);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.accountLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ImageUtilities.PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            final Uri filePath = data.getData();
            final Bitmap bitmap = ImageUtilities.getBitmap(filePath,getContentResolver());
            this.userImage.setImageBitmap(bitmap);
            this.image = ImageUtilities.getStringImage(bitmap, true);
            this.changeImage = true;
        } else if(requestCode == ImageUtilities.REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK && data != null){
            final Bitmap bitmap = (Bitmap) Objects.requireNonNull(data.getExtras()).get("data");
            try {
                assert bitmap != null;
                ImageUtilities.saveImage(bitmap,this);
                this.userImage.setImageBitmap(bitmap);
                this.image = ImageUtilities.getStringImage(bitmap, false);
                this.changeImage = true;
            } catch (IOException e) {
                Log.e(getString(R.string.error), e.toString());
            }
        }
    }


    private void initUI(){
        final TextInputLayout oldPasswordField = findViewById(R.id.oldPasswordOutlinedTextField);
        final TextInputLayout newPasswordField = findViewById(R.id.newPasswordOutlinedTextField);
        final TextInputLayout repeatNewpassord = findViewById(R.id.repeatNewPasswordOutlinedTextField);
        this.usernameField = findViewById(R.id.nameOutlinedTextField);
        this.emailField = findViewById(R.id.emailOutlinedTextField);
        this.phoneField = findViewById(R.id.numberOutlinedTextField);
        this.userImage = this.findViewById(R.id.profileCircleImageView);

        this.getUserData();
        this.changeImage = false;

        this.findViewById(R.id.updateContainedButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final List<String> values = FileUtilities.getData(getCacheDir() + "/mtj.txt");

                if (InternetUtilities.getIsNetworkConnected()) {
                    if(Objects.requireNonNull(usernameField.getEditText()).getText().toString().equals(values.get(UserManagement.USERNAME.getId()))
                            && Objects.requireNonNull(emailField.getEditText()).getText().toString().equals(values.get(UserManagement.EMAIL.getId()))
                            && Objects.requireNonNull(repeatNewpassord.getEditText()).getText().toString().isEmpty()
                            && Objects.requireNonNull(newPasswordField.getEditText()).getText().toString().isEmpty()
                            && Objects.requireNonNull(oldPasswordField.getEditText()).getText().toString().isEmpty()
                            && Objects.requireNonNull(phoneField.getEditText()).getText().toString().equals(values.get(UserManagement.PHONE.getId()))
                            && !changeImage){
                        Utility.showToast("Nessun dato modificato", AccountActivity.this);
                    }else{
                        if ((Objects.requireNonNull(newPasswordField.getEditText()).getText().toString().isEmpty()
                                && !Objects.requireNonNull(oldPasswordField.getEditText()).getText().toString().isEmpty()
                                && !Objects.requireNonNull(repeatNewpassord.getEditText()).getText().toString().isEmpty())
                            || (newPasswordField.getEditText().getText().toString().isEmpty()
                                && !Objects.requireNonNull(oldPasswordField.getEditText()).getText().toString().isEmpty()
                                && Objects.requireNonNull(repeatNewpassord.getEditText()).getText().toString().isEmpty())
                            || (newPasswordField.getEditText().getText().toString().isEmpty()
                                && Objects.requireNonNull(oldPasswordField.getEditText()).getText().toString().isEmpty()
                                && !Objects.requireNonNull(repeatNewpassord.getEditText()).getText().toString().isEmpty())
                            || (!newPasswordField.getEditText().getText().toString().isEmpty()
                                && Objects.requireNonNull(oldPasswordField.getEditText()).getText().toString().isEmpty()
                                && Objects.requireNonNull(repeatNewpassord.getEditText()).getText().toString().isEmpty())
                            || (!newPasswordField.getEditText().getText().toString().isEmpty()
                                && !Objects.requireNonNull(oldPasswordField.getEditText()).getText().toString().isEmpty()
                                && Objects.requireNonNull(repeatNewpassord.getEditText()).getText().toString().isEmpty())
                            || (!newPasswordField.getEditText().getText().toString().isEmpty()
                                && Objects.requireNonNull(oldPasswordField.getEditText()).getText().toString().isEmpty()
                                && !Objects.requireNonNull(repeatNewpassord.getEditText()).getText().toString().isEmpty())
                            || !newPasswordField.getEditText().getText().toString().equals(Objects.requireNonNull(repeatNewpassord.getEditText()).getText().toString())){
                            Utility.showNeutralAlert(getString(R.string.attenzione), "Per modificare la password devi inserire la vecchia password", AccountActivity.this);
                        }else{
                            updateUserData(newPasswordField, changeImage, values.get(UserManagement.ID.getId()));
                        }
                    }
                } else {
                    InternetUtilities.getSnackbar().show();
                }
            }
        });

        this.findViewById(R.id.addUpdatePhotoImageButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(ImageUtilities.showFileChooser(), ImageUtilities.PICK_IMAGE_REQUEST);
            }
        });

        this.findViewById(R.id.makeUpdatePhotoImageButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(ImageUtilities.getPhoto(), ImageUtilities.REQUEST_IMAGE_CAPTURE);
            }
        });

        Utility.setUpToolbar(this, "Il tuo profilo");
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    private void getUserData() {
        final List<String> values = FileUtilities.getData(this.getCacheDir() + "/mtj.txt");

        Objects.requireNonNull(this.usernameField.getEditText()).setText(values.get(UserManagement.USERNAME.getId()));
        Objects.requireNonNull(this.emailField.getEditText()).setText(values.get(UserManagement.EMAIL.getId()));
        Objects.requireNonNull(this.phoneField.getEditText()).setText(values.get(UserManagement.PHONE.getId()));

        final StringBuilder stringBuilder = new StringBuilder();

        for (int i=UserManagement.IMAGE.getId(); i<values.size(); i++){
            stringBuilder.append(values.get(i)).append("\n");
        }
        this.image = stringBuilder.toString();
        this.userImage.setImageBitmap(ImageUtilities.decodeBitmap(this.image));
    }

    private void updateUserData(final TextInputLayout passwordField, final boolean changeImage, final String id){
        final String url ="https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        try {
                            final JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("result").equals("success")) {
                                final String email = emailField.getEditText().getText().toString();
                                final String username = usernameField.getEditText().getText().toString();
                                final String phone = phoneField.getEditText().getText().toString();
                                Utility.saveUserData(AccountActivity.this, email, username, image, phone, id, jsonObject.getString("role"));
                                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                Utility.showToastIntent("Informazioni aggiornate con successso!", AccountActivity.this, i);
                            }else{
                                Utility.showNeutralAlert(getString(R.string.attenzione),jsonObject.getString("message"), AccountActivity.this);
                            }
                        } catch (final JSONException e) {
                            Log.e(getString(R.string.error), e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError error) {
                        Log.e(getString(R.string.error), error.toString());
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                final Map<String,String> params = new HashMap<>();
                final List<String> values = FileUtilities.getData(getCacheDir() + "/mtj.txt");

                params.put("type", "updateUserData");
                if(usernameField.getEditText()!= null && !usernameField.getEditText().getText().toString().equals(values.get(UserManagement.USERNAME.getId()))){
                    params.put("username", usernameField.getEditText().getText().toString());
                }
                if(emailField.getEditText() != null && !emailField.getEditText().getText().toString().equals(values.get(UserManagement.EMAIL.getId()))){
                    params.put("email", emailField.getEditText().getText().toString());
                }
                if(phoneField.getEditText() != null && !phoneField.getEditText().getText().toString().equals(values.get(UserManagement.PHONE.getId()))){
                    params.put("number", phoneField.getEditText().getText().toString());
                }
                if(passwordField.getEditText() != null && !passwordField.getEditText().getText().toString().isEmpty()){
                    params.put("password", passwordField.getEditText().getText().toString());
                }
                params.put("oldEmail", values.get(UserManagement.EMAIL.getId()));

                if(changeImage){
                    params.put("image", image);
                }

                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }
}
