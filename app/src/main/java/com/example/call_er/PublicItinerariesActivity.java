package com.example.call_er;

import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.RadioGroup;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.call_er.Adapters.ListAdapter.PublicItinerariesListAdapter;
import com.example.call_er.Items.ImageItem;
import com.example.call_er.Items.ItineraryItem;
import com.example.call_er.Utilities.InternetUtilities;
import com.example.call_er.Utilities.LoadingDialog;
import com.example.call_er.Utilities.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class PublicItinerariesActivity extends AppCompatActivity {

    private PublicItinerariesListAdapter listAdapter;
    private List<ItineraryItem> values;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_public_itineraries);
        InternetUtilities.setRequestQueue(Volley.newRequestQueue(this));
        this.initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.makeSnackbar(this, R.id.publicItinerariesConstraintLayout);
        InternetUtilities.registerNetworkCallback(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetUtilities.onActivityStop(this);
    }

    private void initUI(){
        final ListView listView = this.findViewById(R.id.publicItinerariesListView);
        this.listAdapter = new PublicItinerariesListAdapter(getApplicationContext(),this);
        listView.setAdapter(this.listAdapter);

        final RadioGroup rg = this.findViewById(R.id.itineraryTypeRadioGroup);
        final LoadingDialog loadingDialog = new LoadingDialog(PublicItinerariesActivity.this);

        rg.check(rg.getChildAt(0).getId());
        loadingDialog.startLoadingDialog();

        if (InternetUtilities.getIsNetworkConnected()) {
            getItineraries("organizer", loadingDialog);
        } else {
            InternetUtilities.getSnackbar().show();
        }

        this.values = new ArrayList<>();

        rg.setOnCheckedChangeListener((group, checkedId) -> {
        loadingDialog.startLoadingDialog();

            switch(checkedId){
                case -1:
                    Utility.showNeutralAlert(getString(R.string.attenzione),getString(R.string.nessuna_categoria_scelta), PublicItinerariesActivity.this);
                    break;
                case R.id.ufficialRadioButton:
                    if (InternetUtilities.getIsNetworkConnected()) {
                        getItineraries("organizer", loadingDialog);
                    } else {
                        InternetUtilities.getSnackbar().show();
                    }
                    break;
                case R.id.userRadioButton:
                    if (InternetUtilities.getIsNetworkConnected()) {
                        getItineraries("user", loadingDialog);
                    } else {
                        InternetUtilities.getSnackbar().show();
                    }
                    break;
            }
        });

        Utility.setUpToolbar(this, "Itinerari");
        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    private void getItineraries(final String role, final LoadingDialog loadingDialog){
        final String url = "https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("result").equals("success")) {
                            values.clear();
                            if(!jsonObject.getString("message").equals("Nessun itinerario creato")) {
                                final JSONArray jsonArray = new JSONArray(jsonObject.getString("values"));
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    final JSONObject itinerary = (JSONObject) jsonArray.get(i);
                                    final String id = itinerary.getString("itinerary_id");
                                    final String name = itinerary.getString("name");
                                    final String city = itinerary.getString("city");
                                    final String creator_name = itinerary.getString("creator_name");

                                    final ItineraryItem itineraryItem = new ItineraryItem(id, name, city);
                                    itineraryItem.setCreatorName(creator_name);

                                    final String votes;
                                    if( itinerary.has("votes")){
                                        votes = itinerary.getString("votes");
                                    }else{
                                        votes = "0";
                                    }
                                    final String avg;

                                    if(itinerary.has("vote_avg")){
                                        avg = itinerary.getString("vote_avg");
                                    }else{
                                        avg = "0";
                                    }

                                    if(itinerary.has("image")){
                                        final String image = itinerary.getString("image");
                                        final String image_id = itinerary.getString("image_id");
                                        final ImageItem imageItem = new ImageItem(image_id, image);
                                        itineraryItem.setImage(imageItem);
                                    }
                                    itineraryItem.setVotes(votes);
                                    itineraryItem.setAvg(avg);
                                    values.add(itineraryItem);
                                }
                            }
                            listAdapter.setData(values);
                        }else{
                            Utility.showNeutralAlert(getString(R.string.attenzione),jsonObject.getString("message"), PublicItinerariesActivity.this);
                        }
                        loadingDialog.dismissDialog();
                    } catch (final JSONException e) {
                        Log.e(getString(R.string.error), e.toString());
                    }
                },
                error -> Log.e(getString(R.string.error), error.toString())){
            @Override
            protected Map<String,String> getParams(){
                final Map<String, String> params = new HashMap<>();
                params.put("type", "getPublicItineraries");
                params.put("role", role);
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }
}
