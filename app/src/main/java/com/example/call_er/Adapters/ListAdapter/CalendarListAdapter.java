package com.example.call_er.Adapters.ListAdapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.call_er.Items.ImageItem;
import com.example.call_er.Items.ItineraryItem;
import com.example.call_er.Items.VisitedPlaceItem;
import com.example.call_er.Items.VisitedPlaceOrItineraryItem;
import com.example.call_er.R;
import com.example.call_er.Utilities.Extra;
import com.example.call_er.Utilities.ImageUtilities;
import com.example.call_er.VisitedItineraryActivity;
import com.example.call_er.VisitedPlaceActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CalendarListAdapter extends ListAdapter{
    private final Activity activity;

    public CalendarListAdapter(final Context context, final Activity activity){
        super(context);
        this.activity = activity;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final VisitedPlaceOrItineraryItem item = (VisitedPlaceOrItineraryItem) super.getList().get(position);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) super.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = Objects.requireNonNull(inflater).inflate(R.layout.list_favorite_places, parent, false);
        }

        final Bitmap bitmap;
        final ImageItem imageItem;

        if(item.isPlace()){
             imageItem = item.getVisitedPlaceItem().getImage();
        }else{
            imageItem = item.getItineraryItem().getImage();
        }

        if(imageItem != null){
            bitmap = ImageUtilities.decodeBitmap(imageItem.getImage());
        }else{
            bitmap = BitmapFactory.decodeResource(activity.getResources(), R.drawable.logo);
        }

        final ImageView placeImageView = convertView.findViewById(R.id.placeImageView);
        final TextView nameTextView = convertView.findViewById(R.id.nameTextView);
        final TextView messageTextView = convertView.findViewById(R.id.addressTextView);

        final String name;
        final String city;

        if(item.isPlace()){
            final VisitedPlaceItem visitedItem = item.getVisitedPlaceItem();
            name = visitedItem.getName();
            city = visitedItem.getCity();
        }else{
            final ItineraryItem itineraryItem = item.getItineraryItem();
            name = itineraryItem.getName();
            city = itineraryItem.getCity();
        }

        placeImageView.setImageBitmap(bitmap);
        nameTextView.setText(name);
        messageTextView.setText(city);

        convertView.findViewById(R.id.visitedPlacesLinearLayout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent i;
                if(item.isPlace()){
                    final VisitedPlaceItem visitedItem = item.getVisitedPlaceItem();
                    i = new Intent(activity.getApplicationContext(), VisitedPlaceActivity.class);
                    i.putExtra(Extra.NAME.getTypeExtra(), visitedItem.getName());
                    i.putExtra(Extra.CITY.getTypeExtra(), visitedItem.getCity());
                    i.putExtra(Extra.ROAD.getTypeExtra(), visitedItem.getRoad());
                    i.putExtra(Extra.ADDRESS.getTypeExtra(), visitedItem.getAddress());
                    i.putExtra(Extra.LAT.getTypeExtra(), visitedItem.getLat());
                    i.putExtra(Extra.LON.getTypeExtra(), visitedItem.getLon());
                    i.putExtra(Extra.ID.getTypeExtra(), visitedItem.getId());
                    i.putExtra(Extra.DATE.getTypeExtra(), visitedItem.getDate());
                }else{
                    final ItineraryItem itineraryItem = item.getItineraryItem();
                    i = new Intent(activity.getApplicationContext(), VisitedItineraryActivity.class);
                    i.putExtra(Extra.NAME.getTypeExtra(), itineraryItem.getName());
                    i.putExtra(Extra.CITY.getTypeExtra(), itineraryItem.getCity());
                    i.putExtra(Extra.ID.getTypeExtra(), itineraryItem.getId());
                    i.putExtra(Extra.CREATOR_NAME.getTypeExtra(), itineraryItem.getCreatorName());
                    i.putExtra(Extra.DEPARTURE_DATE.getTypeExtra(), itineraryItem.getDepartureDate());
                    i.putExtra(Extra.RETURN_DATE.getTypeExtra(), itineraryItem.getReturnDate());
                }
                i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "CalendarActivity");
                activity.startActivity(i);
            }
        });

        return convertView;
    }

    public void setData(final List<VisitedPlaceOrItineraryItem> newData) {
        super.setList(newData);
        notifyDataSetChanged();
    }
}
