package com.example.call_er.Adapters.ExpandableListAdapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.example.call_er.Items.GenericItem;
import com.example.call_er.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class UserInfoExpandableListAdapter extends BaseExpandableListAdapter {

    private final Context context;
    private List<String> headers;
    private Map<String,List<GenericItem>> values;

    public UserInfoExpandableListAdapter(final Context context) {
        this.context = context;
        this.headers = new ArrayList<>();
        headers.add("Itinerari");
        headers.add("Luoghi Visitati");
        this.values = new HashMap<>();
    }

    @Override
    public int getGroupCount() {
        return this.headers.size();
    }

    @Override
    public int getChildrenCount(final int groupPosition) {
        return Objects.requireNonNull(this.values.get(this.headers.get(groupPosition))).size();
    }

    @Override
    public Object getGroup(final int groupPosition) {
        return this.headers.get(groupPosition);
    }

    @Override
    public Object getChild(final int groupPosition, final int childPosition) {
        return Objects.requireNonNull(this.values.get(this.headers.get(groupPosition))).get(childPosition);
    }

    @Override
    public long getGroupId(final int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(final int groupPosition, final int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, final boolean isExpanded, View convertView, ViewGroup parent) {
        String header = this.headers.get(groupPosition);
        if (convertView == null){
            final LayoutInflater inflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = Objects.requireNonNull(inflater).inflate(R.layout.list_group_expandable_list, parent, false);
        }
        final TextView lblListHeader = convertView.findViewById(R.id.listHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(header);
        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, final boolean isLastChild, View convertView, ViewGroup parent) {
        final LayoutInflater inflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = Objects.requireNonNull(inflater).inflate(R.layout.item_with_date_expandable_list, parent, false);

        final GenericItem genericItem = (GenericItem) getChild(groupPosition, childPosition);
        final TextView nameField = convertView.findViewById(R.id.nameItemTextView);
        final TextView dateField = convertView.findViewById(R.id.dateItemTextView);

        nameField.setText(genericItem.getName());
        dateField.setText(genericItem.getDate());

        return convertView;
    }

    @Override
    public boolean isChildSelectable(final int groupPosition, final int childPosition) {
        return true;
    }

    public void setData(final Map<String, List<GenericItem>> val){
        this.values.clear();
        this.values.putAll(val);
        this.notifyDataSetChanged();
    }

}
