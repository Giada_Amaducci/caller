package com.example.call_er.Adapters.ListAdapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.call_er.Items.VisitedPlaceItem;
import com.example.call_er.R;
import com.example.call_er.Utilities.Extra;
import com.example.call_er.Utilities.ImageUtilities;
import com.example.call_er.VisitedPlaceActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class FavoritePlacesListAdapter extends ListAdapter implements Filterable {

    private List<VisitedPlaceItem> visitedPlacesItemsFull;
    private final Activity activity;

    public FavoritePlacesListAdapter(final Context context, final Activity activity) {
        super(context);
        this.visitedPlacesItemsFull = new ArrayList<>();
        this.activity = activity;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final VisitedPlaceItem item = (VisitedPlaceItem) super.getList().get(position);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) super.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = Objects.requireNonNull(inflater).inflate(R.layout.list_favorite_places, parent, false);
        }

        final Bitmap bitmap;

        if(item.getImage() != null){
            bitmap = ImageUtilities.decodeBitmap(item.getImage().getImage());
        }else{
            bitmap = BitmapFactory.decodeResource(activity.getResources(), R.drawable.logo);
        }

        final ImageView placeImageView = convertView.findViewById(R.id.placeImageView);
        placeImageView.setImageBitmap(bitmap);
        final TextView nameTextView = convertView.findViewById(R.id.nameTextView);
        nameTextView.setText(item.getName());
        final TextView messageTextView = convertView.findViewById(R.id.addressTextView);
        messageTextView.setText(item.getAddress());

        convertView.findViewById(R.id.visitedPlacesLinearLayout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent i = new Intent(activity.getApplicationContext(), VisitedPlaceActivity.class);
                i.putExtra(Extra.NAME.getTypeExtra(), item.getName());
                i.putExtra(Extra.CITY.getTypeExtra(), item.getCity());
                i.putExtra(Extra.ROAD.getTypeExtra(), item.getRoad());
                i.putExtra(Extra.ADDRESS.getTypeExtra(), item.getAddress());
                i.putExtra(Extra.LAT.getTypeExtra(), item.getLat());
                i.putExtra(Extra.LON.getTypeExtra(), item.getLon());
                i.putExtra(Extra.ID.getTypeExtra(), item.getId());
                i.putExtra(Extra.DATE.getTypeExtra(), item.getDate());
                activity.startActivity(i);
            }
        });

        return convertView;
    }

    @Override
    public Filter getFilter() {
        return this.visitedPlacesFilter;
    }

    public void setData(final List<VisitedPlaceItem> newData) {
        this.visitedPlacesItemsFull.clear();
        this.visitedPlacesItemsFull.addAll(newData);
        super.setList(newData);
        notifyDataSetChanged();
    }

    private Filter visitedPlacesFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(final CharSequence constraint) {
            final List<VisitedPlaceItem> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(visitedPlacesItemsFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (final VisitedPlaceItem item : visitedPlacesItemsFull) {
                    if (item.getName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }

            final FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(final CharSequence constraint, final FilterResults results) {
            getList().clear();
            getList().addAll((List) results.values);
            notifyDataSetChanged();
        }
    };
}
