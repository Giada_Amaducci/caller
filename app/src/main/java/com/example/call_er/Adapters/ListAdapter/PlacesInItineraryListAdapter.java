package com.example.call_er.Adapters.ListAdapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.call_er.Items.ItineraryItem;
import com.example.call_er.Items.ItineraryPlaceItem;
import com.example.call_er.Items.VisitedPlaceItem;
import com.example.call_er.PlaceActivity;
import com.example.call_er.R;
import com.example.call_er.Utilities.DateAndCalendarUtilities;
import com.example.call_er.Utilities.Extra;
import com.example.call_er.Utilities.FileUtilities;
import com.example.call_er.Utilities.UserManagement;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PlacesInItineraryListAdapter extends ListAdapter implements Filterable {
    private List<ItineraryPlaceItem> visitedPlacesItemsFull;
    private ItineraryItem itineraryItem;
    private final String prevActivity;
    private final Activity activity;
    private Filter visitedPlacesFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(final CharSequence constraint) {
            final List<ItineraryPlaceItem> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(visitedPlacesItemsFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (final ItineraryPlaceItem item : visitedPlacesItemsFull) {
                    if (item.getPlaceItem().getName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }

            final FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(final CharSequence constraint, final FilterResults results) {
            getList().clear();
            getList().addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public PlacesInItineraryListAdapter(final Context context, final Activity activity, final ItineraryItem itineraryItem, final String prevActivity) {
        super(context);
        this.visitedPlacesItemsFull = new ArrayList<>();
        this.activity = activity;
        this.itineraryItem = itineraryItem;
        this.prevActivity = prevActivity;
    }

    @Override
    public Filter getFilter() {
        return this.visitedPlacesFilter;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        final ItineraryPlaceItem placeItem = (ItineraryPlaceItem) super.getList().get(position);

        if(view == null) {
            final LayoutInflater inflater = (LayoutInflater)super.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = Objects.requireNonNull(inflater).inflate(R.layout.list_places_in_itinerary, viewGroup, false);
        }

        final TextView nameTextView = view.findViewById(R.id.titlePlaceTextView);
        final TextView dateTextView = view.findViewById(R.id.datePlaceTextView);
        final ImageButton imageButton = view.findViewById(R.id.deletePlaceImageButton);
        nameTextView.setText(placeItem.getPlaceItem().getName());
        dateTextView.setText(placeItem.getPlaceItem().getAddress());

        final String username = FileUtilities.getData( activity.getCacheDir()+"/mtj.txt").get(UserManagement.USERNAME.getId());
        if(this.prevActivity.equals("PublicItineraryActivity") || (this.itineraryItem.getCreatorName() != null && !this.itineraryItem.getCreatorName().equals(username))){
            imageButton.setVisibility(View.GONE);
        }else{
            imageButton.setOnClickListener(v -> {
                final AlertDialog dialog = new AlertDialog.Builder(super.getContext())
                        .setTitle(super.getContext().getString(R.string.attenzione))
                        .setMessage(R.string.sicuro_di_cancellare)
                        .setCancelable(false)
                        .setPositiveButton(super.getContext().getString(R.string.si), (dialog12, which) -> removeElement(position))
                        .setNegativeButton(super.getContext().getString(R.string.no), (dialog1, which) -> dialog1.dismiss())
                        .create();
                dialog.show();
            });
        }

        if(prevActivity.equals("ItineraryActivity")){
            view.findViewById(R.id.placeLinearLayout).setOnClickListener(v -> {
                final Intent i = new Intent(activity.getApplicationContext(), PlaceActivity.class);
                i.putExtra(Extra.NAME.getTypeExtra(), placeItem.getPlaceItem().getName());
                i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "PlaceInItinerary");
                i.putExtra(Extra.ADDRESS.getTypeExtra(), placeItem.getPlaceItem().getAddress());
                i.putExtra(Extra.ROAD.getTypeExtra(), placeItem.getPlaceItem().getRoad());
                i.putExtra(Extra.CITY.getTypeExtra(), placeItem.getPlaceItem().getCity());
                i.putExtra(Extra.LAT.getTypeExtra(), placeItem.getPlaceItem().getLat());
                i.putExtra(Extra.LON.getTypeExtra(), placeItem.getPlaceItem().getLon());
                activity.startActivity(i);
            });
        }

        return view;
    }

    public void setData(final List<ItineraryPlaceItem> newData) {
        this.visitedPlacesItemsFull.clear();
        this.visitedPlacesItemsFull.addAll(newData);
        super.setList(newData);
        notifyDataSetChanged();
    }

    private void removeElement(final int position){
        this.visitedPlacesItemsFull.remove(position);
        super.getList().remove(position);
        this.itineraryItem.setPlaces(this.visitedPlacesItemsFull);
        this.notifyDataSetChanged();
    }
}
