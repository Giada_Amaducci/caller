package com.example.call_er.Adapters.ImageSliderAdapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.example.call_er.R;
import com.example.call_er.Utilities.InternetUtilities;
import com.example.call_er.Utilities.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class VisitedPlaceImageSliderAdapter extends VisitedImageSliderAdapter {

    public VisitedPlaceImageSliderAdapter(Context context, Activity activity) {
        super(context, activity);
    }

    @Override
    public void removeElement(final String image_id, final int position){
        final String url = "https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("result").equals("success")) {
                            Utility.showToast(jsonObject.getString("message"), getActivity());
                            getmSliderItems().remove(position);
                            renewItems(getmSliderItems());
                        } else {
                            Utility.showNeutralAlert(getActivity().getString(R.string.attenzione), jsonObject.getString("message"), getActivity());
                        }
                    } catch (final JSONException e) {
                        Log.e(getActivity().getString(R.string.error), e.toString());
                    }
                },
                error -> Log.e(getActivity().getString(R.string.error), error.toString())) {
            @Override
            protected Map<String, String> getParams() {
                final Map<String, String> params = new HashMap<>();
                params.put("type", "deleteImagePlace");
                params.put("image_id", image_id);
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }
}
