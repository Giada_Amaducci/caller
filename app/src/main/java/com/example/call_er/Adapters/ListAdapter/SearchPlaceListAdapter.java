package com.example.call_er.Adapters.ListAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.call_er.Items.PlaceItem;
import com.example.call_er.R;

import java.util.List;
import java.util.Objects;

public class SearchPlaceListAdapter extends ListAdapter {

    public SearchPlaceListAdapter(Context context) {
        super(context);
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        final PlaceItem placeItem = (PlaceItem) super.getList().get(position);

        if(view == null) {
            final LayoutInflater inflater = (LayoutInflater)super.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = Objects.requireNonNull(inflater).inflate(R.layout.item_place, viewGroup, false);
        }
        final TextView nameTextView = view.findViewById(R.id.nameItemTextView);
        final TextView addressTextView = view.findViewById(R.id.addressItemTextView);

        nameTextView.setText(placeItem.getName());
        addressTextView.setText(placeItem.getAddress());

        return view;
    }

    public void setData(final List<PlaceItem> newData) {
        super.setList(newData);
        this.notifyDataSetChanged();
    }
}
