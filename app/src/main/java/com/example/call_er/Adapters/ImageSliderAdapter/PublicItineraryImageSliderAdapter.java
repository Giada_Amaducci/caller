package com.example.call_er.Adapters.ImageSliderAdapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.call_er.Holders.ImageViewHolder;
import com.example.call_er.Items.ImageItem;
import com.example.call_er.R;
import com.example.call_er.Utilities.ImageUtilities;
import com.example.call_er.Utilities.Utility;
import com.smarteist.autoimageslider.SliderViewAdapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PublicItineraryImageSliderAdapter  extends SliderViewAdapter<ImageViewHolder> {

    private Context context;
    private Activity activity;
    private List<ImageItem> mSliderItems = new ArrayList<>();

    public PublicItineraryImageSliderAdapter(final Context context, final Activity activity) {
        this.context = context;
        this.activity = activity;
    }

    public void renewItems(List<ImageItem> sliderItems) {
        this.mSliderItems = sliderItems;
        notifyDataSetChanged();
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.slider_image_itinerary, parent, false);
        return new ImageViewHolder(inflate);
    }

    @Override
    public void onBindViewHolder(final ImageViewHolder viewHolder, final int position) {

        final ImageItem sliderItem = mSliderItems.get(position);

        viewHolder.getImageView().setImageBitmap(ImageUtilities.decodeBitmap(sliderItem.getImage()));

        viewHolder.getSaveButton().setOnClickListener(v -> {
            try {
                ImageUtilities.saveImage(ImageUtilities.decodeBitmap(sliderItem.getImage()), context);
                Utility.showToast("Immagine salvata in galleria!", activity);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        viewHolder.getShareButton().setOnClickListener(v -> context.startActivity(Intent.createChooser(Utility.shareImage(ImageUtilities.decodeBitmap(sliderItem.getImage()), context), "Share With")));
    }

    @Override
    public int getCount() {
        return mSliderItems.size();
    }
}
