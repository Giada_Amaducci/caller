package com.example.call_er.Adapters.RecyclerView;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.call_er.Holders.MessageHolder;
import com.example.call_er.Items.MessageItem;
import com.example.call_er.R;
import com.example.call_er.Utilities.FileUtilities;
import com.example.call_er.Utilities.UserManagement;

import java.util.ArrayList;
import java.util.List;

public class MessageListAdapter extends RecyclerView.Adapter implements Filterable {
    private static final int VIEW_TYPE_MESSAGE_SENT = 1;
    private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;

    private Context mContext;
    private List<MessageItem> mMessageList;
    private List<MessageItem> mMessageListFull;

    public MessageListAdapter(Context context) {
        this.mContext = context;
        this.mMessageList = new ArrayList<>();
        this.mMessageListFull = new ArrayList<>();
    }

    @Override
    public int getItemCount() {
        return mMessageList.size();
    }

    @Override
    public int getItemViewType(int position) {
        final List<String> values = FileUtilities.getData(mContext.getCacheDir() + "/mtj.txt");
        Log.d("message", "" + mMessageList.get(position));
        final MessageItem message = mMessageList.get(position);
        final String userId = values.get(UserManagement.ID.getId());

        if (message.getSenderId().equals(userId)) {
            return VIEW_TYPE_MESSAGE_SENT;
        } else {
            return VIEW_TYPE_MESSAGE_RECEIVED;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;

        if (viewType == VIEW_TYPE_MESSAGE_SENT) {
            view = LayoutInflater.from(parent.getContext())
                                 .inflate(R.layout.item_message_sent, parent, false);
        } else{
            view = LayoutInflater.from(parent.getContext())
                                 .inflate(R.layout.item_message_received, parent, false);
        }

        return new MessageHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final MessageItem message = mMessageList.get(position);

        ((MessageHolder) holder).getMessageText().setText(message.getMessage());
        ((MessageHolder) holder).getTimeText().setText(message.getCreatedAt());
    }

    @Override
    public Filter getFilter() {
        return messageFilter;
    }

    private Filter messageFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            final List<MessageItem> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(mMessageListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (final MessageItem item : mMessageListFull) {
                    if (item.getMessage().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }

            final FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mMessageList.clear();
            final List<?> result = (List<?>) results.values;
            for (final Object object : result) {
                if (object instanceof MessageItem) {
                    mMessageList.add((MessageItem) object);
                }
            }
            notifyDataSetChanged();
        }
    };

    public void setData(List<MessageItem> newData) {
        this.mMessageList.clear();
        this.mMessageList.addAll(newData);
        this.mMessageListFull.clear();
        this.mMessageListFull.addAll(newData);
        this.notifyDataSetChanged();
    }
}
