package com.example.call_er.Adapters.ListAdapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.call_er.Items.ItineraryItem;
import com.example.call_er.PublicItineraryActivity;
import com.example.call_er.R;
import com.example.call_er.Utilities.Extra;
import com.example.call_er.Utilities.ImageUtilities;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PublicItinerariesListAdapter extends ListAdapter implements Filterable {
    private List<ItineraryItem> itineraryItemsFull;
    private final Activity activity;


    public PublicItinerariesListAdapter(final Context context, final Activity activity) {
        super(context);
        this.itineraryItemsFull = new ArrayList<>();
        this.activity = activity;
    }
    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ItineraryItem item = (ItineraryItem) super.getList().get(position);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) super.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = Objects.requireNonNull(inflater).inflate(R.layout.list_favorite_places, parent, false);
        }

        final Bitmap bitmap;

        if(item.getImage() != null){
            bitmap = ImageUtilities.decodeBitmap(item.getImage().getImage());
        }else{
            bitmap = BitmapFactory.decodeResource(activity.getResources(), R.drawable.logo);
        }

        final ImageView placeImageView = convertView.findViewById(R.id.placeImageView);
        placeImageView.setImageBitmap(bitmap);
        final TextView nameTextView = convertView.findViewById(R.id.nameTextView);
        nameTextView.setText(item.getName());
        final TextView messageTextView = convertView.findViewById(R.id.addressTextView);
        messageTextView.setText(String.format("%s: %s", "Creato da", item.getCreatorName()));

        convertView.findViewById(R.id.visitedPlacesLinearLayout).setOnClickListener(v -> {
            final Intent i = new Intent(activity.getApplicationContext(), PublicItineraryActivity.class);
            i.putExtra(Extra.NAME.getTypeExtra(), item.getName());
            i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "PublicItineraryActivity");
            i.putExtra(Extra.CREATOR_NAME.getTypeExtra(), item.getCreatorName());
            i.putExtra(Extra.ID.getTypeExtra(), item.getId());
            i.putExtra(Extra.CITY.getTypeExtra(), item.getCity());
            i.putExtra(Extra.AVG_VOTE.getTypeExtra(), item.getAvg());
            i.putExtra(Extra.USER_COUNT.getTypeExtra(), item.getVotes());
            activity.startActivity(i);
        });

        return convertView;
    }

    @Override
    public Filter getFilter() {
        return this.itineraryFilter;
    }

    public void setData(final List<ItineraryItem> newData) {
        this.itineraryItemsFull.clear();
        this.itineraryItemsFull.addAll(newData);
        super.setList(newData);
        notifyDataSetChanged();
    }

    private Filter itineraryFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(final CharSequence constraint) {
            final List<ItineraryItem> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(itineraryItemsFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (final ItineraryItem item : itineraryItemsFull) {
                    if (item.getName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }

            final FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(final CharSequence constraint, final FilterResults results) {
            getList().clear();
            getList().addAll((List) results.values);
            notifyDataSetChanged();
        }
    };
}
