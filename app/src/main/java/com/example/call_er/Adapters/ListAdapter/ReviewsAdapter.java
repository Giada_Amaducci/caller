package com.example.call_er.Adapters.ListAdapter;

import android.accounts.Account;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageButton;

import com.example.call_er.AccountActivity;
import com.example.call_er.Items.PlaceItem;
import com.example.call_er.Items.ReviewItem;
import com.example.call_er.R;
import com.example.call_er.UserInfoActivity;
import com.example.call_er.Utilities.Extra;
import com.example.call_er.Utilities.FileUtilities;
import com.example.call_er.Utilities.ImageUtilities;
import com.example.call_er.Utilities.UserManagement;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ReviewsAdapter extends ListAdapter {

    private Activity activity;

    public ReviewsAdapter(final Context context, final Activity activity) {
        super(context);
        this.activity = activity;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ReviewItem item = (ReviewItem) super.getList().get(position);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) super.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = Objects.requireNonNull(inflater).inflate(R.layout.list_reviews, parent, false);
        }

        final Bitmap bitmap = ImageUtilities.decodeBitmap(item.getUserItem().getImage());

        final ImageView userImageView = convertView.findViewById(R.id.userImageView);
        userImageView.setImageBitmap(bitmap);

        userImageView.setOnClickListener(v -> {
            final Intent i;
            if(!item.getUserItem().getId().equals(FileUtilities.getData(activity.getCacheDir()+"/mtj.txt").get(UserManagement.ID.getId()))){
                i = new Intent(getContext(), UserInfoActivity.class);
                i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "ReviewsActivity");
                i.putExtra(Extra.ID.getTypeExtra(), item.getUserItem().getId());
                i.putExtra(Extra.NAME.getTypeExtra(), item.getUserItem().getName());
                //i.putExtra(Extra.IMAGE.getTypeExtra(), item.getUserItem().getImage());
                i.putExtra(Extra.EMAIL.getTypeExtra(), item.getUserItem().getEmail());
            }else{
                i = new Intent(getContext(), AccountActivity.class);
            }
            activity.startActivity(i);
        });

        final AppCompatImageButton oneStarButton = convertView.findViewById(R.id.starOneImageButton);
        final AppCompatImageButton twoStarButton = convertView.findViewById(R.id.starTwoImageButton);
        final AppCompatImageButton threeStarButton = convertView.findViewById(R.id.starThreeImageButton);
        final AppCompatImageButton fourStarButton = convertView.findViewById(R.id.starFourImageButton);
        final AppCompatImageButton fiveStarButton = convertView.findViewById(R.id.starFiveImageButton);
        final List<AppCompatImageButton> imageButtons = new ArrayList<>();

        imageButtons.add(0, oneStarButton);
        imageButtons.add(1, twoStarButton);
        imageButtons.add(2, threeStarButton);
        imageButtons.add(3, fourStarButton);
        imageButtons.add(4, fiveStarButton);

        for(int i=0; i< imageButtons.size(); i++){
            if(i<Integer.parseInt(item.getVote())){
                imageButtons.get(i).setImageResource(R.drawable.baseline_star_24);
            }else{
                imageButtons.get(i).setImageResource(R.drawable.baseline_star_border_24);
            }
        }

        final TextView messageTextView = convertView.findViewById(R.id.messageTextView);
        messageTextView.setText(item.getMessage());

        return convertView;
    }

    public void setData(final List<ReviewItem> newData) {
        super.setList(newData);
        this.notifyDataSetChanged();
    }
}
