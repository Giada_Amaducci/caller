package com.example.call_er.Adapters.ListAdapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.call_er.Items.ChatItem;
import com.example.call_er.R;
import com.example.call_er.UserInfoActivity;
import com.example.call_er.Utilities.Extra;
import com.example.call_er.Utilities.ImageUtilities;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ChatsListAdapter extends ListAdapter implements Filterable {

    private final List<ChatItem> chatsItemsFull;
    private final Activity activity;

    public ChatsListAdapter(final Context context, final Activity activity) {
        super(context);
        this.chatsItemsFull = new ArrayList<>();
        this.activity = activity;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ChatItem item = (ChatItem) super.getList().get(position);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) super.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = Objects.requireNonNull(inflater).inflate(R.layout.list_chats, parent, false);
        }
        final Bitmap bitmap = ImageUtilities.decodeBitmap(item.getImage());

        final ImageView userImageView = convertView.findViewById(R.id.userImageView);
        userImageView.setImageBitmap(bitmap);
        final TextView nameTextView = convertView.findViewById(R.id.nameTextView);
        nameTextView.setText(item.getName());
        final TextView messageTextView = convertView.findViewById(R.id.messageTextView);
        messageTextView.setText(item.getLastMessage());

        final TextView countTextView = convertView.findViewById(R.id.messageCountTextView);
        if(item.getUnreadMessages() == 0){
            countTextView.setVisibility(View.INVISIBLE);
        }else{
            countTextView.setVisibility(View.VISIBLE);
            final String unreadMessages = "" + item.getUnreadMessages();
            countTextView.setText(unreadMessages);
        }

        convertView.findViewById(R.id.userImageView).setOnClickListener(v -> {
            final Intent i = new Intent(getContext(), UserInfoActivity.class);
            i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "ChatsActivity");
            i.putExtra(Extra.ID.getTypeExtra(), item.getId());
            i.putExtra(Extra.NAME.getTypeExtra(), item.getName());
            i.putExtra(Extra.EMAIL.getTypeExtra(), item.getEmail());
            activity.startActivity(i);
        });

        return convertView;
    }

    @Override
    public Filter getFilter() {
        return this.chatFilter;
    }

    public void setData(final List<ChatItem> newData) {
        this.chatsItemsFull.clear();
        this.chatsItemsFull.addAll(newData);
        super.setList(newData);
        notifyDataSetChanged();
    }

    private Filter chatFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(final CharSequence constraint) {
            final List<ChatItem> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(chatsItemsFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (final ChatItem item : chatsItemsFull) {
                    if (item.getName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }

            final FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(final CharSequence constraint, final FilterResults results) {
            getList().clear();
            getList().addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

}
