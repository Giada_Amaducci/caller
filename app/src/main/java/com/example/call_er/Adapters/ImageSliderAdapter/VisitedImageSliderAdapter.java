package com.example.call_er.Adapters.ImageSliderAdapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.call_er.Holders.ImageViewHolder;
import com.example.call_er.Items.ImageItem;
import com.example.call_er.R;
import com.example.call_er.Utilities.ImageUtilities;
import com.example.call_er.Utilities.Utility;
import com.smarteist.autoimageslider.SliderViewAdapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class VisitedImageSliderAdapter extends SliderViewAdapter<ImageViewHolder> {

    private List<ImageItem> mSliderItems;
    private Context context;
    private Activity activity;

    VisitedImageSliderAdapter(final Context context, final Activity activity){
        this.context = context;
        this.activity = activity;
        this.mSliderItems = new ArrayList<>();
    }

    public List<ImageItem> getmSliderItems() {
        return mSliderItems;
    }

    public Context getContext() {
        return context;
    }

    public Activity getActivity() {
        return activity;
    }

    public void renewItems(List<ImageItem> sliderItems) {
        this.mSliderItems = sliderItems;
        notifyDataSetChanged();
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent) {
        final View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.slider_image_visited_place, parent, false);
        return new ImageViewHolder(inflate);
    }

    @Override
    public void onBindViewHolder(ImageViewHolder viewHolder, int position) {
        final ImageItem sliderItem = mSliderItems.get(position);

        viewHolder.getImageView().setImageBitmap(ImageUtilities.decodeBitmap(sliderItem.getImage()));

        viewHolder.getDeleteButton().setOnClickListener(v -> {
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setTitle(context.getString(R.string.attenzione))
                    .setMessage(R.string.sicuro_di_cancellare)
                    .setCancelable(false)
                    .setPositiveButton(context.getString(R.string.si), (dialog1, which) -> removeElement(sliderItem.getId(), position))
                    .setNegativeButton(context.getString(R.string.no), (dialog12, which) -> dialog12.dismiss())
                    .create();
            dialog.show();
        });

        viewHolder.getSaveButton().setOnClickListener(v -> {
            try {
                ImageUtilities.saveImage(ImageUtilities.decodeBitmap(sliderItem.getImage()), context);
                Utility.showToast("Immagine salvata in galleria!", activity);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        viewHolder.getShareButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(Intent.createChooser(Utility.shareImage(ImageUtilities.decodeBitmap(sliderItem.getImage()), context), "Share With"));
            }
        });
    }

    @Override
    public int getCount() {
        return mSliderItems.size();
    }

    public void removeElement(final String image_id, final int position){ }
}
