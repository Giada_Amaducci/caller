package com.example.call_er.Adapters.ExpandableListAdapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.example.call_er.Items.PlaceItem;
import com.example.call_er.PlaceActivity;
import com.example.call_er.R;
import com.example.call_er.Utilities.Extra;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class PlaceExpandableListAdapter extends BaseExpandableListAdapter {

    private final Context context;
    private final Activity activity;
    private List<String> headers;
    private Map<String,List<PlaceItem>> values;
    private String placeType;

    public PlaceExpandableListAdapter(final Context context, final Activity activity, final String placeType) {
        this.context =context;
        this.activity = activity;
        this.headers = new ArrayList<>();
        this.placeType = placeType;
        this.values = new HashMap<>();
    }

    public void setHeaders(final List<String> headers) {
        this.headers = headers;
    }

    public Map<String, List<PlaceItem>> getValues() {
        return values;
    }

    @Override
    public int getGroupCount() {
        return this.headers.size();
    }

    @Override
    public int getChildrenCount(final int groupPosition) {
        return Objects.requireNonNull(this.values.get(this.headers.get(groupPosition))).size();
    }

    @Override
    public Object getGroup(final int groupPosition) {
        return this.headers.get(groupPosition);
    }

    @Override
    public Object getChild(final int groupPosition, final int childPosition) {
        return Objects.requireNonNull(this.values.get(this.headers.get(groupPosition))).get(childPosition);
    }

    @Override
    public long getGroupId(final int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(final int groupPosition, final int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, final boolean isExpanded, View convertView, ViewGroup parent) {
        String header = this.headers.get(groupPosition);
        if (convertView == null){
            final LayoutInflater inflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = Objects.requireNonNull(inflater).inflate(R.layout.list_group_expandable_list, parent, false);
        }
        final TextView lblListHeader = convertView.findViewById(R.id.listHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(header);
        return convertView;
    }

    public View getChildView(final int groupPosition, final int childPosition, final boolean isLastChild, View convertView, ViewGroup parent) {
        final LayoutInflater inflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = Objects.requireNonNull(inflater).inflate(R.layout.item_with_date_expandable_list, parent, false);

        final PlaceItem visitedItem = (PlaceItem)getChild(groupPosition, childPosition);
        final TextView nameField = convertView.findViewById(R.id.nameItemTextView);
        final TextView addressField = convertView.findViewById(R.id.dateItemTextView);

        nameField.setText(visitedItem.getName());
        addressField.setText(visitedItem.getAddress());

        convertView.findViewById(R.id.itemLayout).setOnClickListener(v -> {
            final Intent i = new Intent(activity.getApplicationContext(), PlaceActivity.class);
            i.putExtra(Extra.NAME.getTypeExtra(), visitedItem.getName());
            i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "PlacesActivity");
            i.putExtra(Extra.ADDRESS.getTypeExtra(), visitedItem.getAddress());
            i.putExtra(Extra.ROAD.getTypeExtra(), visitedItem.getRoad());
            i.putExtra(Extra.CITY.getTypeExtra(), visitedItem.getCity());
            i.putExtra(Extra.LAT.getTypeExtra(), visitedItem.getLat());
            i.putExtra(Extra.LON.getTypeExtra(), visitedItem.getLon());
            i.putExtra(Extra.TYPE.getTypeExtra(), placeType);
            activity.startActivity(i);
        });

        return convertView;
    }

    @Override
    public boolean isChildSelectable(final int groupPosition, final int childPosition) {
        return true;
    }

    public void setData(final Map<String, List<PlaceItem>> val){
        this.values.clear();
        this.values.putAll(val);
        this.notifyDataSetChanged();
    }
}
