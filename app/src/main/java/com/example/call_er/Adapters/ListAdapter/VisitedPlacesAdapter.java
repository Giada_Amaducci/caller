package com.example.call_er.Adapters.ListAdapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.call_er.Items.VisitedPlaceItem;
import com.example.call_er.R;
import com.example.call_er.Utilities.Extra;
import com.example.call_er.Utilities.ImageUtilities;
import com.example.call_er.Utilities.InternetUtilities;
import com.example.call_er.Utilities.Utility;
import com.example.call_er.VisitedPlaceActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class VisitedPlacesAdapter extends ListAdapter implements Filterable {

    private List<VisitedPlaceItem> visitedPlacesItemsFull;
    private final Activity activity;
    public VisitedPlacesAdapter(final Context context, final Activity activity) {
        super(context);
        this.visitedPlacesItemsFull = new ArrayList<>();
        this.activity = activity;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final VisitedPlaceItem item = (VisitedPlaceItem) super.getList().get(position);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) super.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = Objects.requireNonNull(inflater).inflate(R.layout.list_places, parent, false);
        }

        final Bitmap bitmap;

        if(item.getImage() != null){
            bitmap = ImageUtilities.decodeBitmap(item.getImage().getImage());
        }else{
            bitmap = BitmapFactory.decodeResource(activity.getResources(), R.drawable.logo);
        }

        final ImageView placeImageView = convertView.findViewById(R.id.placeImageView);
        placeImageView.setImageBitmap(bitmap);
        final TextView nameTextView = convertView.findViewById(R.id.nameTextView);
        nameTextView.setText(item.getName());
        final TextView messageTextView = convertView.findViewById(R.id.addressTextView);
        messageTextView.setText(item.getAddress());
        final ImageButton imageButton = convertView.findViewById(R.id.addFavoritePlacesImageButton);

        if(item.isPrefered()){
            imageButton.setBackgroundResource(R.drawable.baseline_favorite_green_24);
        }else{
            imageButton.setBackgroundResource(R.drawable.baseline_favorite_border_green_24);
        }

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(item.isPrefered()){
                    v.setBackgroundResource(R.drawable.baseline_favorite_border_green_24);
                    addOrDeleteFavorite(item.getId(), "delete");
                }else{
                    v.setBackgroundResource(R.drawable.baseline_favorite_green_24);
                    addOrDeleteFavorite(item.getId(), "add");
                }
                item.setIsPrefered(!item.isPrefered());
            }
        });

        convertView.findViewById(R.id.visitedPlacesLinearLayout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent i = new Intent(activity.getApplicationContext(), VisitedPlaceActivity.class);
                i.putExtra(Extra.NAME.getTypeExtra(), item.getName());
                i.putExtra(Extra.CITY.getTypeExtra(), item.getCity());
                i.putExtra(Extra.ROAD.getTypeExtra(), item.getRoad());
                i.putExtra(Extra.ADDRESS.getTypeExtra(), item.getAddress());
                i.putExtra(Extra.LAT.getTypeExtra(), item.getLat());
                i.putExtra(Extra.LON.getTypeExtra(), item.getLon());
                i.putExtra(Extra.ID.getTypeExtra(), item.getId());
                i.putExtra(Extra.DATE.getTypeExtra(), item.getDate());
                activity.startActivity(i);
            }
        });

        return convertView;
    }

    @Override
    public Filter getFilter() {
        return this.visitedPlacesFilter;
    }

    public void setData(final List<VisitedPlaceItem> newData) {
        this.visitedPlacesItemsFull.clear();
        this.visitedPlacesItemsFull.addAll(newData);
        super.setList(newData);
        notifyDataSetChanged();
    }

    private Filter visitedPlacesFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(final CharSequence constraint) {
            final List<VisitedPlaceItem> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(visitedPlacesItemsFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (final VisitedPlaceItem item : visitedPlacesItemsFull) {
                    if (item.getName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }

            final FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(final CharSequence constraint, final FilterResults results) {
            getList().clear();
            getList().addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    private void addOrDeleteFavorite(final String id, final String action) {
        final String url = "https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        try {
                            final JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("result").equals("success")) {
                                Utility.showToast(jsonObject.getString("message"), activity);
                            } else {
                                Utility.showNeutralAlert(activity.getString(R.string.attenzione), jsonObject.getString("message"), activity);
                            }
                        } catch (final JSONException e) {
                            Log.e(activity.getString(R.string.error), e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError error) {
                        Log.e(activity.getString(R.string.error), error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                final Map<String, String> params = new HashMap<>();
                params.put("type", "addOrDeleteFavorite");
                params.put("place_id", id);
                params.put("action", action);
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }
}
