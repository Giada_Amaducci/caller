package com.example.call_er.Adapters.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.call_er.Holders.LocationViewHolder;
import com.example.call_er.Items.SingleRecyclerViewLocation;
import com.example.call_er.MapActivity;
import com.example.call_er.R;

import java.lang.ref.WeakReference;
import java.util.List;

public class LocationRecyclerViewAdapter extends RecyclerView.Adapter<LocationViewHolder> {

    private List<SingleRecyclerViewLocation> locationList;
    private WeakReference<MapActivity> weakReference;

    public LocationRecyclerViewAdapter(MapActivity activity,
                                       List<SingleRecyclerViewLocation> locationList) {
        this.locationList = locationList;
        this.weakReference = new WeakReference<>(activity);
    }

    @NonNull
    @Override
    public LocationViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_directions_card, parent, false);
        return new LocationViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(LocationViewHolder holder, int position) {
        SingleRecyclerViewLocation singleRecyclerViewLocation = locationList.get(position);
        holder.getName().setText(singleRecyclerViewLocation.getName());
        holder.getAddress().setText(singleRecyclerViewLocation.getAddress());
        holder.setClickListener((view, position1) -> weakReference.get()
                .drawNavigationPolylineRoute(weakReference.get().directionsRouteList.get(position1)));
    }

    @Override
    public int getItemCount() {
        return locationList.size();
    }
}
