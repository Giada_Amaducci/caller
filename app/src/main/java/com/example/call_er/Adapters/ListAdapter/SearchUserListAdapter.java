package com.example.call_er.Adapters.ListAdapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.call_er.Items.UserItem;
import com.example.call_er.R;
import com.example.call_er.Utilities.ImageUtilities;

import java.util.List;
import java.util.Objects;

public class SearchUserListAdapter extends ListAdapter {

    public SearchUserListAdapter(Context context) {
        super(context);
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        final UserItem userItem = (UserItem) super.getList().get(position);

        if(view == null) {
            final LayoutInflater inflater = (LayoutInflater)super.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = Objects.requireNonNull(inflater).inflate(R.layout.item_user, viewGroup, false);
        }
        final TextView nameTextView = view.findViewById(R.id.nameTextView);
        final TextView phoneTextView = view.findViewById(R.id.phoneTextView);
        final ImageView imageView = view.findViewById(R.id.userImageView);

        nameTextView.setText(userItem.getName());
        phoneTextView.setText(userItem.getPhone());
        final Bitmap bitmap = ImageUtilities.decodeBitmap(userItem.getImage());
        imageView.setImageBitmap(bitmap);

        return view;
    }

    public void setData(final List<UserItem> newData) {
        super.setList(newData);
        this.notifyDataSetChanged();
    }
}
