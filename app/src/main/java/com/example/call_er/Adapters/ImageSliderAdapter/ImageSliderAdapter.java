package com.example.call_er.Adapters.ImageSliderAdapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.example.call_er.Holders.ImageViewHolder;
import com.example.call_er.Items.ImageItem;
import com.example.call_er.R;
import com.example.call_er.Utilities.FileUtilities;
import com.example.call_er.Utilities.ImageUtilities;
import com.example.call_er.Utilities.InternetUtilities;
import com.example.call_er.Utilities.UserManagement;
import com.example.call_er.Utilities.Utility;
import com.smarteist.autoimageslider.SliderViewAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ImageSliderAdapter extends SliderViewAdapter<ImageViewHolder> {

    private Context context;
    private Activity activity;
    private List<ImageItem> mSliderItems = new ArrayList<>();

    public ImageSliderAdapter(final Context context, final Activity activity) {
        this.context = context;
        this.activity = activity;
    }

    public void renewItems(List<ImageItem> sliderItems) {
        this.mSliderItems = sliderItems;
        notifyDataSetChanged();
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.slider_image_place, parent, false);
        return new ImageViewHolder(inflate);
    }

    @Override
    public void onBindViewHolder(final ImageViewHolder viewHolder, final int position) {

        final ImageItem sliderItem = mSliderItems.get(position);

        viewHolder.getImageView().setImageBitmap(ImageUtilities.decodeBitmap(sliderItem.getImage()));

        if(sliderItem.isVoted()){
            viewHolder.getVotePhotoButton().setBackgroundResource(R.drawable.baseline_favorite_24);
        }else{
            viewHolder.getVotePhotoButton().setBackgroundResource(R.drawable.baseline_favorite_border_24);
        }

        viewHolder.getVotePhotoButton().setOnClickListener(v -> {
            if(sliderItem.isVoted()){
                v.setBackgroundResource(R.drawable.baseline_favorite_border_24);
                addOrDeleteVote(sliderItem.getId(), "delete");
            }else{
                v.setBackgroundResource(R.drawable.baseline_favorite_24);
                addOrDeleteVote(sliderItem.getId(), "add");
            }
            sliderItem.setIsVoted(!sliderItem.isVoted());
        });

        viewHolder.getSaveButton().setOnClickListener(v -> {
            try {
                ImageUtilities.saveImage(ImageUtilities.decodeBitmap(sliderItem.getImage()), context);
                Utility.showToast("Immagine salvata in galleria!", activity);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        viewHolder.getShareButton().setOnClickListener(v -> context.startActivity(Intent.createChooser(Utility.shareImage(ImageUtilities.decodeBitmap(sliderItem.getImage()), context), "Share With")));
    }

    @Override
    public int getCount() {
        return mSliderItems.size();
    }

    private void addOrDeleteVote(final String imageId, final String action){
        final String url = "https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("result").equals("success")) {
                            Utility.showToast(jsonObject.getString("message"), activity);
                        } else {
                            Utility.showNeutralAlert(activity.getString(R.string.attenzione), jsonObject.getString("message"), activity);
                        }
                    } catch (final JSONException e) {
                        Log.e(activity.getString(R.string.error), e.toString());
                    }
                },
                error -> Log.e(activity.getString(R.string.error), error.toString())) {
            @Override
            protected Map<String, String> getParams() {
                final Map<String, String> params = new HashMap<>();
                params.put("type", "addOrDeleteVote");
                params.put("image_id", imageId);
                params.put("user_id", FileUtilities.getData( activity.getCacheDir()+"/mtj.txt").get(UserManagement.ID.getId()));
                params.put("action", action);
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }
}
