package com.example.call_er.Adapters.ListAdapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

public class ListAdapter extends BaseAdapter {

    private final List<?> list;
    private final Context context;

    public ListAdapter(final Context context){
        this.list = new ArrayList<>();
        this.context = context;
    }

    @Override
    public int getCount() {
        return this.list.size();
    }

    @Override
    public Object getItem(int position) {
        return this.list.get(position);
    }

    @Override
    public long getItemId(final int position) {
        return position;
    }

    @Override
    public View getView(final int i, final View view, final ViewGroup viewGroup) {
        return null;
    }

    public List<?> getList() {
        return list;
    }

    public void setList(final List<?> list){
        this.list.clear();
        this.list.addAll((List)list);
        this.notifyDataSetChanged();
    }

    public Context getContext() {
        return context;
    }
}
