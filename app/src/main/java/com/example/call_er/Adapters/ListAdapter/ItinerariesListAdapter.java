package com.example.call_er.Adapters.ListAdapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.example.call_er.Adapters.ImageSliderAdapter.VisitedItineraryImageSliderAdapter;
import com.example.call_er.Items.ImageItem;
import com.example.call_er.Items.ItineraryItem;
import com.example.call_er.Items.PlaceItem;
import com.example.call_er.OrganizerItineraryActivity;
import com.example.call_er.Utilities.FileUtilities;
import com.example.call_er.Utilities.InternetUtilities;
import com.example.call_er.Utilities.UserManagement;
import com.example.call_er.Utilities.Utility;
import com.example.call_er.VisitedItineraryActivity;
import com.example.call_er.R;
import com.example.call_er.Utilities.DateAndCalendarUtilities;
import com.example.call_er.Utilities.Extra;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ItinerariesListAdapter extends ListAdapter implements Filterable {

    private List<ItineraryItem> itinerariesItemsFull;
    private PlaceItem placeToAdd;
    private final Activity activity;
    private final String startActivity;

    public ItinerariesListAdapter(final Context context, final Activity activity, final String startActivity) {
        super(context);
        this.itinerariesItemsFull = new ArrayList<>();
        this.startActivity = startActivity;
        this.activity = activity;
    }



    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ItineraryItem item = (ItineraryItem) super.getList().get(position);
        final String role = FileUtilities.getData( activity.getCacheDir()+"/mtj.txt").get(UserManagement.ROLE.getId());

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) super.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if(!this.startActivity.equals("PlaceActivity")){
                convertView = Objects.requireNonNull(inflater).inflate(R.layout.list_itineraries, parent, false);
            }else{
                convertView = Objects.requireNonNull(inflater).inflate(R.layout.list_add_itineraries, parent, false);
            }

        }

        final TextView nameTextView = convertView.findViewById(R.id.nameTextView);
        nameTextView.setText(item.getName());

        final TextView dateTextView = convertView.findViewById(R.id.dateTextView);

        if(role.equals("user")){
            dateTextView.setText(String.format("%s - %s", DateAndCalendarUtilities.getCorrectDateFormatForOutput(item.getDepartureDate()), DateAndCalendarUtilities.getCorrectDateFormatForOutput(item.getReturnDate())));
        }else{
            dateTextView.setText(String.format("%s: %s", "Utenti che lo hanno provato ", item.getUsers()));
        }


        if(this.startActivity.equals("PlaceActivity")){
            convertView.findViewById(R.id.addPlacetoTripImageButton).setOnClickListener(v -> {
                addPlaceToTrip(item.getId(), placeToAdd.getName(), placeToAdd.getLon(), placeToAdd.getLat(), placeToAdd.getAddress(), placeToAdd.getCity(), placeToAdd.getRoad());
            });
        }else{
            convertView.findViewById(R.id.visitedPlacesLinearLayout).setOnClickListener(v -> {
                if(role.equals("user")){
                    final Intent i = new Intent(activity.getApplicationContext(), VisitedItineraryActivity.class);
                    i.putExtra(Extra.NAME.getTypeExtra(), item.getName());
                    i.putExtra(Extra.DEPARTURE_DATE.getTypeExtra(), item.getDepartureDate());
                    i.putExtra(Extra.RETURN_DATE.getTypeExtra(), item.getReturnDate());
                    i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "ItineraryActivity");
                    i.putExtra(Extra.CREATOR_NAME.getTypeExtra(), item.getCreatorName());
                    i.putExtra(Extra.ID.getTypeExtra(), item.getId());
                    i.putExtra(Extra.CITY.getTypeExtra(), item.getCity());
                    activity.startActivity(i);
                }else{
                    final Intent i = new Intent(activity.getApplicationContext(), OrganizerItineraryActivity.class);
                    i.putExtra(Extra.NAME.getTypeExtra(), item.getName());
                    i.putExtra(Extra.ACTIVITY_NAME.getTypeExtra(), "ItineraryActivity");
                    i.putExtra(Extra.USER_COUNT.getTypeExtra(), item.getUsers());
                    i.putExtra(Extra.ID.getTypeExtra(), item.getId());
                    i.putExtra(Extra.CITY.getTypeExtra(), item.getCity());
                    activity.startActivity(i);
                }

            });
        }

        return convertView;
    }

    @Override
    public Filter getFilter() {
        return this.itinerariesFilter;
    }

    public void setData(final List<ItineraryItem> newData) {
        this.itinerariesItemsFull.clear();
        this.itinerariesItemsFull.addAll(newData);
        super.setList(newData);
        notifyDataSetChanged();
    }

    private Filter itinerariesFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(final CharSequence constraint) {
            final List<ItineraryItem> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(itinerariesItemsFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (final ItineraryItem item : itinerariesItemsFull) {
                    if (item.getName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }

            final FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(final CharSequence constraint, final FilterResults results) {
            getList().clear();
            getList().addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public void setPlaceToAdd(PlaceItem placeToAdd) {
        this.placeToAdd = placeToAdd;
    }

    private void addPlaceToTrip(final String itinerary_id, final String name,
                                final String lon, final String lat,
                                final String address, final String city, final String road){
        final String url = "https://caller.altervista.org/api.php";
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        final JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("result").equals("success")) {
                            Utility.showToast("Luogo aggiunto con successo!", activity);
                        } else {
                            Utility.showNeutralAlert(activity.getString(R.string.attenzione), jsonObject.getString("message"), activity);
                        }
                    } catch (final JSONException e) {
                        Log.e(activity.getString(R.string.error), e.toString());
                    }
                },
                error -> Log.e(activity.getString(R.string.error), error.toString())) {
            @Override
            protected Map<String, String> getParams() {
                final Map<String, String> params = new HashMap<>();
                params.put("type", "addPlaceToItinerary");
                params.put("itinerary_id", itinerary_id);
                params.put("name", name);
                params.put("lat", lat);
                params.put("lon", lon);
                params.put("address", address);
                params.put("city", city);
                params.put("road", road);
                return params;
            }
        };
        InternetUtilities.getRequestQueue().add(stringRequest);
    }
}
